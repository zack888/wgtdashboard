# Node Modules
Note that:
- Nebular (global stylesheet for scrollbar)
- ngx-nvd3 (modified for zoom)

# Modified Dependencies

## ngx-nvd3

### dist: ngx-nvd3.core.js

```javascript
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var d3 = require("d3");
var nv = require("nvd3");
var Angular2NvD3;
(function (Angular2NvD3) {
    var NgNvD3 = (function () {
        function NgNvD3(el) {
            this.viewInitialize = false;
            this.el = el;
        }
        NgNvD3.prototype.isViewInitialize = function (value) {
            this.viewInitialize = value;
        };
        NgNvD3.prototype.updateWithOptions = function (options, data) {
            if (!this.viewInitialize || !options) {
                return;
            }
            ;
            var self = this;
            this.clearElement();
            this.chart = nv.models[options.chart.type]();
            this.chart.id = Math.random().toString(36).substr(2, 15);
            for (var key in this.chart) {
                if (!this.chart.hasOwnProperty(key)) {
                    continue;
                }
                if (key[0] === '_') {
                }
                else if ([
                    'clearHighlights',
                    'highlightPoint',
                    'id',
                    'options',
                    'resizeHandler',
                    'state',
                    'open',
                    'close',
                    'tooltipContent'
                ].indexOf(key) >= 0) {
                }
                else if (key === 'dispatch') {
                    this.configureEvents(this.chart[key], options.chart[key]);
                }
                else if ([
                    'bars',
                    'bars1',
                    'bars2',
                    'boxplot',
                    'bullet',
                    'controls',
                    'discretebar',
                    'distX',
                    'distY',
                    'interactiveLayer',
                    'legend',
                    'lines',
                    'lines1',
                    'lines2',
                    'multibar',
                    'pie',
                    'scatter',
                    'scatters1',
                    'scatters2',
                    'sparkline',
                    'stack1',
                    'stack2',
                    'sunburst',
                    'tooltip',
                    'x2Axis',
                    'xAxis',
                    'y1Axis',
                    'y2Axis',
                    'y3Axis',
                    'y4Axis',
                    'yAxis',
                    'yAxis1',
                    'yAxis2'
                ].indexOf(key) >= 0 ||
                    (key === 'stacked' && options.chart.type === 'stackedAreaChart')) {
                    this.configure(this.chart[key], options.chart[key], options.chart.type);
                }
                else if ((key === 'xTickFormat' || key === 'yTickFormat') && options.chart.type === 'lineWithFocusChart') {
                }
                else if ((key === 'tooltips') && options.chart.type === 'boxPlotChart') {
                }
                else if ((key === 'tooltipXContent' || key === 'tooltipYContent') && options.chart.type === 'scatterChart') {
                }
                else if (options.chart[key] === undefined || options.chart[key] === null) {
                }
                else {
                    this.chart[key](options.chart[key]);
                }
            }
            this.updateWithData(data, options);
            nv.addGraph(function () {
                if (!self.chart) {
                    return;
                }
                if (self.chart.resizeHandler) {
                    self.chart.resizeHandler.clear();
                }
                self.chart.resizeHandler = nv.utils.windowResize(function () {
                    if (self.chart && self.chart.update) {
                        self.chart.update();
                    }
                });
                return self.chart;
            }, options.chart['callback']);
        };
        NgNvD3.prototype.updateWithData = function (data, options) {
            if (data) {
              var self = this;
                d3.select(this.el.nativeElement).select('svg').remove();
                var h = void 0, w = void 0;
                this.svg = d3.select(this.el.nativeElement).append('svg');
                if (h = options.chart.height) {
                    if (!isNaN(+h)) {
                        h += 'px';
                    }
                    this.svg.attr('height', h).style({ height: h });
                }
                if (w = options.chart.width) {
                    if (!isNaN(+w)) {
                        w += 'px';
                    }
                    this.svg.attr('width', w).style({ width: w });
                }
                else {
                    this.svg.attr('width', '100%').style({ width: '100%' });
                }

                this.svg.call(d3.behavior.zoom().on("zoom", function () {
                  self.svg.attr("transform", "translate(" + d3.event.translate + ")" + " scale(" + d3.event.scale + ")");
                  //self.svg.attr("transform", d3.event.transform)
                })).append("g");

                this.svg.datum(data).call(this.chart);
            }
        };
        NgNvD3.prototype.configure = function (chart, options, chartType) {
            if (chart && options) {
                for (var key in chart) {
                    if (!chart.hasOwnProperty(key)) {
                        continue;
                    }
                    var value = chart[key];
                    if (key[0] === '_') {
                    }
                    else if (key === 'dispatch') {
                        this.configureEvents(value, options[key]);
                    }
                    else if (key === 'tooltip') {
                        this.configure(chart[key], options[key], chartType);
                    }
                    else if (key === 'contentGenerator') {
                        if (options[key]) {
                            chart[key](options[key]);
                        }
                    }
                    else if ([
                        'axis',
                        'clearHighlights',
                        'defined',
                        'highlightPoint',
                        'nvPointerEventsClass',
                        'options',
                        'rangeBand',
                        'rangeBands',
                        'scatter',
                        'open',
                        'close'
                    ].indexOf(key) === -1) {
                        if (options[key] === undefined || options[key] === null) {
                        }
                        else {
                            chart[key](options[key]);
                        }
                    }
                }
            }
        };
        NgNvD3.prototype.configureEvents = function (dispatch, options) {
            if (dispatch && options) {
                for (var key in dispatch) {
                    if (!dispatch.hasOwnProperty(key)) {
                        continue;
                    }
                    if (options[key] === undefined || options[key] === null) {
                    }
                    else {
                        dispatch.on(key + '._', options[key]);
                    }
                }
            }
        };
        NgNvD3.prototype.clearElement = function () {
            this.el.nativeElement.innerHTML = '';
            if (this.chart && this.chart.tooltip && this.chart.tooltip.id) {
                d3.select('#' + this.chart.tooltip.id()).remove();
            }
            if (nv.tooltip && nv.tooltip.cleanup) {
                nv.tooltip.cleanup();
            }
            if (this.chart && this.chart.resizeHandler) {
                this.chart.resizeHandler.clear();
            }
            this.chart = null;
        };
        return NgNvD3;
    }());
    Angular2NvD3.NgNvD3 = NgNvD3;
})(Angular2NvD3 = exports.Angular2NvD3 || (exports.Angular2NvD3 = {}));
```

## Nebular

### theme/styles/theme/_default.scss

```css
/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */

@import '../core/functions';
@import '../core/mixins';

$theme: (
  font-main: unquote('"Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif'),
  font-secondary: font-main,

  font-weight-thin: 200,
  font-weight-light: 300,
  font-weight-normal: 400,
  font-weight-bolder: 500,
  font-weight-bold: 600,
  font-weight-ultra-bold: 800,

  // TODO: use it as a default font-size
  base-font-size: 16px,

  font-size-xlg: 1.25rem,
  font-size-lg: 1.125rem,
  font-size: 1rem,
  font-size-sm: 0.875rem,
  font-size-xs: 0.75rem,

  radius: 0.375rem,
  padding: 1.25rem,
  margin: 1.5rem,
  line-height: 1.25,

  color-bg: #ffffff,
  color-bg-active: #e9edf2,
  color-fg: #a4abb3,
  color-fg-heading: #2a2a2a,
  color-fg-text: #4b4b4b,
  color-fg-highlight: #40dc7e,

  separator: #ebeef2,

  color-gray: rgba(81, 113, 165, 0.15),
  color-neutral: transparent,
  color-white: #ffffff,
  color-disabled: rgba(255, 255, 255, 0.4),

  color-primary: #8a7fff,
  color-success: #40dc7e,
  color-info: #4ca6ff,
  color-warning: #ffa100,
  color-danger: #ff4c6a,

  // TODO: move to constants
  social-color-facebook: #3b5998,
  social-color-twitter: #55acee,
  social-color-google: #dd4b39,
  social-color-linkedin: #0177b5,
  social-color-github: #6b6b6b,
  social-color-stackoverflow: #2f96e8,
  social-color-dribble: #f26798,
  social-color-behance: #0093fa,

  border-color: color-gray,
  shadow: 0 2px 12px 0 #dfe3eb,

  link-color: #3dcc6d,
  link-color-hover: #2ee56b,
  link-color-visited: link-color,

  scrollbar-fg: #dadada,
  scrollbar-bg: #f2f2f2,
  scrollbar-width: 15px,
  scrollbar-thumb-radius: 2.5px,

  radial-gradient: none,
  linear-gradient: none,

  card-font-size: font-size,
  card-line-height: line-height,
  card-font-weight: font-weight-normal,
  card-fg: color-fg, // TODO: not used
  card-fg-text: color-fg-text,
  card-fg-heading: color-fg-heading, // TODO: not used
  card-bg: color-bg,
  card-height-xxsmall: 96px,
  card-height-xsmall: 216px,
  card-height-small: 336px,
  card-height-medium: 456px,
  card-height-large: 576px,
  card-height-xlarge: 696px,
  card-height-xxlarge: 816px,
  card-shadow: shadow,
  card-border-radius: radius,
  card-padding: padding,
  card-margin: margin,
  card-header-font-family: font-secondary,
  card-header-font-size: font-size-lg,
  card-header-font-weight: font-weight-bold,
  card-separator: separator,
  card-header-fg: color-fg, // TODO: not used
  card-header-fg-heading: color-fg-heading,
  card-header-active-bg: color-fg,
  card-header-active-fg: color-bg,
  card-header-disabled-bg: color-disabled,
  card-header-primary-bg: color-primary,
  card-header-info-bg: color-info,
  card-header-success-bg: color-success,
  card-header-warning-bg: color-warning,
  card-header-danger-bg: color-danger,

  header-font-family: font-secondary,
  header-font-size: font-size,
  header-line-height: line-height,
  header-fg: color-fg-heading,
  header-bg: color-bg,
  header-height: 4.75rem,
  header-padding: 1.25rem,
  header-shadow: shadow,

  footer-height: 4.725rem,
  footer-padding: 1.25rem,
  footer-fg: color-fg-heading,
  footer-bg: color-bg,
  footer-separator: separator,
  footer-shadow: shadow,

  layout-font-family: font-main,
  layout-font-size: font-size,
  layout-line-height: line-height,
  layout-fg: color-fg,
  layout-bg: #ebeff5,
  layout-min-height: 100vh,
  layout-content-width: 900px,
  layout-window-mode-min-width: 300px,
  layout-window-mode-max-width: 1920px,
  layout-window-mode-bg: layout-bg,
  layout-window-mode-padding-top: 4.75rem,
  layout-window-shadow: shadow,
  layout-padding: 2.25rem 2.25rem 0.75rem,
  layout-medium-padding: 1.5rem 1.5rem 0.5rem,
  layout-small-padding: 1rem 1rem 0,

  sidebar-font-size: font-size,
  sidebar-line-height: line-height,
  sidebar-fg: color-fg-heading,
  sidebar-bg: color-bg,
  sidebar-height: 100vh,
  sidebar-width: 16rem,
  sidebar-width-compact: 3.5rem,
  sidebar-padding: padding,
  sidebar-header-height: 3.5rem,
  sidebar-footer-height: 3.5rem,
  sidebar-shadow: shadow,

  menu-font-family: font-secondary,
  menu-font-size: font-size,
  menu-font-weight: font-weight-bolder,
  menu-fg: color-fg-text,
  menu-bg: color-bg,
  menu-active-fg: color-fg-heading,
  menu-active-bg: color-bg,
  menu-active-font-weight: font-weight-bold,

  menu-submenu-bg: color-bg,
  menu-submenu-fg: color-fg-text,
  menu-submenu-active-fg: color-fg-heading,
  menu-submenu-active-bg: color-bg,
  menu-submenu-active-border-color: color-fg-highlight,
  menu-submenu-active-shadow: none,
  menu-submenu-hover-fg: menu-submenu-active-fg,
  menu-submenu-hover-bg: menu-submenu-bg,
  menu-submenu-item-border-width: 0.125rem,
  menu-submenu-item-border-radius: radius,
  menu-submenu-item-padding: 0.5rem 1rem,
  menu-submenu-item-container-padding: 0 1.25rem,
  menu-submenu-padding: 0.5rem,

  menu-group-font-weight: font-weight-bolder,
  menu-group-font-size: 0.875rem,
  menu-group-fg: color-fg,
  menu-group-padding: 1rem 1.25rem,
  menu-item-padding: 0.675rem 1rem 0.675rem 0.75rem,
  menu-item-separator: separator,
  menu-icon-font-size: 2.5rem,
  menu-icon-margin: 0 0.5rem 0 0,
  menu-icon-color: color-fg,
  menu-icon-active-color: color-fg-heading,

  tabs-font-family: font-secondary,
  tabs-font-size: font-size-lg,
  tabs-content-font-family: font-main,
  tabs-content-font-size: font-size,
  tabs-active-bg: transparent,
  tabs-active-font-weight: card-header-font-weight,
  tabs-padding: padding,
  tabs-content-padding: 0,
  tabs-header-bg: transparent,
  tabs-separator: separator,
  tabs-fg: color-fg,
  tabs-fg-text: color-fg-text,
  tabs-fg-heading: color-fg-heading,
  tabs-bg: transparent,
  tabs-selected: color-success,

  route-tabs-font-family: font-secondary,
  route-tabs-font-size: font-size-lg,
  route-tabs-active-bg: transparent,
  route-tabs-active-font-weight: card-header-font-weight,
  route-tabs-padding: padding,
  route-tabs-header-bg: transparent,
  route-tabs-separator: separator,
  route-tabs-fg: color-fg,
  route-tabs-fg-heading: color-fg-heading,
  route-tabs-bg: transparent,
  route-tabs-selected: color-success,

  user-font-size: font-size,
  user-line-height: line-height,
  user-bg: color-bg,
  user-fg: color-fg,
  user-fg-highlight: #bcc3cc,
  user-font-family-secondary: font-secondary,
  user-size-small: 1.5rem,
  user-size-medium: 2.5rem,
  user-size-large: 3.25rem,
  user-size-xlarge: 4rem,
  user-menu-fg: color-fg-heading,
  user-menu-bg: color-bg,
  user-menu-active-fg: #ffffff,
  user-menu-active-bg: color-success,
  user-menu-border: color-success,

  actions-font-size: font-size,
  actions-font-family: font-secondary,
  actions-line-height: line-height,
  actions-fg: color-fg,
  actions-bg: color-bg,
  actions-separator: separator,
  actions-padding: padding,
  actions-size-small: 1.5rem,
  actions-size-medium: 2.25rem,
  actions-size-large: 3.5rem,

  search-btn-open-fg: color-fg,
  search-btn-close-fg:	color-fg,
  search-bg: layout-bg,
  search-bg-secondary: color-fg,
  search-text: color-fg-heading,
  search-info: color-fg,
  search-dash: color-fg,
  search-placeholder: color-fg,

  smart-table-header-font-family: font-secondary,
  smart-table-header-font-size: font-size,
  smart-table-header-font-weight: font-weight-bold,
  smart-table-header-line-height: line-height,
  smart-table-header-fg: color-fg-heading,
  smart-table-header-bg: color-bg,

  smart-table-font-family: font-main,
  smart-table-font-size: font-size,
  smart-table-font-weight: font-weight-normal,
  smart-table-line-height: line-height,
  smart-table-fg: color-fg-heading,
  smart-table-bg: color-bg,

  smart-table-bg-even: #f5f7fc,
  smart-table-fg-secondary: color-fg,
  smart-table-bg-active: #e6f3ff,
  smart-table-padding: 0.875rem 1.25rem,
  smart-table-filter-padding: 0.375rem 0.5rem,
  smart-table-separator: separator,
  smart-table-border-radius: radius,

  smart-table-paging-border-color: separator,
  smart-table-paging-border-width: 1px,
  smart-table-paging-fg-active: #ffffff,
  smart-table-paging-bg-active: color-success,
  smart-table-paging-hover: rgba(0, 0, 0, 0.05),

  toaster-bg: color-primary,
  toaster-fg-default: color-inverse,
  toaster-btn-close-bg: transparent,
  toaster-btn-close-fg: toaster-fg-default,
  toaster-shadow: shadow,

  toaster-fg: color-white,
  toaster-success: color-success,
  toaster-info: color-info,
  toaster-warning: color-warning,
  toaster-wait: color-primary,
  toaster-error: color-danger,

  btn-fg: color-white,
  btn-font-family: font-secondary,
  btn-line-height: line-height,
  btn-disabled-opacity: 0.3,
  btn-cursor: default,

  btn-primary-bg: color-primary,
  btn-secondary-bg: transparent,
  btn-info-bg: color-info,
  btn-success-bg: color-success,
  btn-warning-bg: color-warning,
  btn-danger-bg: color-danger,

  btn-secondary-border: #dadfe6,
  btn-secondary-border-width: 2px,

  btn-padding-y-lg: 0.875rem,
  btn-padding-x-lg: 1.75rem,
  btn-font-size-lg: font-size-lg,

  // default size
  btn-padding-y-md: 0.75rem,
  btn-padding-x-md: 1.5rem,
  btn-font-size-md: 1rem,

  btn-padding-y-sm: 0.625rem,
  btn-padding-x-sm: 1.5rem,
  btn-font-size-sm: 0.875rem,

  btn-padding-y-tn: 0.5rem,
  btn-padding-x-tn: 1.25rem,
  btn-font-size-tn: 0.75rem,

  btn-border-radius: radius,
  btn-rectangle-border-radius: 0.25rem,
  btn-semi-round-border-radius: 0.75rem,
  btn-round-border-radius: 1.5rem,

  btn-hero-shadow: none,
  btn-hero-text-shadow: none,
  btn-hero-bevel-size: 0 0 0 0,
  btn-hero-glow-size: 0 0 0 0,

  btn-outline-fg: color-fg-heading,
  btn-outline-hover-fg: #ffffff,

  btn-group-bg: layout-bg,
  btn-group-fg: color-fg-heading,
  btn-group-separator: #dadfe6,

  form-control-text-primary-color: color-fg-heading,
  form-control-text-secondary-color: color-fg,
  form-control-font-family: font-secondary,
  form-control-bg: color-bg,
  form-control-focus-bg: color-bg,

  form-control-border-width: 2px,
  form-control-border-type: solid,
  form-control-border-radius: radius,
  form-control-border-color: #dadfe6,
  form-control-selected-border-color: color-success,

  form-control-info-border-color: color-info,
  form-control-success-border-color: color-success,
  form-control-danger-border-color: color-danger,
  form-control-warning-border-color: color-warning,

  form-control-placeholder-color: color-fg,
  form-control-placeholder-font-size: 1rem,

  form-control-font-size: 1rem,
  form-control-sm-font-size: font-size-sm,
  form-control-sm-padding: 0.375rem 1.125rem,
  form-control-lg-font-size: font-size-lg,
  form-control-lg-padding: 1.125rem,

  form-control-label-font-weight: 400,

  form-control-feedback-font-size: 0.875rem,
  form-control-feedback-font-weight: font-weight-normal,

  checkbox-bg: transparent,
  checkbox-size: 1.25rem,
  checkbox-border-size: 2px,
  checkbox-border-color: form-control-border-color,
  checkbox-selected-border-color: color-success,
  checkbox-fg: color-fg-heading,
  radio-fg: color-success,

  modal-font-size: font-size,
  modal-line-height: line-height,
  modal-font-weight: font-weight-normal,
  modal-fg: color-fg-text,
  modal-fg-heading: color-fg-heading,
  modal-bg: color-bg,
  modal-border: transparent,
  modal-border-radius: radius,
  modal-padding: padding,
  modal-header-font-family: font-secondary,
  modal-header-font-weight: font-weight-bolder,
  modal-header-font-size: font-size-lg,
  modal-body-font-family: font-main,
  modal-body-font-weight: font-weight-normal,
  modal-body-font-size: font-size,
  modal-separator: separator,
);

// register the theme
$nb-themes: nb-register-theme($theme, default);

```
