import { Component, OnInit } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { NbJSThemeOptions } from '@nebular/theme/services/js-themes/theme.options';

@Component({
  selector: 'ngx-app',
  template: '<ngx-loading-bar [color]="\'#42db7d\'" [includeSpinner]="false"></ngx-loading-bar><router-outlet></router-outlet>',
})

export class AppComponent implements OnInit {
  theme: NbJSThemeOptions;

  constructor(private themeService: NbThemeService) {}

  ngOnInit(): void {
    this.themeService.getJsTheme().subscribe((theme: NbJSThemeOptions) => this.theme = theme);

    //this.themeService.changeTheme('cosmic');
    this.themeService.changeTheme('default');
  }
}
