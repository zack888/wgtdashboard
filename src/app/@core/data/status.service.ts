import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

export interface Status {
  items: any;
}

@Injectable()
export class StatusService {
  statusInterface: Status = {
    items: []
  };

  data = [{
    id: 1910239127,
    name: 'Mark',
    date: 'Today 7.12 PM',
    url: 'https://tmall.com/test@mdo?brand=abc&sku=xyz',
    status: 'In Progress',
  }, {
    id: 1813731812,
    name: 'Mark',
    date: 'Yesterday 5.42 AM',
    url: 'https://tmall.com/test@mdo?brand=abc&sku=xyz',
    status: 'Completed',
  }, {
    id: 1211230718,
    name: 'Mark',
    date: '15 Jan 2017',
    url: 'https://tmall.com/test@mdo?brand=abc&sku=xyz',
    status: 'Completed',
  }];

  getData(): Observable<any> {
    return Observable.of(this.statusInterface.items);
  }

  /*getData() {
    //return this.data;
    return this.statusInterface.items;
  }*/
}
