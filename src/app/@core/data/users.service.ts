import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/observable/of';

import { Http, Response, RequestOptions, Headers} from '@angular/http';

import * as AWS from 'aws-sdk';
import * as Cognito from "amazon-cognito-identity-js";
import { CognitoIdentityServiceProvider } from 'amazon-cognito-identity-js';

let counter = 0;

export interface User {
  name: string;
  credits: number;
  completed: number;
  progress: number;
  email: string
}

@Injectable()
export class UserService {
  analysis: any[] = [{
    id: 0
  }, {
    id: 1
  }, {
    id: 2
  }];

  //private hiddenSource = new BehaviorSubject<any>({});
  //hiddenAnalysis = this.hiddenSource.asObservable();

  userInterface: User = {
    name: ' ',
    credits: 0,
    completed: 0,
    progress: 0,
    email: window.sessionStorage.getItem("email")
  };

  /*hide(item) {
    this.hiddenSource.next(item);
  }*/

  fom: any;

  private users = {
    user: { name: this.userInterface.name, picture: 'assets/images/user.png', icon: require("../../../assets/images/ic_sg.png") },
  };

  private userArray: any[];
  private API_URL = 'https://3erpvhu3aj.execute-api.ap-southeast-1.amazonaws.com/live/register';

  constructor(private http: Http) {
  }

  reload() {
    var userPool = new Cognito.CognitoUserPool({
      UserPoolId: 'ap-southeast-1_7PvKAeRNz',
      ClientId: 'sa6cluje5ml19da5j49hfr2ms',
    });

    AWS.config.update({
      region: 'ap-southeast-1'
    });

    var docClient = new AWS.DynamoDB.DocumentClient();

    var params = {
      TableName: "WGTUsers",
      KeyConditionExpression: "#id = :id",
      ExpressionAttributeNames: {
          "#id": "username",
      },
      ExpressionAttributeValues: {
           ":id": userPool.getCurrentUser().getUsername(),
      }
    };

    docClient.query(params, (err, data) => {
      if (err) {
        //TODO
      } else {
        if (data.Items.length > 0) {
          this.users = {user: { name: data.Items[0].name, picture: 'assets/images/user.png', icon: require("../../../assets/images/ic_sg.png") }};
          this.userInterface.name = data.Items[0].name;
          this.name(this.userInterface.name);
          this.userInterface.credits = data.Items[0].credits;
          this.userInterface.completed = data.Items[0].completed;
          this.userInterface.progress = data.Items[0].progress;
        }
      }
    });
  }

  getUsers(): Observable<any> {
    return Observable.of(this.users);
  }

  getUserArray(): Observable<any[]> {
    return Observable.of(this.userArray);
  }

  getUser(): Observable<any> {
    counter = (counter + 1) % this.userArray.length;
    return Observable.of(this.userArray[counter]);
  }

  setEmail(email: string) {
    this.userInterface.email = email;
    window.sessionStorage.setItem("email", email);
  }

  name(name) {
    window.sessionStorage.setItem("name", name);
  }

  getName(): string {
    return window.sessionStorage.getItem("name");
  }

  persist(token) {
    window.sessionStorage.setItem("sessionToken", token);
  }

  isLoggedOut(): boolean {
    var userPool = new Cognito.CognitoUserPool({
      UserPoolId: 'ap-southeast-1_7PvKAeRNz',
      ClientId: 'sa6cluje5ml19da5j49hfr2ms',
    });

    return userPool.getCurrentUser() == null;
  }

  session() {
    var sessionToken = window.sessionStorage.getItem("sessionToken");
    AWS.config.update({
      region: "ap-southeast-1"
    });
    var userPool = new Cognito.CognitoUserPool({
      UserPoolId: 'ap-southeast-1_7PvKAeRNz',
      ClientId: 'sa6cluje5ml19da5j49hfr2ms',
    });

    if (userPool.getCurrentUser() != null) {
      userPool.getCurrentUser().getSession(function(err, session) {
        if (err) {
          // TODO
        } else {
          AWS.config.credentials = new AWS.CognitoIdentityCredentials({
            IdentityPoolId : 'ap-southeast-1:3bccfae5-d52c-46d2-b2d7-fd1b429d79b6',
            Logins : {
                'cognito-idp.ap-southeast-1.amazonaws.com/ap-southeast-1_7PvKAeRNz' : session.getIdToken().getJwtToken()
            }
          });
          (<AWS.CognitoIdentityCredentials>AWS.config.credentials).refresh((error) => {
            if (error) {
              // TODO
            } else {
              console.log('Successfully logged!');
            }
          });
        }
    });
    }
  }
}
