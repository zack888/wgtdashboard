import { Component, Input, OnInit } from '@angular/core';
import { NbMenuService, NbSidebarService } from '@nebular/theme';
import { User, UserService } from '../../../@core/data/users.service';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {
  userInterface: User;

  @Input() position = 'normal';

  user: any;

  userMenu = [/*{ title: 'Profile' },*/ { title: 'Log out', link: '/auth/login' }];

  constructor(private sidebarService: NbSidebarService,
              private menuService: NbMenuService,
              private userService: UserService) {
    this.userInterface = userService.userInterface;
    if (this.userInterface.name == ' ') {
      this.userInterface.name = this.userService.getName();
    }
  }

  ngOnInit() {
    this.userService.getUsers()
      .subscribe((users: any) => {
        this.user =  users.user;
        console.log(this.user);
      });
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    return false;
  }

  toggleSettings(): boolean {
    this.sidebarService.toggle(false, 'settings-sidebar');
    return false;
  }

  goToHome() {
    this.menuService.navigateHome();
  }
/*
  startSearch() {
    this.analyticsService.trackEvent('startSearch');
  }
  */
}
