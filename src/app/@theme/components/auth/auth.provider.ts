import {
    NbAbstractAuthProvider,
    NbAuthResult,
    NbAuthSimpleToken,
    NbAuthService,
} from '@nebular/auth';
import * as AWS from 'aws-sdk';
import * as Cognito from "amazon-cognito-identity-js";
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

export interface UserData {
  username: string;
  password: string;
}

export interface TokenResponse {
  access_token: string;
  token_type: string;
  expires_in: number;
}

export class AuthProvider extends NbAbstractAuthProvider {
  constructor(
    private authService: NbAuthService,
  ) {
    super();
  
    AWS.config.update({
        region: "ap-southeast-1"
    });
  }

  userPool = new Cognito.CognitoUserPool({
    UserPoolId: 'ap-southeast-1_7PvKAeRNz',
    ClientId: 'sa6cluje5ml19da5j49hfr2ms',
  });
  
  logout(): Observable<NbAuthResult> {
    // return this.authService.logout('name');
    return Observable.of(new NbAuthResult(
      true,
      {},
      '/auth/login',
      false,
      'Sign out success.',
    ));
  }

  authenticate(user: UserData): Observable<NbAuthResult> {
    console.log("auth");
    var pool = new Cognito.CognitoUser({
      Username : user.username,
      Pool : this.userPool
    });

    var auth = new Cognito.AuthenticationDetails({
      Username : user.username,
      Password : user.password
    });

    return Observable.create(observer => {
      pool.authenticateUser(auth, {
        onSuccess: function (result) {
          return new NbAuthResult(
            true,
            {},
            '/',
            true,
            'Login success'
          );
        },
  
        onFailure: function(err) {
          return new NbAuthResult(
            true,
            {},
            '/auth/login',
            false,
            'Login failed',
          );
        },
      });
    })
  }
  
    getConfigValue(key: string) {
      return this.config[key];
    }
    register(data?: UserData): Observable<NbAuthResult> {
      throw new Error('Method not implemented.');
    }
    requestPassword(data?: UserData): Observable<NbAuthResult> {
      throw new Error('Method not implemented.');
    }
    resetPassword(data?: UserData): Observable<NbAuthResult> {
      throw new Error('Method not implemented.');
    }
}