/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { NB_AUTH_OPTIONS_TOKEN } from '../../auth.options';
import { getDeepFromObject } from '../../helpers';

import { UserService } from '../../../../../@core/data/users.service';

import { NbAuthResult, NbAuthService } from '../../services/auth.service';

import * as AWS from 'aws-sdk';
import * as Cognito from "amazon-cognito-identity-js";
import { CognitoIdentity } from 'aws-sdk';

@Component({
  selector: 'nb-request-password-page',
  styleUrls: ['./request-password.component.scss'],
  template: `
    <nb-auth-block>
      <h2 class="title">Forgot Password</h2>
      <small class="form-text sub-title">Enter your email adress and we’ll email a verification code to reset password</small>
      <form (ngSubmit)="requestPass()" #requestPassForm="ngForm">

        <div *ngIf="errors && errors.length > 0 && !submitted"
             class="alert alert-danger" role="alert">
          <div><strong>Oh snap!</strong></div>
        </div>
        <div *ngIf="messages && messages.length > 0 && !submitted"
             class="alert alert-success" role="alert">
          <div><strong>Success!</strong></div>
        </div>

        <div class="form-group">
          <label for="input-email" class="sr-only">Enter your email address</label>
          <input name="email" [(ngModel)]="user.email" id="input-email" #email="ngModel"
                 class="form-control" placeholder="Email address" pattern=".+@.+\..+"
                 [class.form-control-danger]="email.invalid && email.touched"
                 [required]="getConfigValue('forms.validation.email.required')"
                 autofocus>
          <small class="form-text error" *ngIf="email.invalid && email.touched && email.errors?.required">
            Email is required!
          </small>
          <small class="form-text error"
                 *ngIf="email.invalid && email.touched && email.errors?.pattern">
            Email should be the real one!
          </small>
        </div>

        <button [disabled]="submitted || !requestPassForm.form.valid" class="btn btn-hero-success btn-block"
                [class.btn-pulse]="submitted">
          Request password
        </button>
      </form>

      <div class="links col-sm-12">
        <small class="form-text">
          Already have an account? <a routerLink="../login"><strong>Sign In</strong></a>
        </small>
        <small class="form-text">
          New here? <a routerLink="../register"><strong>Sign Up</strong></a>
        </small>
      </div>
    </nb-auth-block>
  `,
})
export class NbRequestPasswordComponent {

  redirectDelay: number = 0;
  showMessages: any = {};
  provider: string = '';

  submitted = false;
  errors: string[] = [];
  messages: string[] = [];
  user: any = {};

  constructor(protected service: NbAuthService,
              @Inject(NB_AUTH_OPTIONS_TOKEN) protected config = {},
              protected userService: UserService,
              protected router: Router) {

    this.redirectDelay = this.getConfigValue('forms.requestPassword.redirectDelay');
    this.showMessages = this.getConfigValue('forms.requestPassword.showMessages');
    this.provider = this.getConfigValue('forms.requestPassword.provider');
  }

  requestPass(): void {
    var self = this;

    this.errors = this.messages = [];
    this.submitted = true;

    var userPool = new Cognito.CognitoUserPool({
      UserPoolId: 'ap-southeast-1_7PvKAeRNz',
      ClientId: 'sa6cluje5ml19da5j49hfr2ms',
    });

    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
      IdentityPoolId: 'ap-southeast-1:3bccfae5-d52c-46d2-b2d7-fd1b429d79b6',
    });

    AWS.config.update({
      region: "ap-southeast-1"
    });

    var pool = new Cognito.CognitoUser({
      Username : this.user.email.toLowerCase(),
      Pool : userPool
    });

    pool.forgotPassword({
      onSuccess: function (result) {
        this.message = [result];
      },
      onFailure: function (err) {
        this.errors = [err];
        this.submitted = false;
        console.log(err);
      },
      inputVerificationCode() {
        self.userService.setEmail(self.user.email.toLowerCase());
        setTimeout(() => {
          return self.router.navigateByUrl('/auth/reset-password');
        }, 0);
        /*var verificationCode = prompt('Please input verification code ' ,'');
        var newPassword = prompt('Enter new password ' ,'');
        pool.confirmPassword(verificationCode, newPassword, {
          onFailure(err) {
            this.errors = [err];
            this.submitted = false;
            console.log(err);
          },
          onSuccess() {
            this.messages = ["Success"];
            setTimeout(() => {
              return this.router.navigateByUrl('/auth/login');
            }, 0);
          },
        });*/
      }
    });

    /*this.service.requestPassword(this.provider, this.user).subscribe((result: NbAuthResult) => {
      this.submitted = false;
      if (result.isSuccess()) {
        this.messages = result.getMessages();
      } else {
        this.errors = result.getErrors();
      }

      const redirect = result.getRedirect();
      if (redirect) {
        setTimeout(() => {
          return this.router.navigateByUrl(redirect);
        }, this.redirectDelay);
      }
    });*/
  }

  getConfigValue(key: string): any {
    return getDeepFromObject(this.config, key, null);
  }
}
