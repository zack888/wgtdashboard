/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, Inject } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NB_AUTH_OPTIONS_TOKEN } from '../../auth.options';
import { getDeepFromObject } from '../../helpers';

import { NbAuthResult, NbAuthService } from '../../services/auth.service';

import * as AWS from 'aws-sdk';
import * as Cognito from "amazon-cognito-identity-js";

import { UserService } from '../../../../../@core/data/users.service';

import { LoadingBarService } from '@ngx-loading-bar/core';

@Component({
  selector: 'nb-register',
  styleUrls: ['./register.component.scss'],
  template: `
    <nb-auth-block>
      <h2 class="title">Sign Up</h2>
      <form (ngSubmit)="register()" #form="ngForm">

        <div *ngIf="errors.length > 0 && !submitted"
             class="alert alert-danger" role="alert">
          <div><strong>Oh snap!</strong></div>
          <div *ngFor="let error of errors">{{ error }}</div>
        </div>
        <!--div *ngIf="showMessages.success && messages && messages.length > 0 && !submitted"
             class="alert alert-success" role="alert">
          <div><strong>Hooray!</strong></div>
          <div *ngFor="let message of messages">{{ message }}</div>
        </div-->

        <div class="form-group">
          <label for="input-name" class="sr-only">Name</label>
          <input name="fullName" [(ngModel)]="user.fullName" id="input-name" #fullName="ngModel"
                 class="form-control" placeholder="Full name"
                 [class.form-control-danger]="fullName.invalid && fullName.touched"
                 [required]="getConfigValue('forms.validation.fullName.required')"
                 [minlength]="getConfigValue('forms.validation.fullName.minLength')"
                 [maxlength]="getConfigValue('forms.validation.fullName.maxLength')"
                 autofocus>
          <small class="form-text error" *ngIf="fullName.invalid && fullName.touched && fullName.errors?.required">
            Full name is required!
          </small>
          <small
            class="form-text error"
            *ngIf="fullName.invalid && fullName.touched && (fullName.errors?.minlength || fullName.errors?.maxlength)">
            Full name should contains
            from {{getConfigValue('forms.validation.password.minLength')}}
            to {{getConfigValue('forms.validation.password.maxLength')}}
            characters
          </small>
        </div>

        <div class="form-group">
          <label for="input-email" class="sr-only">Email address</label>
          <input name="email" [(ngModel)]="user.email" id="input-email" #email="ngModel"
                 class="form-control" placeholder="Email address" pattern=".+@.+\..+"
                 [class.form-control-danger]="email.invalid && email.touched"
                 [required]="getConfigValue('forms.validation.email.required')">
          <small class="form-text error" *ngIf="email.invalid && email.touched && email.errors?.required">
            Email is required!
          </small>
          <small class="form-text error"
                 *ngIf="email.invalid && email.touched && email.errors?.pattern">
            Email should be the real one!
          </small>
        </div>

        <div class="form-group">
          <label for="input-password" class="sr-only">Password</label>
          <input name="password" [(ngModel)]="user.password" type="password" id="input-password"
                 class="form-control" placeholder="Password" #password="ngModel"
                 [class.form-control-danger]="password.invalid && password.touched"
                 [required]="getConfigValue('forms.validation.password.required')"
                 [minlength]="6"
                 [maxlength]="getConfigValue('forms.validation.password.maxLength')">
          <small class="form-text error" *ngIf="password.invalid && password.touched && password.errors?.required">
            Password is required!
          </small>
          <small
            class="form-text error"
            *ngIf="password.invalid && password.touched && (password.errors?.minlength || password.errors?.maxlength)">
            Password should contains
            from 6
            to {{ getConfigValue('forms.validation.password.maxLength') }}
            characters
          </small>
        </div>

        <div class="form-group">
          <label for="input-re-password" class="sr-only">Repeat password</label>
          <input
            name="rePass" [(ngModel)]="user.confirmPassword" type="password" id="input-re-password"
            class="form-control" placeholder="Confirm Password" #rePass="ngModel"
            [class.form-control-danger]="(rePass.invalid || password.value != rePass.value) && rePass.touched"
            [required]="getConfigValue('forms.validation.password.required')">
          <small class="form-text error"
                 *ngIf="rePass.invalid && rePass.touched && rePass.errors?.required">
            Password confirmation is required!
          </small>
          <small
            class="form-text error"
            *ngIf="rePass.touched && password.value != rePass.value && !rePass.errors?.required">
            Password does not match the confirm password.
          </small>
        </div>

        <div class="form-group accept-group col-sm-12" *ngIf="getConfigValue('forms.register.terms')">
          <nb-checkbox name="terms" [(ngModel)]="user.terms" [required]="getConfigValue('forms.register.terms')">
            Agree to <a href="#" target="_blank"><strong>Terms & Conditions</strong></a>
          </nb-checkbox>
        </div>

        <button [disabled]="submitted || !form.valid" class="btn btn-block btn-hero-success"
                [class.btn-pulse]="submitted">
          Register
        </button>
      </form>

      <div class="links">
        <small class="form-text">
          Already have an account? <a routerLink="../login"><strong>Sign in</strong></a>
        </small>
      </div>
    </nb-auth-block>
  `,
})
export class NbRegisterComponent {

  redirectDelay: number = 0;
  //showMessages: any = {};
  provider: string = '';

  submitted = false;
  errors: string[] = [];
  //messages: string[] = [];
  user: any = {};

  invalid = [
    'outlook', 'live', 'hotmail', 'yahoo', 'altavista', 'protonmail', 'yandex', 'aol.com', 'icloud',
    'mail.com', 'fastmail', 'zoho.com', 'hushmail', 'inbox.com', 'gmail',
  ]

  taskId: string;
  pageId: string;

  constructor(protected service: NbAuthService,
              @Inject(NB_AUTH_OPTIONS_TOKEN) protected config = {},
              protected router: Router, protected userService: UserService,
              private loadingBar: LoadingBarService, private activatedRoute: ActivatedRoute
            ) {

    this.redirectDelay = this.getConfigValue('forms.register.redirectDelay');
    //this.showMessages = this.getConfigValue('forms.register.showMessages');
    this.provider = this.getConfigValue('forms.register.provider');
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.taskId = params['task'];
      this.pageId = params['page'];
    });
  }

  validEmail(): boolean {
    var type = this.user.email.split('@')[1];
    var invalid = false;
    this.invalid.forEach(element => {
      if (type.indexOf(element) >= 0) {
        invalid = true;
      }
    });
    return !invalid;
  }

  register(): void {
    this.loadingBar.start();
    this.errors = [];

    if (this.validEmail()) {
      this.submitted = true;

      var userPool = new Cognito.CognitoUserPool({
        UserPoolId: 'ap-southeast-1_7PvKAeRNz',
        ClientId: 'sa6cluje5ml19da5j49hfr2ms',
      });

      if (userPool.getCurrentUser() != null) {
        userPool.getCurrentUser().signOut();
      }

      AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: 'ap-southeast-1:3bccfae5-d52c-46d2-b2d7-fd1b429d79b6',
      });

      (<AWS.CognitoIdentityCredentials>AWS.config.credentials).clearCachedId();

      this.service.register(this.provider, this.user).subscribe((result: NbAuthResult) => {
        AWS.config.update({
          region: "ap-southeast-1"
        });

        AWS.config.credentials = new AWS.CognitoIdentityCredentials({
          IdentityPoolId: 'ap-southeast-1:3bccfae5-d52c-46d2-b2d7-fd1b429d79b6',
        });

        var attributeList = [];
        var attributeName = new Cognito.CognitoUserAttribute({
          Name : 'name',
          Value : this.user.fullName
        });
        attributeList.push(attributeName);

        userPool.signUp(this.user.email.toLowerCase(), this.user.password, attributeList, null, (err, res) => {
          if (err) {
            this.errors.push(err.message);
            this.submitted = false;
            this.loadingBar.complete();
          } else {
            //this.messages[0] = "Registered successfully!";
            var pool = new Cognito.CognitoUser({
              Username : this.user.email.toLowerCase(),
              Pool : userPool
            });

            var auth = new Cognito.AuthenticationDetails({
              Username : this.user.email.toLowerCase(),
              Password : this.user.password
            });

            pool.authenticateUser(auth, {
              onSuccess: (res) => {
                //this.messages[0] = "Login Successfully!";

                userPool.getCurrentUser().getSession((err, session) => {
                  AWS.config.credentials = new AWS.CognitoIdentityCredentials({
                    IdentityPoolId : 'ap-southeast-1:3bccfae5-d52c-46d2-b2d7-fd1b429d79b6',
                    Logins : {
                        'cognito-idp.ap-southeast-1.amazonaws.com/ap-southeast-1_7PvKAeRNz' : session.getIdToken().getJwtToken()
                    }
                  });

                  (<AWS.CognitoIdentityCredentials>AWS.config.credentials).refresh((err) => {
                    if (err) {
                      this.errors.push(err.message);
                      this.submitted = false;
                      this.loadingBar.complete();
                    } else {
                      this.userService.setEmail(this.user.email.toLowerCase());
                      this.userService.persist(AWS.config.credentials.sessionToken);

                      setTimeout(() => {
                        this.loadingBar.complete();

                        if (this.taskId && this.pageId) {
                          return this.router.navigateByUrl('/pages?page=' + this.pageId + '&task=' + this.taskId);
                        } else {
                          return this.router.navigateByUrl('/pages');
                        }
                      }, this.redirectDelay);
                    }
                  });
                });
              },

              onFailure: (err) => {
                this.errors.push(err.message);
                this.submitted = false;
                this.loadingBar.complete();
              },
            });
          }
        });

        /*const redirect = result.getRedirect();
        if (redirect) {
          setTimeout(() => {
            return this.router.navigateByUrl(redirect);
          }, this.redirectDelay);
        }*/
      });
    } else {
      this.errors.push('Please use your corporate email address to register');
      this.submitted = false;
      this.loadingBar.complete();
    }
  }

  getConfigValue(key: string): any {
    return getDeepFromObject(this.config, key, null);
  }
}
