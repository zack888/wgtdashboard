/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, Inject } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NB_AUTH_OPTIONS_TOKEN } from '../../auth.options';
import { getDeepFromObject } from '../../helpers';

import { NbAuthResult, NbAuthService } from '../../services/auth.service';

import * as AWS from 'aws-sdk';
import * as Cognito from "amazon-cognito-identity-js";
import { CognitoIdentity } from 'aws-sdk';

import { UserService } from '../../../../../@core/data/users.service';

import { LoadingBarService } from '@ngx-loading-bar/core';

@Component({
  selector: 'nb-login',
  template: `
    <nb-auth-block>
      <h2 class="title">Sign In</h2>

      <br><img [src]="LOGO" style="display: block;margin: 0 auto;">

      <small class="form-text sub-title"><br>Supercharge your data analytics</small>

      <form (ngSubmit)="login()" #form="ngForm" autocomplete="nope">

        <div *ngIf="errors.length > 0 && !submitted"
             class="alert alert-danger" role="alert">
          <div><strong>Oh snap! Something went wrong with your login 😥</strong></div>
          <div *ngFor="let error of errors">{{ error }}</div>
        </div>

        <div *ngIf="messages.length > 0 && !submitted"
             class="alert alert-success" role="alert">
          <div><strong>Login success! 👍</strong></div>
          <div *ngFor="let message of messages">{{ message }}</div>
        </div>

        <div class="form-group">
          <label for="input-email" class="sr-only">Email address</label>
          <input name="email" [(ngModel)]="user.email" id="input-email" pattern=".+@.+\..+"
                 class="form-control" placeholder="Email address" #email="ngModel"
                 [class.form-control-danger]="email.invalid && email.touched" autofocus
                 [required]="getConfigValue('forms.validation.email.required')">
          <small class="form-text error" *ngIf="email.invalid && email.touched && email.errors?.required">
            Email is required!
          </small>
          <small class="form-text error"
                 *ngIf="email.invalid && email.touched && email.errors?.pattern">
            Email should be the real one!
          </small>
        </div>

        <div class="form-group">
          <label for="input-password" class="sr-only">Password</label>
          <input name="password" [(ngModel)]="user.password" type="password" id="input-password"
                 class="form-control" placeholder="Password" #password="ngModel"
                 [class.form-control-danger]="password.invalid && password.touched"
                 [required]="getConfigValue('forms.validation.password.required')"
                 [minlength]="6"
                 [maxlength]="getConfigValue('forms.validation.password.maxLength')">
          <small class="form-text error" *ngIf="password.invalid && password.touched && password.errors?.required">
            Password is required!
          </small>
          <small
            class="form-text error"
            *ngIf="password.invalid && password.touched && (password.errors?.minlength || password.errors?.maxlength)">
            Password should contains
            from 6
            to {{ getConfigValue('forms.validation.password.maxLength') }}
            characters
          </small>
        </div>

        <div class="form-group accept-group col-sm-12">
          <!--nb-checkbox name="rememberMe" [(ngModel)]="user.rememberMe">Remember me</nb-checkbox>
          <a class="forgot-password" routerLink="../request-password">Forgot Password?</a-->
        </div>

        <div class="row">
          <div class="col-md-6" style="padding-bottom: 1rem;">
            <button type="button" class="btn btn-block btn-hero-info" (click)="register()">
              Register
            </button>
          </div>
          <div class="col-md-6" style="padding-bottom: 1rem;">
            <button [disabled]="submitted || !form.valid" class="btn btn-block btn-hero-success"
              [class.btn-pulse]="submitted">
              Sign In
            </button>
          </div>
        </div>
      </form>

      <div class="links">
        <small class="form-text">
          Forgot Your Password? <a routerLink="../request-password"><strong>Click here</strong></a>
        </small>
        <!--small class="form-text">Or connect with:</small>

        <div class="socials">
          <a href="https://github.com/akveo" target="_blank" class="socicon-github"></a>
          <a href="https://www.facebook.com/akveo/" target="_blank" class="socicon-facebook"></a>
          <a href="https://twitter.com/akveo_inc" target="_blank" class="socicon-twitter"></a>
        </div-->

        <!--small class="form-text">
          Don't have an account? <a routerLink="../register"><strong>Sign Up</strong></a>
        </small-->

        <!--br><br>

        <button class="btn btn-block btn-hero-primary" (click)="prev()">
          Switch to Previous Version
        </button-->

      </div>
    </nb-auth-block>
  `,
})
export class NbLoginComponent {
  LOGO = require("../../../../../../assets/images/wgtlogo.png");

  redirectDelay: number = 0;
  showMessages: any = {};
  provider: string = '';

  errors: string[] = [];
  messages: string[] = [];
  user: any = {};
  submitted: boolean = false;

  hasData: boolean = false;

  taskId: string;
  pageId: string;

  constructor(protected service: NbAuthService,
              @Inject(NB_AUTH_OPTIONS_TOKEN) protected config = {},
              protected router: Router, protected userService: UserService,
              private loadingBar: LoadingBarService, private activatedRoute: ActivatedRoute
            ) {

    this.redirectDelay = this.getConfigValue('forms.login.redirectDelay');
    this.showMessages = this.getConfigValue('forms.login.showMessages');
    this.provider = this.getConfigValue('forms.login.provider');
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.taskId = params['task'];
      this.pageId = params['page'];
    });

    var userPool = new Cognito.CognitoUserPool({
      UserPoolId: 'ap-southeast-1_7PvKAeRNz',
      ClientId: 'sa6cluje5ml19da5j49hfr2ms',
    });

    if (userPool.getCurrentUser() != null) {
      userPool.getCurrentUser().signOut();

      if (AWS.config.credentials) {
        (<AWS.CognitoIdentityCredentials>AWS.config.credentials).clearCachedId();
      }

      this.userService.name('');
    }
  }

  register() {
    setTimeout(() => {
      this.loadingBar.complete();
      if (this.taskId && this.pageId) {
        return this.router.navigateByUrl('/auth/register');
      } else {
        return this.router.navigateByUrl('/auth/register?page=' + this.pageId + '&task=' + this.taskId);
      }
    }, 0);
  }

  prev() {
    window.location.href = "https://wheregottext.com/oldwgt/index.html";
  }

  login(): void {
    this.loadingBar.start();
    this.errors = [];
    this.messages = [];

    this.submitted = true;

    var userPool = new Cognito.CognitoUserPool({
      UserPoolId: 'ap-southeast-1_7PvKAeRNz',
      ClientId: 'sa6cluje5ml19da5j49hfr2ms',
    });

    /*if (userPool.getCurrentUser() != null) {
      userPool.getCurrentUser().signOut();
    }*/

    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
      IdentityPoolId: 'ap-southeast-1:3bccfae5-d52c-46d2-b2d7-fd1b429d79b6',
    });

    //(<AWS.CognitoIdentityCredentials>AWS.config.credentials).clearCachedId();

    this.service.authenticate(this.provider, this.user).subscribe((result: NbAuthResult) => {
      AWS.config.update({
        region: "ap-southeast-1"
      });

      AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: 'ap-southeast-1:3bccfae5-d52c-46d2-b2d7-fd1b429d79b6',
      });

      var pool = new Cognito.CognitoUser({
        Username : this.user.email.toLowerCase(),
        Pool : userPool
      });

      var auth = new Cognito.AuthenticationDetails({
        Username : this.user.email.toLowerCase(),
        Password : this.user.password
      });

      pool.authenticateUser(auth, {
        onSuccess: (res) => {
          this.errors = [];
          this.messages.push("Login Successfully!");

          userPool.getCurrentUser().getSession((err, session) => {
            AWS.config.credentials = new AWS.CognitoIdentityCredentials({
              IdentityPoolId : 'ap-southeast-1:3bccfae5-d52c-46d2-b2d7-fd1b429d79b6',
              Logins : {
                  'cognito-idp.ap-southeast-1.amazonaws.com/ap-southeast-1_7PvKAeRNz' : session.getIdToken().getJwtToken()
              }
            });

            (<AWS.CognitoIdentityCredentials>AWS.config.credentials).refresh((err) => {
              if (err) {
                this.messages = [];
                this.errors.push(err.message);
                this.submitted = false;
                this.loadingBar.complete();

                /*setTimeout(() => {
                  return this.router.navigateByUrl('/auth/register');
                }, 1500);*/
              } else {
                this.userService.setEmail(this.user.email.toLowerCase());
                this.userService.persist(session.getIdToken().getJwtToken());

                setTimeout(() => {
                  this.loadingBar.complete();
                  if (this.taskId && this.pageId) {
                    return this.router.navigateByUrl('/pages/charts/' + this.pageId + '?task=' + this.taskId);
                  } else {
                    return this.router.navigateByUrl('/pages');
                  }
                }, this.redirectDelay);
              }
            });
          });
        },

        onFailure: (err) => {
          this.messages = [];
          this.errors.push(err.message);
          this.submitted = false;
          this.loadingBar.complete();

          /*setTimeout(() => {
            return this.router.navigateByUrl('/auth/register');
          }, 1500);*/
        },
      });
    });
  }

  getConfigValue(key: string): any {
    return getDeepFromObject(this.config, key, null);
  }
}
