import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';

import * as AWS from 'aws-sdk';
import * as Cognito from "amazon-cognito-identity-js";
import { CognitoIdentityServiceProvider } from 'amazon-cognito-identity-js';

import { User, UserService } from '../../../@core/data/users.service'

import { LoadingBarService } from '@ngx-loading-bar/core';

import * as validator from 'valid-url';

import { ToasterService, ToasterConfig, Toast, BodyOutputType } from 'angular2-toaster';

@Component({
  selector: 'ngx-new-video',
  styleUrls: ['./new-video.component.scss'],
  templateUrl: './new-video.component.html',
})
export class NewVideoComponent {
  showMessages: any = {};
  errors: string[] = [];
  urlerr: boolean = false;
  messages: string[] = [];
  submitted: boolean = false;

  user: User;
  submission: any = {};

  config: ToasterConfig;

  constructor(protected userService: UserService, private loadingBar: LoadingBarService, protected router: Router,
    private toasterService: ToasterService) {
    if (this.userService.isLoggedOut()) {
      setTimeout(() => {
        return this.router.navigateByUrl('/auth/login');
      }, 0);
    } else {
      this.userService.session();
    }
    this.user = userService.userInterface;
  }

  validator = [
    'youtube'
  ];

  validurl: boolean = true;

  valid(url): boolean {
    var okay = false;
    for (const valid of this.validator) {
      if (url.indexOf(valid) > -1) {
        okay = true;
        break;
      }
    }
    return okay;
  }

  validURL(url) {
    return !validator.isUri(url);
  }

  submit(form) {
    this.messages = [];
    this.errors = [];

    if (this.valid(this.submission.url)) {
      this.loadingBar.start();
      this.submitted = true;

      var userPool = new Cognito.CognitoUserPool({
        UserPoolId: 'ap-southeast-1_7PvKAeRNz',
        ClientId: 'sa6cluje5ml19da5j49hfr2ms',
      });

      var params = {
        "id" : userPool.getCurrentUser().getUsername(),
        "url": this.submission.url,
      }

      var url = 'https://jwzu8znvz1.execute-api.ap-southeast-1.amazonaws.com/prod/WGT_NewVideo';
      var method = 'POST';
      var xhr = this.createCORSRequest(method, url);

      xhr.onload = () => {
        if (JSON.parse(xhr.responseText).message == "Success") {
          form.reset();

          this.showToast('success', 'Submitted successfully!', 'You may check back later and view the analysis once it\'s completed');

          this.submitted = false;
          this.messages.push('Please wait while we analyze the URL. You may check your tasks for the progress.');
        } else {
          this.submitted = false;

          this.showToast('error', 'Failed to submit', 'Please try again later');

          this.errors.push('Unable to process your task. Please try again later');
        }

        this.loadingBar.complete();
      };

      xhr.onerror = () => {
        console.log(xhr.responseText);
        this.submitted = false;
        this.errors.push('Unable to process your task. Please try again later');

        this.showToast('error', 'Failed to submit', 'Please try again later');

        this.loadingBar.complete();
      };

      xhr.send(JSON.stringify(params));
    } else {
      this.errors.push('This URL does not seem to be from Youtube')
    }
  }

  tasks: any[] = [
    { id: false, description: 'Sort by popularity' },
    { id: true, description: 'Sort by time' },
  ];

  private showToast(type: string, title: string, body: string) {
    this.config = new ToasterConfig({
      positionClass: 'toast-top-right',
      timeout: 10000,
      newestOnTop: true,
      tapToDismiss: true,
      preventDuplicates: true,
      animation: 'slideDown',
      limit: 5,
    });
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 10000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this.toasterService.popAsync(toast);
  }

  createCORSRequest = function(method, url) {
    var xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {
      xhr.open(method, url, true);
    } /*else if (typeof XDomainRequest != "undefined") {
      // IE8 & IE9
      xhr = new XDomainRequest();
      xhr.open(method, url);
    }*/ else {
      xhr = null;
    }
    return xhr;
  };
}
