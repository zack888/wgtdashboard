import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SubmissionComponent } from './submission.component';
import { NewTaskComponent } from './new-task/new-task.component';
import { NewVideoComponent } from './new-video/new-video.component';

const routes: Routes = [{
  path: '',
  component: SubmissionComponent,
  children: [{
    path: 'task',
    component: NewTaskComponent,
  }, {
    path: 'viduce',
    component: NewVideoComponent,
  }],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class SubmissionRoutingModule {

}

export const routedComponents = [
  SubmissionComponent,
  NewTaskComponent,
  NewVideoComponent
];
