import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import * as Cognito from "amazon-cognito-identity-js";
import { User, UserService } from '../../../@core/data/users.service'
import { LoadingBarService } from '@ngx-loading-bar/core';
import * as validator from 'valid-url';
import { ToasterService, ToasterConfig, Toast, BodyOutputType } from 'angular2-toaster';

@Component({
  selector: 'ngx-new-task',
  styleUrls: ['./new-task.component.scss'],
  templateUrl: './new-task.component.html',
})
export class NewTaskComponent {
  showMessages: any = {};
  errors: string[] = [];
  urlerr: boolean = false;
  messages: string[] = [];
  submitted: boolean = false;

  user: User;

  submission: any = {};

  urls: LocalDataSource = new LocalDataSource();
  emails: LocalDataSource = new LocalDataSource();
  timeSelect: boolean = false;
  picSelect: boolean = false;

  picture: boolean = false;

  config: ToasterConfig;

  settings = {
    actions: false,
    hideSubHeader: true,
    columns: {
      url: {
        title: 'URL',
        type: 'string',
        width: '100%',
        filter: false,
      },
    },
  };

  rsettings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      emails: {
        title: 'Email Address',
        type: 'string',
        width: '90%',
        filter: false,
      },
    },
    hideSubHeader: false,
  };

  urlList: any[] = [
    "", "", "", "", "",
  ];

  constructor(protected userService: UserService, private loadingBar: LoadingBarService, protected router: Router, private toasterService: ToasterService) {
    if (this.userService.isLoggedOut()) {
      setTimeout(() => {
        return this.router.navigateByUrl('/auth/login');
      }, 0);
    } else {
      this.userService.session();
    }
    this.user = userService.userInterface;
    this.urls.load([]);
    this.emails.load([]);
    this.emails2.load([]);
    console.log(this.user.email);
  }

  validator = [
    'item',
    'detail',
    'spm',
    'sku',
    'itm',
    'prod'
  ];

  validurl: boolean = true;

  valid(url): boolean {
    var okay = false;
    for (const valid of this.validator) {
      if (url.indexOf(valid) > -1) {
        okay = true;
        break;
      }
    }
    return okay;
  }

  validURL(url) {
    return !validator.isUri(url);
  }

  validEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email.toLowerCase());
  }

  onURLAdd(event): void {
    if (!event.newData.url) {
      event.confirm.reject();
    } else if (this.validURL(event.newData.url)) {
      window.confirm('Your input is not an URL')
      event.confirm.reject();
    } else {
      event.confirm.resolve(event.newData);
      if (!this.valid(event.newData.url)) {
        this.validurl = false;
      }

      var urls = "";
      this.urls.getElements().then(e => {
        e.forEach(element => {
          urls = urls + element.url + ",";
        });
      });
    }
  }

  onDeleteURL(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  onEmailAdd(event): void {
    if (!event.newData.emails) {
      event.confirm.reject();
      return;
    } else if (!this.validEmail(event.newData.emails)) {
      window.confirm('Your input is not an email address')
      event.confirm.reject();
    } else if (this.emails && this.emails.count() == 4) {
      this.hideEmailInput();
      event.confirm.resolve(event.newData);
    } else {
      event.confirm.resolve(event.newData);
    }
  }

  onDeleteEmail(event): void {
    event.confirm.resolve();
    if (this.emails && this.emails.count() <= 5) {
      this.showEmailInput();
    }
  }

  hideEmailInput() {
    var set = this.rsettings;
    set.hideSubHeader = true;
    this.rsettings = Object.assign({}, set);
  }

  showEmailInput() {
    var set = this.rsettings;
    set.hideSubHeader = false;
    this.rsettings = Object.assign({}, set);
  }

  submit(form) {
    this.messages = [];
    this.errors = [];

    var emails = [this.user.email];
    if (this.emails.count() > 0) {
      this.emails.getElements().then(e => {
        e.forEach(element => {
          emails.push(element.emails);
        });

        //this.send(form, emails);
        this.send(form, this.user.email);
      })
    } else {
      //this.send(form, emails);
      this.send(form, this.user.email);
    }
  }

  isMobile(url) {
    return (url.indexOf('://m.') > -1 || url.indexOf('.m.') > -1);
  }

  send(form, emails) {
    var urls = this.serialURL();
    for (var i = 0; i < this.urlSubmission.length; i++) {
      if (this.isMobile(this.urlSubmission[i])) {
        this.errors.push('Please do not submit any mobile URL(s)');
        return;
      }
    }

    if (urls.length > 0) {
      this.loadingBar.start();
      this.submitted = true;

      var userPool = new Cognito.CognitoUserPool({
        UserPoolId: 'ap-southeast-1_7PvKAeRNz',
        ClientId: 'sa6cluje5ml19da5j49hfr2ms',
      });

      var params = {
        "id" : userPool.getCurrentUser().getUsername(),
        "title": this.submission.url,
        "type": 0,
        "email": emails,
        "url": urls,
        "time": this.timeSelect,
        "picture": this.picSelect
      }

      var url = 'https://jwzu8znvz1.execute-api.ap-southeast-1.amazonaws.com/prod/WGT_NewTask';
      var method = 'POST';
      var xhr = this.createCORSRequest(method, url);

      xhr.onload = () => {
        if (JSON.parse(xhr.responseText).message == "Success") {
          form.reset();
          this.urls.empty();
          this.urls.refresh();
          this.emails.empty();
          this.urlSubmission = [];

          this.submitted = false;
          this.showToast('success', 'Submitted successfully!', 'You may check back later and view the analysis once it\'s completed');
          this.messages.push('Please wait while we analyze the URL. You may check your tasks for the progress.');
        } else {
          this.submitted = false;
          this.showToast('error', 'Failed to submit', 'Please try again later');
          this.errors.push('Unable to process your task. Please try again later');
        }

        this.loadingBar.complete();
      };

      xhr.onerror = () => {
        console.log(xhr.responseText);
        this.submitted = false;
        this.showToast('error', 'Failed to submit', 'Please try again later');
        this.errors.push('Unable to process your task. Please try again later');

        this.loadingBar.complete();
      };

      xhr.send(JSON.stringify(params));
    } else {
      this.errors.push('Please enter at least 1 URL for analysis')
      this.showToast('error', 'Failed to submit', 'You need to enter at least 1 URL');
    }
  }

  tasks: any[] = [
    { id: false, description: 'Sort by popularity' },
    { id: true, description: 'Sort by time' },
  ];

  set(type) {
    this.timeSelect = type;
  }

  /*XDomainRequest: {
    new (): XDomainRequest;
    prototype: XDomainRequest;
    create(): XDomainRequest;
  };*/

  private showToast(type: string, title: string, body: string) {
    this.config = new ToasterConfig({
      positionClass: 'toast-top-right',
      timeout: 10000,
      newestOnTop: true,
      tapToDismiss: true,
      preventDuplicates: true,
      animation: 'slideDown',
      limit: 5,
    });
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 10000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this.toasterService.popAsync(toast);
  }

  createCORSRequest = function(method, url) {
    var xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {
      xhr.open(method, url, true);
    } /*else if (typeof XDomainRequest != "undefined") {
      // IE8 & IE9
      xhr = new XDomainRequest();
      xhr.open(method, url);
    }*/ else {
      xhr = null;
    }
    return xhr;
  };

  urlSubmission: any[] = [];

  focus(row) {
    console.log('focus');
  }

  blur(row) {
    console.log('blur');
  }

  check(): any[] {
    var list = [];
    for (var i = 0; i < this.urlList.length; i++) {
      if (this.isUrlValid(this.urlList[i])) {
        list.push(this.urlList[i]);
      } /*else {
        this.showToast('error', 'Invalid URL', 'Please make sure URL is correct');
      }*/
    }
    return list.filter(function(item, pos, self) {
      return self.indexOf(item) == pos;
    });
  }

  isUrlValid(userInput) {
    var res = userInput.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
    if(res == null)
        return false;
    else
        return true;
  }

  clean() {
    var list = this.check();
    if (this.urlSubmission.length > 0) {
      for (var i = 0; i < list.length; i++) {
        var found = false;
        for (var j = 0; j < this.urlSubmission.length; j++) {
          if (this.urlSubmission[j].url == list[i]) {
            found = true;
          }
        }
        if (!found) {
          if (!this.isMobile(list[i])) {
            this.urlSubmission.push(list[i]);
            this.urls.add({
              url: list[i]
            });
          } else {
            this.showToast('error', 'Invalid URL', 'Please do not input mobile URL');
          }
        }
      }
    } else {
      for (var i = 0; i < list.length; i++) {
        if (!this.isMobile(list[i])) {
          this.urlSubmission.push(list[i]);
          this.urls.add({
            url: list[i]
          });
        } else {
          this.showToast('error', 'Invalid URL', 'Please do not input mobile URL');
        }
      }
    }
    this.urls.refresh();

    this.urlList = ["","","","","",];
  }

  serialURL() : string {
    this.clean();
    var url = "";
    for (var i = 0; i < this.urlSubmission.length; i++) {
      url = url + this.urlSubmission[i] + ",";
    }
    return url.slice(0, -1);
  }

  addToList() {
    this.urls.getElements().then((data) => {
      var list = this.check();
      if (data.length > 0) {
        for (var i = 0; i < list.length; i++) {
          var found = false;
          for (var j = 0; j < data.length; j++) {
            if (data[j].url == list[i]) {
              found = true;
            }
          }
          if (!found) {
            if (!this.isMobile(list[i])) {
              this.urlSubmission.push(list[i]);
              this.urls.add({
                url: list[i]
              });
            } else {
              this.showToast('error', 'Invalid URL', 'Please do not input mobile URL');
            }
          }
        }
      } else {
        for (var i = 0; i < list.length; i++) {
          if (!this.isMobile(list[i])) {
            this.urlSubmission.push(list[i]);
            this.urls.add({
              url: list[i]
            });
          } else {
            this.showToast('error', 'Invalid URL', 'Please do not input mobile URL');
          }
        }
      }
      this.urls.refresh();

      this.urlList = ["","","","","",];
    });
  }

  /* SOCIAL MEDIA PAGE */

  medium: any[] = [
    { id: "facebook", description: 'Facebook' },
  ];

  submission2: any = {};
  emails2: LocalDataSource = new LocalDataSource();

  submitted2: boolean = false;
  keywords: any[] = [];

  assignedMedia = 'facebook';

  onSelect(event) {}

  set2(media) {
    this.assignedMedia = media;
  }

  submit2(form) {
    this.messages = [];
    this.errors = [];

    var emails = [this.user.email];
    if (this.emails2.count() > 0) {
      this.emails2.getElements().then(e => {
        e.forEach(element => {
          emails.push(element.emails);
        });

        //this.send(form, emails);
        this.send2(form, this.user.email);
      })
    } else {
      //this.send(form, emails);
      this.send2(form, this.user.email);
    }
  }

  send2(form, emails) {
    this.loadingBar.start();
    this.submitted = true;

    var userPool = new Cognito.CognitoUserPool({
      UserPoolId: 'ap-southeast-1_7PvKAeRNz',
      ClientId: 'sa6cluje5ml19da5j49hfr2ms',
    });

    var keyword = '';
    for (var i = 0; i < this.keywords.length; i++) {
      if (i != 0) keyword += ',';
      keyword += this.keywords[i];
    }

    var params = {
      "id" : userPool.getCurrentUser().getUsername(),
      "title": this.submission2.url,
      "type": 20,
      "email": emails,
      "social": true,
      "keywords": keyword,
      "media": this.assignedMedia
    }

    var url = 'https://jwzu8znvz1.execute-api.ap-southeast-1.amazonaws.com/prod/WGT_NewTask';
    var method = 'POST';
    var xhr = this.createCORSRequest(method, url);

    xhr.onload = () => {
      if (JSON.parse(xhr.responseText).message == "Success") {
        form.reset();
        this.emails2.empty();

        this.submitted2 = false;
        this.showToast('success', 'Submitted successfully!', 'You may check back later and view the analysis once it\'s completed');
        this.messages.push('Please wait while we analyze the URL. You may check your tasks for the progress.');
      } else {
        this.submitted2 = false;
        this.showToast('error', 'Failed to submit', 'Please try again later');
        this.errors.push('Unable to process your task. Please try again later');
      }

      this.loadingBar.complete();
    };

    xhr.onerror = () => {
      console.log(xhr.responseText);
      this.submitted2 = false;
      this.showToast('error', 'Failed to submit', 'Please try again later');
      this.errors.push('Unable to process your task. Please try again later');

      this.loadingBar.complete();
    };

    xhr.send(JSON.stringify(params));
  }

}
