import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
import { SubmissionRoutingModule, routedComponents } from './submission-routing.module';
import { ToasterModule } from 'angular2-toaster';
import { TagInputModule } from 'ngx-chips';

@NgModule({
  imports: [
    ThemeModule,
    SubmissionRoutingModule,
    Ng2SmartTableModule,
    ToasterModule,
    TagInputModule,
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class SubmissionModule { }
