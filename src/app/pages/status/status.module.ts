import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
import { StatusRoutingModule, routedComponents } from './status-routing.module';
import { SmartTableService } from '../../@core/data/smart-table.service';
import { StatusService } from '../../@core/data/status.service';
import { ChartButtonComponent } from './task-status/chart-button.component';
import { UrlmodalComponent } from './urlmodal/urlmodal.component';

const components = [
  ChartButtonComponent,
  UrlmodalComponent
];

@NgModule({
  imports: [
    ThemeModule,
    StatusRoutingModule,
    Ng2SmartTableModule,
  ],
  declarations: [
    ...routedComponents,
    ...components
  ],
  providers: [
    SmartTableService,
    StatusService,
  ],
  entryComponents: [
    ChartButtonComponent,
    UrlmodalComponent
  ]
})
export class StatusModule { }
