import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LocalDataSource } from 'ng2-smart-table';

@Component({
  selector: 'ngx-urlmodal',
  styleUrls: ['./urlmodal.component.scss'],
  templateUrl: './urlmodal.component.html',
})
export class UrlmodalComponent {

  modalHeader: string;

  settings = {
    actions: false,
    hideSubHeader: true,
    columns: {
      name: {
        title: 'URLs',
        type: 'string',
        width: '100%',
      }
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private activeModal: NgbActiveModal) { }

  public load(items) {
    this.source.load(items);
  }

  closeModal() {
    this.activeModal.close();
  }
}
