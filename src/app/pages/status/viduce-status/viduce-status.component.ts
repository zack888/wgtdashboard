import { Component } from '@angular/core';
import { LocalDataSource, Ng2SmartTableModule } from 'ng2-smart-table';
import { Router } from '@angular/router';

import { Status, StatusService } from '../../../@core/data/status.service';

import * as AWS from 'aws-sdk';
import * as Cognito from "amazon-cognito-identity-js";
import { CognitoIdentityServiceProvider } from 'amazon-cognito-identity-js';

import { dateFormat } from 'dateformat';
import { UserService } from '../../../@core/data/users.service';

import { LoadingBarService } from '@ngx-loading-bar/core';
import { ViewChild } from '@angular/core/src/metadata/di';

@Component({
  selector: 'ngx-viduce-status',
  templateUrl: './viduce-status.component.html',
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
})
export class ViduceStatusComponent {
  statusInterface: Status;

  settings = {
    actions: false,
    hideSubHeader: true,
    columns: {
      date: {
        title: 'Posted',
        type: 'string',
        width: '20%',
      },
      hoster: {
        title: 'Stream',
        type: 'string',
        width: '20%',
      },
      title: {
        title: 'Title',
        type: 'string',
        width: '40%',
      },
      stats: {
        title: 'Status',
        type: 'string',
        width: '20%',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  data = [];

  constructor(private service: StatusService, private userService: UserService, protected router: Router, private loadingBar: LoadingBarService) {
    this.statusInterface = service.statusInterface;
    if (this.userService.isLoggedOut()) {
      setTimeout(() => {
        return this.router.navigateByUrl('/auth/login');
      }, 0);
    } else {
      this.userService.session();
    }

    this.source.onChanged().subscribe((change) => {
      console.log(change);
    });
  }

  ngOnInit() {
    this.loadingBar.start();

    var userPool = new Cognito.CognitoUserPool({
      UserPoolId: 'ap-southeast-1_7PvKAeRNz',
      ClientId: 'sa6cluje5ml19da5j49hfr2ms',
    });

    AWS.config.update({
      region: 'ap-southeast-1'
    });

    var docClient = new AWS.DynamoDB.DocumentClient();

    var params = {
      TableName: "WGT_Video",
      KeyConditionExpression: "#id = :id",
      ExpressionAttributeNames: {
          "#id": "username",
      },
      ExpressionAttributeValues: {
           ":id": userPool.getCurrentUser().getUsername(),
      },
      ScanIndexForward: false
    };

    docClient.query(params, (err, data) => {
      if (err) {
        //TODO
        console.error(err);
      } else {

        for (let item of data.Items) {
          var status = "";
          if (item.stats == 100) {
            status = "Completed";
          } else if (item.status == 0) {
            status = "In queue";
          } else if (item.status < 0) {
            status = "Error";
          } else {
            status = "In progress";
          }

          this.data.push({
            id: item.timestamp,
            date: this.getDateString(item.timestamp),
            title: item.title,
            hoster: item.hoster,
            stats: status
          });
        };

        this.source.load(this.data);
      }
      this.loadingBar.complete();
    });
  }

  getDateString(timestamp: number): string {
    console.log(timestamp);

    const date = new Date(timestamp);
    var minutes = "0" + date.getMinutes();
    var seconds = "0" + date.getSeconds();
    var month = (date.getMonth() + 1).toString();

    return date.getDate() + '/' + month + '/' + date.getFullYear() + ' - ' +
      date.getHours() + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
  }
/*
  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to remove this task?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }*/
}
