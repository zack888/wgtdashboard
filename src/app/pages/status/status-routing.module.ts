import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StatusComponent } from './status.component';
import { TaskStatusComponent } from './task-status/task-status.component';
import { ViduceStatusComponent } from './viduce-status/viduce-status.component';

const routes: Routes = [{
  path: '',
  component: StatusComponent,
  children: [{
    path: 'task',
    component: TaskStatusComponent,
  },
  {
    path: 'viduce',
    component: ViduceStatusComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StatusRoutingModule { }

export const routedComponents = [
  StatusComponent,
  TaskStatusComponent,
  ViduceStatusComponent
];
