import { Component } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { Router } from '@angular/router';
import { Status, StatusService } from '../../../@core/data/status.service';
import * as AWS from 'aws-sdk';
import * as Cognito from "amazon-cognito-identity-js";
import { UserService } from '../../../@core/data/users.service';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { ChartButtonComponent } from '../task-status/chart-button.component';

@Component({
  selector: 'ngx-task-status',
  templateUrl: './task-status.component.html',
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
})
export class TaskStatusComponent {
  statusInterface: Status;

  settings = {
    actions: false,
    hideSubHeader: true,
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      /*name: {
        title: 'Analysis',
        type: 'string',
        width: '25%',
      },*/
      date: {
        title: 'Posted',
        type: 'string',
        width: '15%',
      },
      title: {
        title: 'Title',
        type: 'string',
        width: '30%',
      },
      type: {
        title: 'Type',
        type: 'string',
        width: '10%',
      },
      status: {
        title: 'Status',
        type: 'string',
        width: '10%',
      },
      url: {
        title: 'URL(s)',
        type: 'custom',
        renderComponent: ChartButtonComponent,
        onComponentInitFunction(instance) {
          instance.click.subscribe(row => {
          });
        },
        width: '10%',
        filter: false
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  data = [];

  constructor(private service: StatusService, private userService: UserService, protected router: Router, private loadingBar: LoadingBarService) {
    this.statusInterface = service.statusInterface;
    if (this.userService.isLoggedOut()) {
      setTimeout(() => {
        return this.router.navigateByUrl('/auth/login');
      }, 0);
    } else {
      this.userService.session();
    }

    this.source.onChanged().subscribe((change) => {
      console.log(change);
    });
  }

  ngOnInit() {
    this.loadingBar.start();

    var userPool = new Cognito.CognitoUserPool({
      UserPoolId: 'ap-southeast-1_7PvKAeRNz',
      ClientId: 'sa6cluje5ml19da5j49hfr2ms',
    });

    AWS.config.update({
      region: 'ap-southeast-1'
    });

    var docClient = new AWS.DynamoDB.DocumentClient();

    var params = {
      TableName: "WGTTask",
      KeyConditionExpression: "#id = :id",
      ExpressionAttributeNames: {
          "#id": "username",
      },
      ExpressionAttributeValues: {
           ":id": userPool.getCurrentUser().getUsername(),
      },
      ScanIndexForward: false
    };

    docClient.query(params, (err, data) => {
      if (err) {
        //TODO
        console.error(err);
      } else {

        for (let item of data.Items) {
          var status = "";
          if (item.status == 100) {
            status = "Completed";
          } else if (item.status == 0) {
            status = "In queue";
          } else if (item.status < 0) {
            status = "Error";
          } else {
            status = "In progress";
          }

          this.data.push({
            id: item.timestamp,
            name: item.name,
            date: this.getDateString(item.timestamp),
            //url: item.url,
            title: item.title,
            status: status,
            url: item.url,
            type: item.type.toString() == '20' ? 'Social Media' : 'eCommerce'
          });
        };

        this.source.load(this.data);
      }
      this.loadingBar.complete();
    });
  }

  getDateString(timestamp: number): string {
    console.log(timestamp);

    const date = new Date(timestamp);
    var minutes = "0" + date.getMinutes();
    var seconds = "0" + date.getSeconds();
    var month = (date.getMonth() + 1).toString();

    return date.getDate() + '/' + month + '/' + date.getFullYear() + ' - ' +
      date.getHours() + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
  }
/*
  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to remove this task?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }*/
}
