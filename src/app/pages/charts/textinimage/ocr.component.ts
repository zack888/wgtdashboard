import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import * as AWS from 'aws-sdk';
import * as Cognito from "amazon-cognito-identity-js";
import { CognitoIdentityServiceProvider } from 'amazon-cognito-identity-js';

import { dateFormat } from 'dateformat';

import { LoadingBarService } from '@ngx-loading-bar/core';
import { UserService } from '../../../@core/data/users.service';

import { ToasterService, ToasterConfig, Toast, BodyOutputType } from 'angular2-toaster';

import { LocalDataSource } from 'ng2-smart-table';

@Component({
  selector: 'ngx-ocr',
  styleUrls: ['./ocr.component.scss'],
  templateUrl: './ocr.component.html',
})

export class OcrComponent {
  tasks: any[] = [
    { id: 0, url: 'Select a completed task to view analysis', date: '' },
  ];
  curTask: any = this.tasks[0];
  analysis: any[] = [{ id: 0 }, { id: 1 }, { id: 2 }, { id: 3}];
  has = [ false, false, false, false ];
  enable = [ true, true, true, true ];
  errors = [ false, false, false, false ];
  pending = [ false, false, false, false ];
  curr = 0;
  currTaskId = 0;
  a = 0;

  empty = false;

  config: ToasterConfig;

  settings = {
    actions: false,
    hideSubHeader: true,
    columns: {
      item: {
        title: 'Text',
        type: 'string',
        width: '70%',
        filter: false
      },
      url: {
        title: 'Image',
        type: 'html',
        width: '30%',
        filter: false,
        valuePrepareFunction: (value) => {
          return `<a href="` + value + `" target="_blank"><img src="` + value +`" width="120" style="object-fit: contain;display:block;width:100%;max-width:120px" /></a>`
        }
      }
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private loadingBar: LoadingBarService, public userService: UserService,
    protected router: Router, private activatedRoute: ActivatedRoute, private toasterService: ToasterService) {
    if (this.userService.isLoggedOut()) {
      setTimeout(() => {
        this.activatedRoute.queryParams.subscribe((params: Params) => {
          let taskId = params['task'];
          if (taskId) {
            return this.router.navigateByUrl('/auth/login?page=cloud&task=' + taskId);
          } else {
            return this.router.navigateByUrl('/auth/login');
          }
        });
      }, 0);
    } else {
      this.userService.session();
    }
  }

  ngOnInit() {
    var hasTask = false;

    this.activatedRoute.queryParams.subscribe((params: Params) => {
      let taskId = params['task'];
      console.log(taskId);
      if (taskId) {
        this.view(taskId);
        hasTask = true;
      }
    });

    var userPool = new Cognito.CognitoUserPool({
      UserPoolId: 'ap-southeast-1_7PvKAeRNz',
      ClientId: 'sa6cluje5ml19da5j49hfr2ms',
    });

    if (userPool.getCurrentUser()) {
      AWS.config.update({
        region: 'ap-southeast-1'
      });

      var docClient = new AWS.DynamoDB.DocumentClient();

      var params = {
        TableName: "WGTTask",
        KeyConditionExpression: "#id = :id",
        ExpressionAttributeNames: {
            "#id": "username",
        },
        ExpressionAttributeValues: {
             ":id": userPool.getCurrentUser().getUsername(),
        },
        ScanIndexForward: false
      };

      this.loadingBar.start();

      docClient.query(params, (err, data) => {
        if (err) {
          //TODO

          this.loadingBar.complete();
        } else {
          for (let item of data.Items) {
            hasTask = true;
            if (item.status == 100) {
              this.tasks.push({
                id: this.tasks.length,
                url: item.title,
                date: this.getDateString(item.timestamp),
                timestamp: item.timestamp
              });
            }
          };

          if (!hasTask) {
            this.empty = true;
          }

          this.loadingBar.complete();
        }
      });
    }
  }

  getDateString(timestamp: number): string {
    const date = new Date(timestamp);
    var minutes = "0" + date.getMinutes();
    var seconds = "0" + date.getSeconds();
    var month = (date.getMonth() + 1).toString();

    return '[ ' + date.getDate() + '/' + month + '/' + date.getFullYear() + ' - ' +
      date.getHours() + ':' + minutes.substr(-2) + ':' + seconds.substr(-2) + ' ]';
  }

  view(task) {
    this.empty = false;

    var userPool = new Cognito.CognitoUserPool({
      UserPoolId: 'ap-southeast-1_7PvKAeRNz',
      ClientId: 'sa6cluje5ml19da5j49hfr2ms',
    });

    AWS.config.update({
      region: 'ap-southeast-1'
    });

    var docClient = new AWS.DynamoDB.DocumentClient();

    var params = {
      TableName: "WGT_Analysis",
      KeyConditionExpression: "#id = :time",
      ExpressionAttributeNames: {
          "#id": "timestamp",
      },
      ExpressionAttributeValues: {
           ":time": parseInt(task),
      }
    };

    docClient.query(params, (err, data) => {
      if (err) {
      } else {
        this.has = [ false, false, false, false];
        this.curr = -1;

        if (data.Items && data.Items[0].title) {
          this.showToast('default', 'You are viewing', data.Items[0].title);
        } else {
          this.showToast('default', 'You are viewing', 'Text in Images Example');
        }

        for (let item of data.Items) {
          if (item.data) {
            this.analysis[item.type] = item.data;
            this.has[item.type] = true;
            if (this.pending[item.type]) {
              this.pending[item.type] = false;
            }
          } else if (!this.has[item.type]) {
            this.pending[item.type] = true;
          }
        };
      }

      this.loadingBar.complete();
    });
  }

  setNewUser(id: any): void {
    this.curr = id;
    this.curTask = this.tasks.filter(value => value.id === parseInt(id));

    this.has = [ false, false, false, false ];
    this.enable = [ true, true, true, true ];
    this.errors = [ false, false, false, false ];
    this.pending = [ false, false, false, false ];

    if (id > 0) {
      this.loadingBar.start();
      this.currTaskId = this.tasks[id].timestamp;

      this.loadIntel(this.tasks[id].timestamp);
    }
  }

  private showToast(type: string, title: string, body: string) {
    this.config = new ToasterConfig({
      positionClass: 'toast-top-right',
      timeout: 10000,
      newestOnTop: true,
      tapToDismiss: true,
      preventDuplicates: true,
      animation: 'slideDown',
      limit: 5,
    });
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 10000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this.toasterService.popAsync(toast);
  }

  loadIntel(id) {
    var self = this;
    var xhr = this.createCORSRequest('GET', 'https://s3-ap-southeast-1.amazonaws.com/wgtimages/oc' + id + '.json');
    xhr.onload = () => {
      var items = JSON.parse(xhr.responseText);

      items.map(x => {
        x.item = x.item.replace('|', '\n');
        x.url = x.url;
      });

      this.source.load(items);
      self.loadingBar.complete();
    }

    xhr.onerror = () => {
      console.log(xhr.responseText);
      this.loadingBar.complete();
    };

    xhr.send();
  }

  createCORSRequest = function(method, url) {
    var xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {
      xhr.open(method, url, true);
    } else {
      xhr = null;
    }
    return xhr;
  };
}
