import { NgModule } from '@angular/core';
import { AngularEchartsModule } from 'ngx-echarts';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ChartModule } from 'angular2-chartjs';
import { ThemeModule } from '../../@theme/theme.module';
import { ChartsRoutingModule, routedComponents } from './charts-routing.module';
import { AnalysisBarComponent } from './analysis/analysis-bar.component';
import { AnalysisBarNegComponent } from './analysis/analysis-bar-neg.component';
import { AnalysisLineComponent } from './analysis/analysis-line.component';
import { AnalysisPieComponent } from './analysis/analysis-pie.component';
import { AnalysisMultipleXaxisComponent } from './analysis/analysis-multiple-xaxis.component';
import { AnalysisBarHorizontalComponent } from './analysis/analysis-bar-horizontal.component';
import { AnalysisRadarComponent } from './analysis/analysis-radar.component';
import { ModalComponent } from './monitor/modal.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { AgWordCloudModule } from 'angular4-word-cloud';
import { NgxCarouselModule } from 'ngx-carousel';
import { TreeMapModule } from '@swimlane/ngx-charts';
import { TagCloudModule } from 'angular-tag-cloud-module';
import 'hammerjs';
import { EmbedVideo } from 'ngx-embed-video';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { NgxPopperModule } from './analysis/popper/popper.module';
import { FomComponent } from './monitor/fom.component';
import { CarouleryComponent } from './analysis/caroulery/caroulery.component';
import { MentionsComponent } from './analysis/mentions/chart-mentions.component';
import { VerbamodalComponent } from './analysis/verbamodal/verbamodal.component';
import { VerbasearchComponent } from './analysis/verbasearch/verbasearch.component';
import { VisibleButtonComponent } from './analysis/mentions/visible-button.component';
import { VerbatimButtonComponent } from './analysis/mentions/verbatim-button.component';
import { UndoButtonComponent } from './analysis/mentions/undo-button.component';
import { UrlLineComponent } from './monitor/url-line.component';
import { RmUrlButtonComponent } from './monitor/url-remove.component';
import { UrlStatButtonComponent } from './monitor/url-status.component';
import { OpenButtonComponent } from './analysis/mentions/saver/open-button.component';
import { SaverComponent } from './analysis/mentions/saver/saver.component';
import { OpenerComponent} from './analysis/mentions/saver/opener.component';
import { CloudComponent } from './analysis/cloud/newcloud.component';
import { RatingComponent } from './monitor-v2/rating.component';
import { ChartUrlButtonComponent } from './monitor-v2/chart-button.component';
import { ChartEditButtonComponent } from './monitor-v2/edit-button.component';
import { ChartUrlmodalComponent } from './monitor-v2/urlmodal/urlmodal.component';
import { TreeMapComponent } from './viduce/analyses/treemap.component';
import { BubbleChartComponent } from './viduce/analyses/bubblechart.component';
import { TimelineComponent } from './viduce/analyses/timeline.component';
import { NotificationsComponent } from '../components/notifications/notifications.component';
import { ToasterModule } from 'angular2-toaster';
import { VerticalTimelineModule } from 'angular-vertical-timeline';

const components = [
  AnalysisBarComponent,
  AnalysisBarNegComponent,
  AnalysisLineComponent,
  AnalysisPieComponent,
  AnalysisMultipleXaxisComponent,
  AnalysisBarHorizontalComponent,
  AnalysisRadarComponent,
  ModalComponent,
  FomComponent,
  CarouleryComponent,
  MentionsComponent,
  VerbamodalComponent,
  VerbasearchComponent,
  VisibleButtonComponent,
  VerbatimButtonComponent,
  UndoButtonComponent,
  UrlLineComponent,
  RmUrlButtonComponent,
  UrlStatButtonComponent,
  OpenButtonComponent,
  SaverComponent,
  OpenerComponent,
  CloudComponent,
  TreeMapComponent,
  BubbleChartComponent,
  TimelineComponent,
  NotificationsComponent,
  RatingComponent,
  ChartUrlButtonComponent,
  ChartEditButtonComponent,
  ChartUrlmodalComponent
];

@NgModule({
  imports: [ ThemeModule, ChartsRoutingModule, AngularEchartsModule, NgxChartsModule, ChartModule, NgxCarouselModule, ToasterModule, TagCloudModule,
    NgxPopperModule, NgxDatatableModule, Ng2SmartTableModule, AgWordCloudModule.forRoot(), TreeMapModule, VerticalTimelineModule, EmbedVideo.forRoot(),
    MyDateRangePickerModule ],
  declarations: [...routedComponents, ...components],
  entryComponents: [ ModalComponent, VerbamodalComponent, VisibleButtonComponent, VerbatimButtonComponent, UndoButtonComponent, UrlLineComponent,
    ChartEditButtonComponent, RmUrlButtonComponent, UrlStatButtonComponent, SaverComponent, OpenerComponent, OpenButtonComponent, RatingComponent,
    ChartUrlmodalComponent, ChartUrlButtonComponent ],
})

export class ChartsModule {}
