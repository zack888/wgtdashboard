import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChartsComponent } from './charts.component';
import { AnalysisComponent } from './analysis/analysis.component';
import { AnalysisCloudComponent } from './analysis-wordcloud/analysis.component';
import { AnalysisViduceComponent } from './viduce/analysis.component';
import { AudioViduceComponent } from './viduce/audio.component';
import { ClusterComponent } from './analysis-cluster/cluster.component';
import { OcrComponent } from './textinimage/ocr.component';
import { MonitorV2Component } from './monitor-v2/monitor.component';
import { MonitorAnalysisComponent } from './monitor-v2/analysis/monitor-analysis.component';
import { MonitorListComponent } from './monitor-v2/tasklist/monitor-list.component';
import { MonitorNewComponent } from './monitor-v2/newtask/monitor-new.component';

const routes: Routes = [{
  path: '',
  component: ChartsComponent,
  children: [{
    path: 'task',
    component: AnalysisComponent,
  },
  {
    path: 'cloud',
    component: AnalysisCloudComponent,
  },
  {
    path: 'monitor',
    component: MonitorV2Component,
    children: [{
      path: '',
      redirectTo: 'view',
      pathMatch: 'full',
    }, {
      path: 'view',
      component: MonitorAnalysisComponent,
    }, {
      path: 'status',
      component: MonitorListComponent,
    }, {
      path: 'submit',
      component: MonitorNewComponent,
    }],
  },
  {
    path: 'viduce',
    component: AnalysisViduceComponent,
  },
  {
    path: 'audio',
    component: AudioViduceComponent,
  },
  {
    path: 'cluster',
    component: ClusterComponent,
  },
  {
    path: 'text',
    component: OcrComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChartsRoutingModule { }

export const routedComponents = [
  ChartsComponent,
  AnalysisComponent,
  MonitorV2Component,
  MonitorAnalysisComponent,
  MonitorListComponent,
  MonitorNewComponent,
  AnalysisCloudComponent,
  AnalysisViduceComponent,
  AudioViduceComponent,
  ClusterComponent,
  OcrComponent
];
