import { Component, ViewChild, Input } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { VisibleButtonComponent } from '../mentions/visible-button.component';
import { chroma } from 'chroma-js';
import { AgWordCloudData, AgWordCloudDirective } from 'angular4-word-cloud';

@Component({
    selector: 'ngx-cloud',
    styleUrls: ['./cloud.component.scss'],
    templateUrl: './cloud.component.html',
  })

  export class CloudComponent {
    @Input() curr = 0;
    @ViewChild('word_cloud_chart') word_cloud_chart: AgWordCloudDirective;
    words: Array<AgWordCloudData> = [];

    width = 300;
    height = 225;

    limit = 30;

    limits = [
      {
        value: 30,
        hint: 'Show top 30 terms'
      },
      {
        value: 50,
        hint: 'Show top 50 terms'
      },
      {
        value: 100,
        hint: 'Show top 100 terms'
      },
      {
        value: 200,
        hint: 'Show top 200 terms'
      },
      {
        value: -1,
        hint: 'Show all terms'
      }
    ]

    options = {
        settings: {
            minFontSize: 10,
            maxFontSize: 100,
        },
        margin: {
            top: 10,
            right: 10,
            bottom: 10,
            left: 10
        },
        labels: false
    }

    settings = {
        actions: false,
        hideSubHeader: true,
        columns: {
          name: {
            title: 'Term',
            type: 'string',
            width: '70%',
            filter: false,
            sort: false,

          },
          visible: {
            title: 'Visible',
            type: 'custom',
            width: '30%',
            filter: false,
            sort: false,
            renderComponent: VisibleButtonComponent,
            onComponentInitFunction: (instance: any) => {
            instance.click.subscribe(row => {
                this.refresh(row);
            });
            },
          },
        },
    };

    analysis: any[] = [];
    holding: any[] = [];
    source: LocalDataSource = new LocalDataSource();

  public start(data) {
    this.words = [];
    this.words.length = 0;
    this.holding = [];
    this.holding.length = 0;

    this.width = window.innerWidth * 0.45;
    this.height = this.width * 0.75;

    this.analysis = [];
    this.analysis.length = 0;

    var total = 0;

    if (this.limit < data.length && this.limit > 0) {
      total = this.limit;
    } else {
      total = data.length;
    }

    for (var i = 0; i < data.length; i++) {
      var visible = false;

      if (i < total) {
        visible = true;

        this.words.push({
          text: data[i].name,
          size: (parseInt(data[i].value) + 6) * 2,
          color: this.color(parseFloat(data[i].sentiment))
        });
      } else {
        this.holding.push({
          text: data[i].name,
          size: (parseInt(data[i].value) + 6) * 2,
          color: this.color(parseFloat(data[i].sentiment))
        });
      }

      this.analysis.push({
        name: data[i].name,
        mention: data[i].value,
        negative: data[i].secondary,
        sentiment: data[i].sentiment,
        position: i,
        visible: visible,
      });
    }

    this.source.load(this.analysis);

    setTimeout(() => {
      this.word_cloud_chart.update();
    });
  }

  color(value) : string {
      if (value > 0.6) return '#42db7d'
      else if (value > 0.2) return '##71B787'
      else if (value > -0.2) return '#A19392'
      else if (value > -0.6) return '#D06F9C'
      else return '#ff4ba6'
  }

  set(selection) {
    this.limit = selection;
    if (this.limit < 0) {
      this.limit = this.analysis.length;
    }
    this.restart();
  }

  restart() {
    if (this.limit > this.words.length && this.holding.length > 0) {
      // Top up word cloud
      var temp = [];
      var diff = this.limit - this.words.length;

      for (var i = 0; i < this.holding.length; i++) {
        if (diff > 0) {
          this.words.push(this.holding.shift());
          temp.push(this.words[this.words.length-1]);
          diff -= 1;
        } else {
          break;
        }
      }

      for (var i = 0; i < this.analysis.length; i++) {
        if (temp.length > 0) {
          if (this.analysis[i].name == temp[0].text) {
            this.analysis[i].visible = true;
            temp.shift();
          }
        }
      }

      setTimeout(() => {
        this.word_cloud_chart.update();
        this.source.refresh();
      });
    } else if (this.holding.length > 0) {
      // Remove terms
      var temp = [];
      var diff = this.words.length - this.limit;

      for (var i = this.words.length-1; i >= 0; i++) {
        if (diff > 0) {
          this.holding.unshift(this.words.pop());
          temp.unshift(this.holding[0]);
          diff -= 1;
        } else {
          break;
        }
      }

      for (var i = 0; i < this.analysis.length; i++) {
        if (temp.length > 0) {
          if (this.analysis[i].name == temp[0].text) {
            this.analysis[i].visible = false;
            temp.shift();
          }
        }
      }

      setTimeout(() => {
        this.word_cloud_chart.update();
        this.source.refresh();
      });
    }
  }

  refresh(row) {
    if (row.visible) {
      var newArray = [...this.words];
      this.words = [];
      this.words.length = 0;
      newArray.push({
          text: row.name,
          size: row.mention,
          color: this.color(parseFloat(row.sentiment))
      });

      this.words.push(...newArray);

      setTimeout(() => {
          this.word_cloud_chart.update();
      });

    } else {
        for (var i = 0; i < this.words.length; i++) {
            if (this.words[i].text == row.name) {
                this.words.splice(i, 1);
                break;
            }
        }

        if (this.words.length < this.limit && this.holding.length > 0) {
          this.words.push(this.holding.shift());
        }

        for (var i = 0; i < this.analysis.length; i++) {
          if (this.analysis[i].name == this.words[this.words.length-1].text) {
            this.analysis[i].visible = true;
            break;
          }
        }

        setTimeout(() => {
            this.word_cloud_chart.update();
            this.source.refresh();
        });
    }
  }
}
