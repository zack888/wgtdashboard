import { Component, OnDestroy, Input, SimpleChanges, ElementRef, OnInit, Output, EventEmitter } from '@angular/core';
import { NbThemeService, NbColorHelper } from '@nebular/theme';
import { UserService } from '../../../@core/data/users.service';

import * as AWS from 'aws-sdk';
import * as Cognito from "amazon-cognito-identity-js";
import { CognitoIdentityServiceProvider } from 'amazon-cognito-identity-js';

@Component({
  selector: 'ngx-analysis-bar-neg',
  template: `
    <chart id="analysisBar" type="bar" [data]="data" [options]="options"></chart>
  `,
})

export class AnalysisBarNegComponent implements OnDestroy {
  data: any;
  options: any;
  themeSubscription: any;

  label = ['Demo', 'Demo', 'Demo', 'Demo', 'Demo', 'Demo', 'Demo', 'Demo', 'Demo', 'Demo'];
  negative = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

  @Input() analysis: any;
  @Input() opacity: any;
  @Output() update = new EventEmitter<any>();

  colors: any;

  constructor(private theme: NbThemeService, private element: ElementRef, private userService: UserService) {
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {
      const colors: any = config.variables;
      const chartjs: any = config.variables.chartjs;

      var self = this;
      this.colors = colors;

        this.data = {
          labels: ['Demo', 'Demo', 'Demo', 'Demo', 'Demo', 'Demo', 'Demo'],
          datasets: [{
            data: [0, 0, 0, 0, 0, 0, 0],
            label: 'Percentage of Negatives',
            backgroundColor: 'rgba(231, 76, 60, 1)',
          }],
        };

        this.options = {
          maintainAspectRatio: false,
          responsive: true,
          legend: {
            display: false,
            labels: {
              fontColor: chartjs.textColor,
            },
          },
          scales: {
            xAxes: [
              {
                gridLines: {
                  display: false,
                  color: chartjs.axisLineColor,
                },
                ticks: {
                  fontColor: chartjs.textColor,
                  fontSize: 24,
                  callback: function(tick) {
                    var characterLimit = 4;
                    if (tick.length >= characterLimit) {
                        return tick.slice(0, tick.length).substring(0, characterLimit - 1).trim() + '…';
                    }
                    return tick;
                  }
                },
                barPercentage: 0.5,
              }
            ],
            yAxes: [
              {
                gridLines: {
                  display: false,
                  color: chartjs.axisLineColor,
                },
                ticks: {
                  fontColor: chartjs.textColor,
                  beginAtZero: true,
                  steps: 50,
                  min: 0,
                  max: 100,
                  stepSize: 20,
                },
                scaleLabel: {
                  labelString: 'Negatives (%)',
                  display: true,
                }
              },
            ],
          },

          tooltips: {
            callbacks: {
                title: function(tooltipItem){
                    return this._data.labels[tooltipItem[0].index];
                }
            }
          },

          onClick:
          function(e) {
            var element = this.getElementAtEvent(e);
            if (element.length) {
              self.update.emit({
                index: element[0]._index
              });

              this.data.datasets[0].data.splice(element[0]._index, 1);
              this.data.labels.splice(element[0]._index, 1);

              this.update();
            }
          },
        };
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.analysis.length > 0) {
      this.label = [];
      this.negative = [];

      for (let element of this.analysis) {
        this.label.push(element.name);
        this.negative.push(element.negative);
      }

      this.data = {
        labels: this.label,
        datasets: [{
          data: this.negative,
          label: 'Percentage of Negatives',
          backgroundColor: 'rgba(231, 76, 60, 1)',
        }],
      };
    } else {
      this.data = {
        labels: [],
        datasets: [{
          data: [],
          label: 'Percentage of Negatives',
          backgroundColor: 'rgba(231, 76, 60, 1)',
        }],
      };
    }
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }
}
