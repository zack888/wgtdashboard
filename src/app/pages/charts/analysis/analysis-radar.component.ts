import { Component, OnDestroy, Input, OnChanges } from '@angular/core';
import { NbThemeService, NbColorHelper } from '@nebular/theme';

@Component({
  selector: 'ngx-analysis-radar',
  template: `
    <chart type="radar" [data]="data" [options]="options"></chart>
  `,
})
export class AnalysisRadarComponent implements OnDestroy {
  options: any;
  data: {};
  themeSubscription: any;

  @Input() analysis: any;
  colors: any;

  constructor(private theme: NbThemeService) {
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {

      this.colors = config.variables;
      const chartjs: any = config.variables.chartjs;

      this.data = {
        labels: ['Demo', 'Demo', 'Demo'],
        datasets: [{
          data: [65, 59, 90],
          label: 'Confidence',
          borderColor: this.colors.danger,
          backgroundColor: NbColorHelper.hexToRgbA(this.colors.dangerLight, 0.5),
        }],
      };

      this.options = {
        responsive: true,
        maintainAspectRatio: false,
        scaleFontColor: 'white',
        legend: {
          labels: {
            fontColor: chartjs.textColor,
          },
        },
        scale: {
          pointLabels: {
            fontSize: 16,
            fontColor: chartjs.textColor,
          },
          gridLines: {
            color: chartjs.axisLineColor,
          },
          angleLines: {
            color: chartjs.axisLineColor,
          },
        },
      };
    });
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }

  ngOnChanges(...args: any[]) {
    if (this.analysis) {
      let items: any[] = this.analysis.videos;
      var data = [];
      var labels = [];
      var size = 3;
      if (items.length > 3) {
        size = items.length;
      }
      for (var i = 0; i < size; i++) {
        if (i < items.length) {
          for (let segment of items[i].data) {
            labels.push(items[i].description);
            var value = 0;
            value = segment.confidence * 100;
            data.push(value);
          }
        } else {
          labels.push(" ");
          data.push(0);
        }
      }

      this.data = {
        labels: labels,
        datasets: [{
          data: data,
          label: 'Confidence',
          borderColor: this.colors.danger,
          backgroundColor: NbColorHelper.hexToRgbA(this.colors.dangerLight, 0.5),
        }],
      };
    }
  }
}
