import { Component, ViewChild, Input } from '@angular/core';
import { NgxCarousel } from 'ngx-carousel';
import { PopperContent } from '../popper/popper-content';

@Component({
  selector: 'ngx-caroulery',
  styleUrls: ['./caroulery.component.scss'],
  templateUrl: './caroulery.component.html',
})

export class CarouleryComponent {
  @Input() internalTileItems: Array<any>;

  public carouselTileItems: Array<any>;
  public carouselTile: NgxCarousel;
  //carouselFirstLoadedIndex: number = 0;
  carouselLastLoadedIndex: number = 0;
  carouselLastEventIndex: number = 0;
  carouselFilterMode = false;
  carouselPage = 1;
  carouselMaxPage = 1;

  filterInput: string;
  excludeInput: string;

  hasItems = false;

  tasks1: any[] = [
    { id: 'or', description: 'OR' },{ id: 'and', description: 'AND' },
  ];

  /*tasks2: any[] = [
    { id: 'or', description: 'Or' },{ id: 'and', description: 'And' },
  ];*/

  includeAnd = 'or';
  //excludeAnd = 'or';

  @ViewChild('popper2Content') popper: PopperContent;
  @ViewChild('carousel') carousel;

  ngOnInit() {
    this.carouselTileItems = [];
    this.internalTileItems = [];//this.carouselTileItems;

    this.carouselTile = {
      grid: {xs: 3, sm: 3, md: 3, lg: 3, all: 0},
      speed: 400,
      animation: 'lazy',
      point: {
        visible: false,
      },
      load: 3,
      touch: false,
      easing: 'ease',
    }
  }

  //carouselLoader = 0;

  loadPrev() {
    /*if (!this.carouselFilterMode) {
      for (var i = 0; i < this.carouselLoader; i++) {
        if (this.carouselFirstLoadedIndex - i >= 0) {
          //this.carouselTileItems.unshift(this.internalTileItems[this.carouselFirstLoadedIndex-1]);
          //this.carouselTileItems.splice(0, 0, this.internalTileItems[this.carouselFirstLoadedIndex-1]);
          this.carouselTileItems.pop();
          this.carouselLastLoadedIndex--;
          this.carouselFirstLoadedIndex--;
        }
      }
    }
    this.carouselLoader = 3;*/
    this.carouselPage--;
  }

  loadNext() {
    //this.carouselLoader = 0;
    if (!this.carouselFilterMode) {
      for (var i = 0; i < 3; i++) {
        if (i+this.carouselLastLoadedIndex < this.internalTileItems.length) {
          this.carouselTileItems.push(this.internalTileItems[this.carouselLastLoadedIndex]);
          //this.carouselTileItems.shift();
          this.carouselLastLoadedIndex++;
          //this.carouselFirstLoadedIndex++;
          //this.carouselLoader++;
        }
      }
    }
    this.carouselPage++;
  }

  public carouselTileLoad(evt: any) {}

  public reset() {
    this.carousel.moveTo(0);
    this.carouselPage = 1;
    this.carouselTileItems = [];
  }

  init() {
    this.carouselLastLoadedIndex = 0;
    this.carouselLastEventIndex = 0;
  }

  load(items) {
    this.hasItems = true;
    for (let element of items) {
      if (this.carouselLastLoadedIndex > 0) {
        this.carousel.moveTo(0);
      }

      if (this.carouselTileItems.length < 9) {
        this.carouselTileItems.push({
          id: element.id,
          label: element.label,
          img: element.img
        });
        this.carouselLastLoadedIndex++;
      }
      this.internalTileItems.push({
        id: element.id,
        label: element.label,
        img: element.img
      });

      this.carouselMaxPage = Math.ceil(this.internalTileItems.length / 3);
    }

    /*items.forEach(element => {
      if (this.carouselLastLoadedIndex > 0) {
        this.carousel.moveTo(0);
      }

      if (this.carouselTileItems.length < 9) {
        this.carouselTileItems.push({
          id: element.id,
          label: element.label,
          img: element.img
        });
        this.carouselLastLoadedIndex++;
      }
      this.internalTileItems.push({
        id: element.id,
        label: element.label,
        img: element.img
      });

      this.carouselMaxPage = Math.ceil(this.internalTileItems.length / 3);
    });*/
  }

  cleanIn(): any[] {
    if (this.filterInput) {
      return this.filterInput.toLowerCase().trim().split(/,|、|，/);
    } else {
      return [];
    }
  }

  cleanEx(): any[] {
    if (this.excludeInput) {
      return this.excludeInput.toLowerCase().trim().split(/,|、|，/);
    } else {
      return [];
    }
  }

  include(event) {
    this.filterInput = event;
    this.filter();
  }

  exclude(event) {
    this.excludeInput = event;
    this.filter();
  }

  setIn(event) {
    this.includeAnd = event;
    this.filter();
  }

  /*setEx(event) {
    this.excludeAnd = event;
    this.filter();
  }*/

  public filter() {
    if (this.hasItems) {
      this.carousel.moveTo(0);
      this.carouselPage = 1;
      this.carouselTileItems = [];
      var include = this.cleanIn();
      var exclude = this.cleanEx();
      if (!this.filterInput && !this.excludeInput) {
        this.carouselTileItems = [];
        for (var i = 0; i < 9; i++) {
          this.carouselTileItems.push(this.internalTileItems[i]);
        }
        this.carouselFilterMode = false;
        this.carouselMaxPage = Math.ceil(this.internalTileItems.length / 3);
      } else {
        var holding = 0;
        this.internalTileItems.forEach(element => {
          var allow = true;

          if (include.length > 0) {
            if (this.includeAnd == 'and') {
              for (var j = 0; j < include.length; j++) {
                if (include[j] && element.label.toLowerCase().indexOf(include[j].toLowerCase()) < 0) {
                  allow = false;
                }
              }
            } else {
              for (var j = 0; j < include.length; j++) {
                var found = false;
                if (include[j] && element.label.toLowerCase().indexOf(include[j].toLowerCase()) >= 0) {
                  found = true;
                }
              }

              if (found) {
                allow = true;
              } else {
                allow = false;
              }
            }
          }

          for (var j = 0; j < exclude.length; j++) {
            if (exclude[j] && element.label.toLowerCase().indexOf(exclude[j].toLowerCase()) >= 0) {
              allow = false;
            }
          }

          /*if (exclude.length > 0) {
            if (this.excludeAnd == 'and') {
              for (var j = 0; j < exclude.length; j++) {
                if (exclude[j] && element.label.toLowerCase().indexOf(exclude[j].toLowerCase()) >= 0) {
                  allow = false;
                }
              }
            } else {
              for (var j = 0; j < exclude.length; j++) {
                var ban = false;
                if (exclude[j] && element.label.toLowerCase().indexOf(exclude[j]) >= 0) {
                  ban = true;
                }
              }

              if (ban) {
                allow = false;
              }
            }
          }*/

          if (allow) {
            this.carouselTileItems.push(element);
            holding++;
            this.carouselMaxPage = Math.ceil(holding / 3);
          }
        });
        this.carouselFilterMode = true;
      }
    }
  }

  showingComment = "";
  setShowingComment(comment) {
    this.showingComment = comment;
    //this.popper.show();
  }

  hideShowingComment() {
    //this.popper.hide();
  }

  trimComment(comment) {
    if (comment) {
      if (comment.length > 12) {
        return comment.substr(0, 12) + '...';
      } else {
        return comment;
      }
    } else {
      return '';
    }
  }
}
