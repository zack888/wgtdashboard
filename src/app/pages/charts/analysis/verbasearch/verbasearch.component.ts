import { Component, ViewChild, Input, SimpleChanges } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

@Component({
  selector: 'ngx-verbasearch',
  styleUrls: ['./verbasearch.component.scss'],
  templateUrl: './verbasearch.component.html',
})

export class VerbasearchComponent {
  @Input() verbatim: any[];
  display: any[];
  filterInput: any;
  excludeInput: any;

  includeAnd = 'or';
  //excludeAnd = 'or';

  settings = {
    actions: false,
    hideSubHeader: true,
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      name: {
        title: 'Comments',
        type: 'string',
        width: '100%',
      }
    },
  };

  source: LocalDataSource = new LocalDataSource();

  tasks1: any[] = [
    { id: 'or', description: 'OR' },{ id: 'and', description: 'AND' },
  ];

  /*tasks2: any[] = [
    { id: 'or', description: 'Or' },{ id: 'and', description: 'And' },
  ];*/

  ngOnChanges(changeRecord: SimpleChanges) {
    var list = [];
    for (var i = 0; i < this.verbatim.length; i++) {
      for (var key in this.verbatim[i]) {
        var vals = this.verbatim[i][key];
        for (var j = 0; j < vals.length; j++) {
          var found = false;
          for (var k = 0; k < list.length; k++) {
            if (list[k].name == vals[j]) {
              found = true;
            }
          }
          if (!found) {
            list.push({
              name: vals[j]
            });
          }
        }
      }
    }
    this.display = list;
    this.source.load(list);
  }

  cleanIn(): any[] {
    if (this.filterInput) {
      return this.filterInput.toLowerCase().trim().split(/,|、|，/);
    } else {
      return [];
    }
  }

  cleanEx(): any[] {
    if (this.excludeInput) {
      return this.excludeInput.toLowerCase().trim().split(/,|、|，/);
    } else {
      return [];
    }
  }

  include(event) {
    if (this.filterInput) {
      this.filterInput = event;
      this.filter();
    } else {
      this.source.load(this.display);
    }
  }

  exclude(event) {
    if (this.excludeInput) {
      this.excludeInput = event;
      this.filter();
    } else {
      this.source.load(this.display);
    }
  }

  setIn(event) {
    this.includeAnd = event;
    this.filter();
  }

  /*setEx(event) {
    this.excludeAnd = event;
    this.filter();
  }*/

  filter() {
    var list = [];
    var include = this.cleanIn();
    var exclude = this.cleanEx();

    for (var i = 0; i < this.display.length; i++) {
      var allow = true;

      if (include.length > 0) {
        if (this.includeAnd == 'and') {
          for (var j = 0; j < include.length; j++) {
            if (include[j] && this.display[i].name.toLowerCase().indexOf(include[j].toLowerCase()) < 0) {
              allow = false;
            }
          }
        } else {
          for (var j = 0; j < include.length; j++) {
            var found = false;
            if (include[j] && this.display[i].name.toLowerCase().indexOf(include[j].toLowerCase()) >= 0) {
              found = true;
            }
          }

          if (found) {
            allow = true;
          } else {
            allow = false;
          }
        }
      }

      /*if (exclude.length > 0) {
        if (this.excludeAnd == 'and') {
          for (var j = 0; j < exclude.length; j++) {
            if (exclude[j] && this.display[i].name.toLowerCase().indexOf(exclude[j]) >= 0) {
              allow = false;
            }
          }
        } else {
          for (var j = 0; j < exclude.length; j++) {
            var ban = false;
            if (exclude[j] && this.display[i].name.toLowerCase().indexOf(exclude[j]) >= 0) {
              ban = true;
            }
          }

          if (ban) {
            allow = false;
          }
        }
      }*/

      for (var j = 0; j < exclude.length; j++) {
        if (exclude[j] && this.display[i].name.toLowerCase().indexOf(exclude[j]) >= 0) {
          allow = false;
        }
      }

      if (allow) {
        list.push(this.display[i]);
      }
    }

    this.source.load(list);
  }
}
