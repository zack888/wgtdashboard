import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LocalDataSource } from 'ng2-smart-table';

@Component({
  selector: 'ngx-verbamodal',
  styleUrls: ['./verbamodal.component.scss'],
  templateUrl: './verbamodal.component.html',
})
export class VerbamodalComponent {

  modalHeader: string;

  settings = {
    actions: false,
    hideSubHeader: true,
    columns: {
      name: {
        title: 'Comments',
        type: 'string',
        width: '100%',
      }
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private activeModal: NgbActiveModal) { }

  public load(name, items) {
    this.modalHeader = name;
    this.source.load(items);
  }

  closeModal() {
    this.activeModal.close();
  }
}
