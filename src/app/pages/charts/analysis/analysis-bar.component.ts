import { Component, OnDestroy, Input, SimpleChanges, ElementRef, OnInit, Output, EventEmitter } from '@angular/core';
import { NbThemeService, NbColorHelper } from '@nebular/theme';
import { UserService } from '../../../@core/data/users.service';

import * as AWS from 'aws-sdk';
import * as Cognito from "amazon-cognito-identity-js";
import { CognitoIdentityServiceProvider } from 'amazon-cognito-identity-js';

@Component({
  selector: 'ngx-analysis-bar',
  template: `
    <chart id="analysisBar" type="bar" [data]="data" [options]="options"></chart>
  `,
})

export class AnalysisBarComponent implements OnDestroy {
  data: any;
  options: any;
  themeSubscription: any;

  label = ['Demo', 'Demo', 'Demo', 'Demo', 'Demo', 'Demo', 'Demo', 'Demo', 'Demo', 'Demo'];
  mention = [65, 59, 80, 81, 56, 55, 40, 56, 55, 40];

  @Input() analysis: any;
  @Output() update = new EventEmitter<any>();

  colors: any;

  constructor(private theme: NbThemeService, private element: ElementRef, private userService: UserService) {
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {
      const colors: any = config.variables;
      const chartjs: any = config.variables.chartjs;

      var self = this;
      this.colors = colors;

      this.data = {
        labels: ['Demo', 'Demo', 'Demo', 'Demo', 'Demo', 'Demo', 'Demo'],
        datasets: [{
          data: [28, 48, 40, 19, 86, 27, 90],
          label: 'Percentage of Negatives',
          backgroundColor: NbColorHelper.hexToRgbA(colors.infoLight, 0.8),
        }],
      };

      this.options = {
        maintainAspectRatio: false,
        responsive: true,
        legend: {
          display: false,
          labels: {
            fontColor: chartjs.textColor,
          },
        },
        scales: {
          xAxes: [
            {
              gridLines: {
                display: false,
                color: chartjs.axisLineColor,
              },
              ticks: {
                fontColor: chartjs.textColor,
                fontSize: 24,
                callback: function(tick) {
                  var characterLimit = 4;
                  if (tick.length >= characterLimit) {
                      return tick.slice(0, tick.length).substring(0, characterLimit - 1).trim() + '…';
                  }
                  return tick;
                }
              },
              scaleLabel: {
                labelString: 'Click on bar to hide',
                display: true,
              }
            }
          ],
          yAxes: [
            {
              gridLines: {
                display: true,
                color: chartjs.axisLineColor,
              },
              ticks: {
                fontColor: chartjs.textColor,
                beginAtZero: true,
                steps: 10,
              },
              scaleLabel: {
                labelString: 'Mentions (%)',
                display: true,
              }
            },
          ],
        },

        tooltips: {
          callbacks: {
              title: function(tooltipItem){
                  return this._data.labels[tooltipItem[0].index];
              }
          }
        },

        onClick:
        function(e) {
          var element = this.getElementAtEvent(e);
          if (element.length) {
            self.update.emit({
              index: element[0]._index
            });

            this.data.datasets[0].data.splice(element[0]._index, 1);
            this.data.labels.splice(element[0]._index, 1);

            this.update();
          }
        },
      };
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.analysis.length > 0) {
      this.label = [];
      this.mention = [];

      for (let element of this.analysis) {
        this.label.push(element.name);
        this.mention.push(element.mention);
      }

      this.data = {
        labels: this.label,
        datasets: [{
          data: this.mention,
          label: 'Percentage of Mention',
          backgroundColor: NbColorHelper.hexToRgbA(this.colors.infoLight, 0.8),
        }],
      };
    } else {
      this.data = {
        labels: [],
        datasets: [{
          data: [],
          label: 'Percentage of Negatives',
          backgroundColor: 'rgba(231, 76, 60, 1)',
        }],
      };
    }
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }
}
