import { Component, ViewChild, Input, SimpleChanges, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ngx-verbutton',
  template: `
    <button class="btn btn-info btn-tn" style="height: 30px;" (click)="onClick()"><i class="ion-navicon-round" ></i></button>
  `,
})

export class VerbatimButtonComponent {
  @Input() value;
  @Input() rowData: any;
  @Output() click: EventEmitter<any> = new EventEmitter();

  onClick() {
    this.click.emit(this.rowData);
  }
}
