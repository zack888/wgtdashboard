import { Component, ViewChild, Input, SimpleChanges, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ngx-visible',
  template: `
    <button *ngIf="rowData.visible" class="btn btn-success btn-tn" style="height: 30px;" (click)="onClick(false)"><i class="ion-checkmark" ></i></button>
    <button *ngIf="!rowData.visible" class="btn btn-danger btn-tn" style="height: 30px;" (click)="onClick(true)"><i class="ion-close-round" ></i></button>
  `,
})

export class VisibleButtonComponent {
  @Input() value;
  @Input() rowData: any;
  @Output() click: EventEmitter<any> = new EventEmitter();

  onClick(visible) {
    this.rowData.visible = visible;
    this.click.emit(this.rowData);
  }
}
