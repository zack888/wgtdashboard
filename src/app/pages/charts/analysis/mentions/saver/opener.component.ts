import { Component, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LocalDataSource } from 'ng2-smart-table';

import * as AWS from 'aws-sdk';
import * as Cognito from "amazon-cognito-identity-js";
import { CognitoIdentityServiceProvider } from 'amazon-cognito-identity-js';

import { LoadingBarService } from '@ngx-loading-bar/core';
import { OpenButtonComponent } from './open-button.component';
import { RmUrlButtonComponent } from '../../../monitor/url-remove.component';

@Component({
  selector: 'ngx-opener',
  templateUrl: './opener.component.html',
})
export class OpenerComponent {
  modalHeader: string;
  update: EventEmitter<any> = new EventEmitter();

  settings = {
    actions: false,
    hideSubHeader: true,
    columns: {
      time: {
        title: 'Last Saved',
        type: 'string',
        width: '30%',
      },
      title: {
        title: 'Title',
        type: 'string',
        width: '70%',
      },
      open: {
        title: 'Open',
        type: 'custom',
        filter: false,
        sort: false,
        renderComponent: OpenButtonComponent,
        onComponentInitFunction: (instance: any) => {
          instance.click.subscribe(row => {
            this.select(row);
          });
        },
      },
      delete: {
        title: 'Delete',
        type: 'custom',
        filter: false,
        sort: false,
        renderComponent: RmUrlButtonComponent,
        onComponentInitFunction: (instance: any) => {
          instance.click.subscribe(row => {
            this.delete(row);
          });
        },
      }
    },
  };

  source: LocalDataSource = new LocalDataSource();

  items: any[] = [];
  task = 0;

  constructor(private activeModal: NgbActiveModal, private loadingBar: LoadingBarService) {}

  load(task) {
    this.loadingBar.start();

    this.task = task;

    var userPool = new Cognito.CognitoUserPool({
      UserPoolId: 'ap-southeast-1_7PvKAeRNz',
      ClientId: 'sa6cluje5ml19da5j49hfr2ms',
    });

    AWS.config.update({
      region: 'ap-southeast-1'
    });

    var docClient = new AWS.DynamoDB.DocumentClient();

    var params = {
      TableName: "WGT_Save",
      KeyConditionExpression: "#id = :id",
      ExpressionAttributeNames: {
          "#id": "username"
      },
      ExpressionAttributeValues: {
           ":id": userPool.getCurrentUser().getUsername() + this.task
      },
      ScanIndexForward: false
    };

    docClient.query(params, (err, data) => {
      this.loadingBar.complete();
      if (err) {
        //TODO
      } else {
        this.items = data.Items;
        for (var i = 0; i < this.items.length; i++) {
          this.items[i].time = this.getDateString(this.items[i].timestamp);
        }
        this.source.load(this.items);
      }
    });
  }

  getDateString(timestamp: number): string {
    const date = new Date(timestamp);
    var minutes = "0" + date.getMinutes();
    var seconds = "0" + date.getSeconds();
    var month = (date.getMonth() + 1).toString();

    return date.getDate() + '/' + month + '/' + date.getFullYear() + ' - ' +
      date.getHours() + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
  }

  select(row) {
    this.loadingBar.start();

    var self = this;
    var xhr = this.createCORSRequest('GET', 'https://s3-ap-southeast-1.amazonaws.com/wgtsaves/' + row.pathname);
    xhr.onload = () => {
      self.loadingBar.complete();
      console.log(JSON.parse(xhr.responseText));
      this.update.emit(JSON.parse(xhr.responseText));
      this.closeModal();
    }

    xhr.onerror = () => {
      console.log(xhr.responseText);
      this.loadingBar.complete();
    };

    xhr.send();
  }

  delete(row) {
    this.loadingBar.start();

    var url = 'https://jwzu8znvz1.execute-api.ap-southeast-1.amazonaws.com/prod/WGT_RemoveSave';
    var method = 'POST';
    var xhr = this.createCORSRequest(method, url);

    xhr.onload = () => {
      if (JSON.parse(xhr.responseText).message == "Success") {
        this.source.remove(row);
      }

      this.loadingBar.complete();
    };

    xhr.onerror = () => {
      console.log(xhr.responseText);
      this.loadingBar.complete();
    };

    xhr.send(JSON.stringify(row));
  }

  closeModal() {
    this.activeModal.close();
  }

  createCORSRequest = function(method, url) {
    var xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {
      xhr.open(method, url, true);
    } /*else if (typeof XDomainRequest != "undefined") {
      // IE8 & IE9
      xhr = new XDomainRequest();
      xhr.open(method, url);
    }*/ else {
      xhr = null;
    }
    return xhr;
  };
}
