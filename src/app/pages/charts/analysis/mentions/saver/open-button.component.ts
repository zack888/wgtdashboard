import { Component, ViewChild, Input, SimpleChanges, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ngx-opensave',
  template: `
    <button class="btn btn-success btn-tn" style="height: 30px;" (click)="onClick()" [disabled]="!enable"><i class="ion-arrow-right-b"></i></button>
  `,
})

export class OpenButtonComponent {
  @Input() value;
  @Input() rowData: any;
  @Output() click: EventEmitter<any> = new EventEmitter();
  enable = true;

  onClick() {
    this.enable = false;
    this.click.emit(this.rowData);
  }
}
