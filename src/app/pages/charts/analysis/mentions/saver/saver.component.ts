import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LocalDataSource } from 'ng2-smart-table';

import * as AWS from 'aws-sdk';
import * as Cognito from "amazon-cognito-identity-js";
import { CognitoIdentityServiceProvider } from 'amazon-cognito-identity-js';

import { LoadingBarService } from '@ngx-loading-bar/core';

@Component({
  selector: 'ngx-saver',
  templateUrl: './saver.component.html',
})
export class SaverComponent {
  modalHeader: string;
  submission: any;
  data: any[] = [];
  action: any[] = [];
  sort: any[] = [];
  page = 1;
  task = 0;
  submitted = false;
  messages = [];
  errors = [];

  constructor(private activeModal: NgbActiveModal, private loadingBar: LoadingBarService) { }

  load(data, action, sort, page, task) {
    this.data = data;
    this.action = action;
    this.sort = sort;
    this.page = page;
    this.task = task;
  }

  submit(form) {
    this.messages = [];
    this.errors = [];

    this.loadingBar.start();
    this.submitted = true;

    var submitting = this.submission;

    var userPool = new Cognito.CognitoUserPool({
      UserPoolId: 'ap-southeast-1_7PvKAeRNz',
      ClientId: 'sa6cluje5ml19da5j49hfr2ms',
    });

    var params = {
      "username" : userPool.getCurrentUser().getUsername(),
      "data": this.data,
      "action": this.action,
      "sort": this.sort,
      "page": this.page,
      "title": this.submission,
      "task": this.task
    }

    var url = 'https://jwzu8znvz1.execute-api.ap-southeast-1.amazonaws.com/prod/WGT_SaveAnalysis';
    var method = 'POST';
    var xhr = this.createCORSRequest(method, url);

    xhr.onload = () => {
      form.reset();
      if (JSON.parse(xhr.responseText).message == "Success") {
        this.messages.push('Your analysis has been saved!');
      } else {
        console.log(xhr.responseText);
        this.submitted = false;
        this.errors.push('Unable to save. Please try again later');
      }

      this.loadingBar.complete();
    };

    xhr.onerror = () => {
      console.log(xhr.responseText);
      this.submitted = false;
      this.errors.push('Unable to save. Please try again later');

      this.loadingBar.complete();
    };

    xhr.send(JSON.stringify(params));
  }

  closeModal() {
    this.activeModal.close();
  }

  createCORSRequest = function(method, url) {
    var xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {
      xhr.open(method, url, true);
    } /*else if (typeof XDomainRequest != "undefined") {
      // IE8 & IE9
      xhr = new XDomainRequest();
      xhr.open(method, url);
    }*/ else {
      xhr = null;
    }
    return xhr;
  };
}
