import { Component, OnDestroy, Input, SimpleChanges } from '@angular/core';
import { NbThemeService, NbColorHelper } from '@nebular/theme';

@Component({
  selector: 'ngx-analysis-line',
  template: `
    <chart type="line" [data]="data" [options]="options"></chart>
  `,
})
export class AnalysisLineComponent implements OnDestroy {
  data: any;
  options: any;
  themeSubscription: any;

  @Input() analysis: any;

  colors: any;

  constructor(private theme: NbThemeService) {
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {

      const colors: any = config.variables;
      const chartjs: any = config.variables.chartjs;

      this.colors = colors;

      this.data = {
        labels: ['Demo', 'Demo', 'Demo', 'Demo', 'Demo', 'Demo', 'Demo'],
        datasets: [{
          data: [65, 59, 80, 81, 56, 55, 40],
          label: 'Top Trends',
          backgroundColor: NbColorHelper.hexToRgbA(colors.primary, 0.3),
          borderColor: colors.primary,
        }/*, {
          data: [28, 48, 40, 19, 86, 27, 90],
          label: 'Series B',
          backgroundColor: NbColorHelper.hexToRgbA(colors.danger, 0.3),
          borderColor: colors.danger,
        }, {
          data: [18, 48, 77, 9, 100, 27, 40],
          label: 'Series C',
          backgroundColor: NbColorHelper.hexToRgbA(colors.info, 0.3),
          borderColor: colors.info,
        },*/
        ],
      };

      this.options = {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          xAxes: [
            {
              gridLines: {
                display: true,
                color: chartjs.axisLineColor,
              },
              ticks: {
                fontColor: chartjs.textColor,
              },
            },
          ],
          yAxes: [
            {
              gridLines: {
                display: true,
                color: chartjs.axisLineColor,
              },
              ticks: {
                fontColor: chartjs.textColor,
              },
            },
          ],
        },
        legend: {
          labels: {
            fontColor: chartjs.textColor,
          },
        },
      };
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log(this.analysis);

    if (this.analysis.length > 0) {
      var label = [];
      var occur = [];
      
      for (let element of this.analysis) {
        label.push(element.name);
      }
  
      this.data = {
        labels: label,
        datasets: [{
          data: occur,
          label: 'Top Trends',
          backgroundColor: NbColorHelper.hexToRgbA(this.colors.primary, 0.3),
          borderColor: this.colors.primary,
        }],
      };
    }
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }
}
