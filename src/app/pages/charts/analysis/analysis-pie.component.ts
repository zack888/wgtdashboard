import { Component, OnDestroy, Input, SimpleChanges } from '@angular/core';
import { NbThemeService } from '@nebular/theme';

@Component({
  selector: 'ngx-analysis-pie',
  template: `
    <chart type="pie" [data]="data" [options]="options"></chart>
  `,
})
export class AnalysisPieComponent implements OnDestroy {
  data: any;
  options: any;
  themeSubscription: any;

  @Input() analysis: any;

  colors: any;

  constructor(private theme: NbThemeService) {
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {

      const colors: any = config.variables;
      const chartjs: any = config.variables.chartjs;

      this.colors = colors;

      this.data = {
        labels: ['Demo', 'Demo', 'Demo'],
        datasets: [{
          data: [300, 500, 100],
          backgroundColor: [colors.primaryLight, colors.infoLight, colors.successLight],
        }],
      };

      this.options = {
        maintainAspectRatio: false,
        responsive: true,
        scale: {
          pointLabels: {
            fontSize: 14,
            fontColor: chartjs.textColor,
          },
        },
        scales: {
          xAxes: [
            {
              display: false,
            },
          ],
          yAxes: [
            {
              display: false,
            },
          ],
        },
        legend: {
          labels: {
            fontColor: chartjs.textColor,
          },
        },
      };
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log(this.analysis);

    if (this.analysis.length > 0) {
      var label = [];
      var occur = [];
      
      for (let element of this.analysis) {
        label.push(element.name);
      }
  
      this.data = {
        labels: label,
        datasets: [{
          data: occur,
          backgroundColor: [this.colors.primaryLight, this.colors.infoLight, this.colors.successLight],
        }],
      };
    }
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }
}
