import { Component, ViewChild, Input, SimpleChanges, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'ngx-rating',
  styleUrls: ['./rating.component.scss'],
  template: `
  <div [class]="'br br-' + theme" [class.br-readonly]="readOnly" [class.br-disabled]="disabled">

  <div class="br-units" (mouseleave)="reset()">

    <div class="br-unit" *ngFor="let unit of contexts" [class.br-fraction]="unit.fraction"
        [class.br-selected]="unit.selected" [class.br-active]="unit.active"
        (click)="unit.click($event)" (mouseenter)="unit.enter()"></div>

  </div>

  <div *ngIf="showText" class="br-text">{{ nextRate }}</div>
</div>
  `,
})

export class RatingComponent {
  @Input() value;
  @Input() rowData: any;

  contexts = [];
  nextRate: number;
  disabled: boolean;

  /** Maximal rating that can be given using this widget. */
  max = 6;

  /** A flag indicating if rating can be updated. */
  readOnly = true;

  /** Set the theme */
  theme = 'horizontal';

  /** Show rating title */
  showText = false;

  /** Replace rate value with a title */
  titles = [];

  /** A flag indicating if rating is required for form validation. */
  required = false;

  /** An event fired when a user is hovering over a given rating.
   * Event's payload equals to the rating being hovered over. */
  @Output() hover = new EventEmitter<number>();

  /** An event fired when a user stops hovering over a given rating.
   * Event's payload equals to the rating of the last item being hovered over. */
  @Output() leave = new EventEmitter<number>();

  /** An event fired when a user selects a new rating.
   * Event's payload equals to the newly selected rating. */
  @Output() rateChange = new EventEmitter<number>(true);

  constructor(private changeDetectorRef: ChangeDetectorRef) {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['rate']) {
      this.update(this.value);
    }
  }

  ngOnInit() {
    this.contexts = Array.from({ length: this.max }, (context, i) => ({
      selected: false,
      fraction: false,
      click: (e) => this.handleClick(e, i + 1),
      enter: () => this.handleEnter(i + 1)
    }));

    this.updateState(this.value);
  }

  update(newRate: number, internalChange = true): void {
    if (!this.readOnly && !this.disabled && this.value !== newRate) {
      this.value = newRate;
      this.rateChange.emit(this.value);
    }
    if (internalChange) {
      this.onChange(this.value);
      this.onTouched();
    }
    this.updateState(this.value);
  }

  /** Reset rate value */
  reset() {
    this.leave.emit(this.nextRate);
    this.updateState(this.value);
  }

  private updateState(nextValue) {
    /** Set rate value as text */
    this.nextRate = nextValue - 1;
    /** Set the rating */
    this.contexts = Array.from({ length: this.max }, (context, index) => ({
      selected: index + 1 <= nextValue,
      fraction: (index + 1 === Math.round(nextValue) && nextValue % 1) >= 0.5,
      click: (e) => this.handleClick(e, index),
      enter: () => this.handleEnter(index)
    }));
  }

  private handleClick(e, value) {
    /** (NOT TESTED) Remove 300ms click delay on touch devices */
    e.preventDefault();
    e.stopPropagation();
    this.update(value + 1);
  }

  private handleEnter(index) {
    if (!this.disabled && !this.readOnly) {
      /** Add selected class for rating hover */
      this.contexts.map((context, i) => {
        context.active = i <= index;
        context.fraction = false;
        context.selected = false;
      });
      this.nextRate = index;
      this.hover.emit(index);
    }
  }

  /** This is the initial value set to the component */
  writeValue(value: number) {
    this.update(value, false);
    this.changeDetectorRef.markForCheck();
  }

  onChange = (_: any) => { };
  onTouched = () => { };

  registerOnChange(fn: (value: any) => any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
  }
}
