import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LocalDataSource } from 'ng2-smart-table';
import * as AWS from 'aws-sdk';
import * as Cognito from "amazon-cognito-identity-js";
import { LoadingBarService } from '@ngx-loading-bar/core';
import { UserService } from '../../../../@core/data/users.service';
import { setTimeout } from 'timers';
import { ChartUrlButtonComponent } from '../chart-button.component';
import { RatingComponent } from '../rating.component';
import { ChartEditButtonComponent } from '../edit-button.component';

@Component({
  selector: 'ngx-mon-list',
  styleUrls: ['./monitor-list.component.scss'],
  templateUrl: './monitor-list.component.html',
})

export class MonitorListComponent {
  settings = {
    actions: false,
    hideSubHeader: true,
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      date: {
        title: 'Posted',
        type: 'string',
        width: '15%',
      },
      title: {
        title: 'Title',
        type: 'string',
        width: '20%',
      },
      prog: {
        title: 'Progress',
        type: 'custom',
        renderComponent: RatingComponent,
        onComponentInitFunction(instance) {
          /*instance.click.subscribe(row => {
          });*/
        },
        width: '15%',
        filter: false
      },
      duration: {
        title: 'Duration',
        type: 'string',
        width: '20%',
      },
      status: {
        title: 'Status',
        type: 'string',
        width: '10%',
      },
      url: {
        title: 'URL(s)',
        type: 'custom',
        renderComponent: ChartUrlButtonComponent,
        onComponentInitFunction(instance) {
          instance.click.subscribe(row => {
          });
        },
        width: '10%',
        filter: false
      },
      edit: {
        title: 'Edit',
        type: 'custom',
        renderComponent: ChartEditButtonComponent,
        onComponentInitFunction(instance) {
          instance.click.subscribe(row => {
          });
        },
        width: '10%',
        filter: false
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  data = [];

  constructor(private userService: UserService, protected router: Router, private loadingBar: LoadingBarService) {
    if (this.userService.isLoggedOut()) {
      setTimeout(() => {
        return this.router.navigateByUrl('/auth/login');
      }, 0);
    } else {
      this.userService.session();
    }
  }

  ngOnInit(): void {
    this.loadingBar.start();

    var userPool = new Cognito.CognitoUserPool({
      UserPoolId: 'ap-southeast-1_7PvKAeRNz',
      ClientId: 'sa6cluje5ml19da5j49hfr2ms',
    });

    AWS.config.update({
      region: 'ap-southeast-1'
    });

    var docClient = new AWS.DynamoDB.DocumentClient();

    var params = {
      TableName: "WGTMonitorTask",
      KeyConditionExpression: "#id = :id",
      ExpressionAttributeNames: {
          "#id": "username",
      },
      ExpressionAttributeValues: {
           ":id": userPool.getCurrentUser().getUsername(),
      },
      ScanIndexForward: false
    };

    docClient.query(params, (err, data) => {
      if (err) {
        //TODO
        console.error(err);
      } else {

        for (let item of data.Items) {
          var status = "";
          if (item.status == 100) {
            status = "Completed";
          } else if (item.status == 0) {
            status = "In queue";
          } else if (item.status < 0) {
            status = "Error";
          } else {
            status = "In progress";
          }

          var start = new Date(item.startDate.year, item.startDate.month - 1, item.startDate.day);
          var end = new Date(item.endDate.year, item.endDate.month - 1, item.endDate.day);
          var prog = this.dateDiff(start, new Date(), 'days') / this.dateDiff(start, end, 'days') * 6;

          if (prog > 0 && prog < 6) status = "In progress";
          else if (prog == 6) status = "Completed";

          this.data.push({
            id: item.timestamp,
            name: item.name,
            date: this.getDateString(item.timestamp),
            title: item.title,
            status: status,
            url: item.url,
            prog: prog,
            duration: `${this.getSubDateString(start.getTime())} to ${this.getSubDateString(end.getTime())}`,
            edit: {
              usr: userPool.getCurrentUser().getUsername(),
              tid: item.timestamp
            }
          });
        };

        this.source.load(this.data);
      }
      this.loadingBar.complete();
    });
  }

  getDateString(timestamp: number): string {
    const date = new Date(timestamp);
    var minutes = "0" + date.getMinutes();
    var seconds = "0" + date.getSeconds();
    var month = (date.getMonth() + 1).toString();

    return date.getDate() + '/' + month + '/' + date.getFullYear() + ' - ' +
      date.getHours() + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
  }

  getSubDateString(timestamp: number): string {
    const date = new Date(timestamp);
    var minutes = "0" + date.getMinutes();
    var seconds = "0" + date.getSeconds();
    var month = (date.getMonth() + 1).toString();

    return date.getDate() + '/' + month + '/' + date.getFullYear();
  }

  dateDiff(date1, date2, unit) {
    if (!(date1 instanceof Date) || !(date2 instanceof Date)) {
      return(undefined);
    }

    // Convert to UTC to account for DST
    var UTCdate1 = new Date(date1);
    var UTCdate2 = new Date(date2);
    UTCdate1.setMinutes(UTCdate1.getMinutes() - UTCdate1.getTimezoneOffset());
    UTCdate2.setMinutes(UTCdate2.getMinutes() - UTCdate2.getTimezoneOffset());

    // Calculate difference in milliseconds (always positive)
    var diffMilliseconds = UTCdate2.getTime() - UTCdate1.getTime();

    // Calculate differences for each time unit
    switch (unit) {
      case 'milliseconds':
        return diffMilliseconds;
      case 'seconds':
        var diffSeconds = Math.round(diffMilliseconds / (1000));
        return diffSeconds;
      case 'minutes':
        var diffMinutes = Math.round(diffMilliseconds / (1000*60));
        return diffMinutes;
      case 'hours':
        var diffHours = Math.round(diffMilliseconds / (1000*60*60));
        return diffHours;
      case 'days':
        var diffDays = Math.round(diffMilliseconds / (1000*60*60*24));
        return diffDays;
      case 'weeks':
        var diffWeeks = Math.round(diffMilliseconds / (1000*60*60*24*7));
        return diffWeeks;
      case 'months':
        var month1 = UTCdate1.getMonth();
        var month2 = UTCdate2.getMonth();

        var year1 = UTCdate1.getFullYear();
        var year2 = UTCdate2.getFullYear();

        var totalMonth1 = month1 + (year1*12);
        var totalMonth2 = month2 + (year2*12);

        var diffMonths = Math.round(totalMonth2 - totalMonth1);
        return diffMonths;
      case 'years':
        var year1 = UTCdate1.getFullYear();
        var year2 = UTCdate2.getFullYear();

        var diffYears = Math.round(year2-year1);
        return diffYears;

      // If no unit is passed, default to return milliseconds
      default:
        return diffMilliseconds;
    }
  };
}
