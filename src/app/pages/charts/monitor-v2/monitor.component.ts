import { Component } from '@angular/core';

@Component({
  selector: 'ngx-monitorv2',
  styleUrls: ['./monitor.component.scss'],
  templateUrl: './monitor.component.html',
})

export class MonitorV2Component {
  tabs: any[] = [
    {
      title: 'Analysis',
      route: '/pages/analysis/monitor/view',
    },
    {
      title: 'Task List',
      route: '/pages/analysis/monitor/status',
    },
    {
      title: 'New Task',
      route: '/pages/analysis/monitor/submit',
    },
  ];
}
