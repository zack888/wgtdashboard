import { Component, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { LocalDataSource } from 'ng2-smart-table';

import * as AWS from 'aws-sdk';
import * as Cognito from "amazon-cognito-identity-js";
import { CognitoIdentityServiceProvider } from 'amazon-cognito-identity-js';

import { LoadingBarService } from '@ngx-loading-bar/core';
import { User, UserService } from '../../../../@core/data/users.service';
import { setTimeout } from 'timers';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from '../../monitor/modal.component';

import { IMyDrpOptions, IMyDateRangeModel, MyDateRangePicker } from 'mydaterangepicker';

import * as validator from 'valid-url';

import { ToasterService, ToasterConfig, Toast, BodyOutputType } from 'angular2-toaster';

@Component({
  selector: 'ngx-mon-new',
  styleUrls: ['./monitor-new.component.scss'],
  templateUrl: './monitor-new.component.html',
})

export class MonitorNewComponent {
  @ViewChild('dateranger') ranger: MyDateRangePicker;

  isEdit = false;

  myDateRangePickerOptions: IMyDrpOptions = {
    dateFormat: 'dd.mm.yyyy',
  };

  today = new Date();
  originDate: any;

  private model: any = {
    beginDate: {year: this.today.getFullYear(), month: this.today.getMonth() + 1, day: this.today.getUTCDate()},
    endDate: {year: this.today.getFullYear(), month: this.today.getMonth() + 1, day: this.today.getUTCDate()}
  };

  showMessages: any = {};
  errors: string[] = [];
  urlerr: boolean = false;
  messages: string[] = [];
  submitted: boolean = false;
  user: User;

  submission: any = {};

  urls: LocalDataSource = new LocalDataSource();

  urlList: any[] = [
    "", "", "", "", "",
  ];

  timeSelect: boolean = false;
  picSelect: boolean = false;

  picture: boolean = false;

  config: ToasterConfig;

  settings = {
    actions: false,
    hideSubHeader: true,
    columns: {
      url: {
        title: 'URL',
        type: 'string',
        width: '100%',
        filter: false,
      },
    },
  };

  constructor(protected userService: UserService, private loadingBar: LoadingBarService, protected router: Router,
    private toasterService: ToasterService, private activatedRoute: ActivatedRoute) {
    if (this.userService.isLoggedOut()) {
      setTimeout(() => {
        return this.router.navigateByUrl('/auth/login');
      }, 0);
    } else {
      this.userService.session();

      this.activatedRoute.queryParams.subscribe((params: Params) => {
        let taskId = params['tid'];
        let userId = params['usr'];
        if (taskId && userId) this.load(userId, taskId);
      });
    }
    this.user = userService.userInterface;
    this.urls.load([]);
  }

  load(userId, taskId) {
    this.loadingBar.start();

    var userPool = new Cognito.CognitoUserPool({
      UserPoolId: 'ap-southeast-1_7PvKAeRNz',
      ClientId: 'sa6cluje5ml19da5j49hfr2ms',
    });

    AWS.config.update({
      region: 'ap-southeast-1'
    });

    var docClient = new AWS.DynamoDB.DocumentClient();

    var params = {
      TableName: "WGTMonitorTask",
      KeyConditionExpression: "#id = :u AND #tid = :d",
      ExpressionAttributeNames: {
          "#id": "username",
          "#tid": "timestamp"
      },
      ExpressionAttributeValues: {
            ":u": userId,
            ":d": Number(taskId)
      }
    };

    docClient.query(params, (err, data) => {
      if (err) {
        //TODO
        console.error(err);
      } else {
        if (data.Items.length > 0) {
          var item = data.Items[0];
          var start = item.startDate;
          var end = item.endDate;
          var urls = item.url.split(",");
          var title = item.title;

          this.startDate = start;
          this.endDate = end;

          this.ranger.selectBeginDate(start);
          this.ranger.selectEndDate(end);
          this.ranger.rangeSelected();

          this.originDate = new Date(start.year, start.month - 1, start.day);
          console.log(this.originDate);
          this.submission.url = title;

          for (var i = 0; i < urls.length; i++) {
            this.urlSubmission.push(urls[i]);
            this.urls.add({
              url: urls[i]
            });
          }

          this.urls.refresh();
          this.isEdit = true;

          this.showToast('success', 'Task Loaded', 'Start editing your task');
        } else {
          this.showToast('error', 'Invalid Task', 'Task not found')
        }
      }
      this.loadingBar.complete();
    });
  }

  validator = [
    'item',
    'detail',
    'spm',
    'sku',
    'itm',
    'prod'
  ];

  validurl: boolean = true;

  valid(url): boolean {
    var okay = false;
    for (const valid of this.validator) {
      if (url.indexOf(valid) > -1) {
        okay = true;
        break;
      }
    }
    return okay;
  }

  validURL(url) {
    return !validator.isUri(url);
  }

  validEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email.toLowerCase());
  }

  onURLAdd(event): void {
    if (!event.newData.url) {
      event.confirm.reject();
    } else if (this.validURL(event.newData.url)) {
      window.confirm('Your input is not an URL')
      event.confirm.reject();
    } else {
      event.confirm.resolve(event.newData);
      if (!this.valid(event.newData.url)) {
        this.validurl = false;
      }

      var urls = "";
      this.urls.getElements().then(e => {
        e.forEach(element => {
          urls = urls + element.url + ",";
        });
      });
    }
  }

  onDeleteURL(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  submit(form) {
    this.messages = [];
    this.errors = [];
    this.send(form, this.user.email);
  }

  isMobile(url) {
    return (url.indexOf('://m.') > -1 || url.indexOf('.m.') > -1);
  }

  send(form, emails) {
    var urls = this.serialURL();
    for (var i = 0; i < this.urlSubmission.length; i++) {
      if (this.isMobile(this.urlSubmission[i])) {
        this.errors.push('Please do not submit any mobile URL(s)');
        return;
      }
    }

    if (!this.validDate()) return;

    if (urls.length > 0) {
      this.loadingBar.start();
      this.submitted = true;

      var userPool = new Cognito.CognitoUserPool({
        UserPoolId: 'ap-southeast-1_7PvKAeRNz',
        ClientId: 'sa6cluje5ml19da5j49hfr2ms',
      });

      var params = {
        "id" : userPool.getCurrentUser().getUsername(),
        "title": this.submission.url,
        "type": 0,
        "email": emails,
        "url": urls,
        "time": this.timeSelect,
        "picture": this.picSelect,
        "startDate": {
          "year": this.startDate.year,
          "month": this.startDate.month,
          "day": this.startDate.day,
        },
        "endDate": {
          "year": this.endDate.year,
          "month": this.endDate.month,
          "day": this.endDate.day,
        }
      }

      var url = 'https://jwzu8znvz1.execute-api.ap-southeast-1.amazonaws.com/prod/WGT_NewMonitorTask';
      var method = 'POST';
      var xhr = this.createCORSRequest(method, url);

      xhr.onload = () => {
        if (JSON.parse(xhr.responseText).message == "Success") {
          form.reset();
          this.urls.empty();
          this.urls.refresh();
          this.urlSubmission = [];

          this.submitted = false;
          this.showToast('success', 'Submitted successfully!', 'You may check back later and view the analysis once it\'s completed');
          this.messages.push('Please wait while we analyze the URL. You may check your tasks for the progress.');
        } else {
          this.submitted = false;
          this.showToast('error', 'Failed to submit', 'Please try again later');
          this.errors.push('Unable to process your task. Please try again later');
        }

        this.loadingBar.complete();
      };

      xhr.onerror = () => {
        console.log(xhr.responseText);
        this.submitted = false;
        this.showToast('error', 'Failed to submit', 'Please try again later');
        this.errors.push('Unable to process your task. Please try again later');

        this.loadingBar.complete();
      };

      xhr.send(JSON.stringify(params));
    } else {
      this.errors.push('Please enter at least 1 URL for analysis')
      this.showToast('error', 'Failed to submit', 'You need to enter at least 1 URL');
    }
  }

  tasks: any[] = [
    { id: false, description: 'Sort by popularity' },
    { id: true, description: 'Sort by time' },
  ];

  set(type) {
    this.timeSelect = type;
  }

  /*XDomainRequest: {
    new (): XDomainRequest;
    prototype: XDomainRequest;
    create(): XDomainRequest;
  };*/

  dateDiff(date1, date2, unit) {
    if (!(date1 instanceof Date) || !(date2 instanceof Date)) {
      return(undefined);
    }

    // Convert to UTC to account for DST
    var UTCdate1 = new Date(date1);
    var UTCdate2 = new Date(date2);
    UTCdate1.setMinutes(UTCdate1.getMinutes() - UTCdate1.getTimezoneOffset());
    UTCdate2.setMinutes(UTCdate2.getMinutes() - UTCdate2.getTimezoneOffset());

    // Calculate difference in milliseconds (always positive)
    var diffMilliseconds = UTCdate2.getTime() - UTCdate1.getTime();

    // Calculate differences for each time unit
    switch (unit) {
      case 'milliseconds':
        return diffMilliseconds;
      case 'seconds':
        var diffSeconds = Math.round(diffMilliseconds / (1000));
        return diffSeconds;
      case 'minutes':
        var diffMinutes = Math.round(diffMilliseconds / (1000*60));
        return diffMinutes;
      case 'hours':
        var diffHours = Math.round(diffMilliseconds / (1000*60*60));
        return diffHours;
      case 'days':
        var diffDays = Math.round(diffMilliseconds / (1000*60*60*24));
        return diffDays;
      case 'weeks':
        var diffWeeks = Math.round(diffMilliseconds / (1000*60*60*24*7));
        return diffWeeks;
      case 'months':
        var month1 = UTCdate1.getMonth();
        var month2 = UTCdate2.getMonth();

        var year1 = UTCdate1.getFullYear();
        var year2 = UTCdate2.getFullYear();

        var totalMonth1 = month1 + (year1*12);
        var totalMonth2 = month2 + (year2*12);

        var diffMonths = Math.round(totalMonth2 - totalMonth1);
        return diffMonths;
      case 'years':
        var year1 = UTCdate1.getFullYear();
        var year2 = UTCdate2.getFullYear();

        var diffYears = Math.round(year2-year1);
        return diffYears;

      // If no unit is passed, default to return milliseconds
      default:
        return diffMilliseconds;
    }
  };

  startDate: any = {};
  endDate: any = {};

  onDateRangeChanged(event: IMyDateRangeModel) {
    this.startDate = event.beginDate;
    this.endDate = event.endDate;
  }

  private validDate() {
    var start = new Date(this.startDate.year, this.startDate.month - 1, this.startDate.day);
    var end = new Date(this.endDate.year, this.endDate.month - 1, this.endDate.day);
    if (!this.isEdit) {
      if (start <= new Date()) {
        this.showToast('error', 'Invalid Date', 'Please choose a starting date that is after today');
        return false;
      }
    } else {
      if (this.originDate > start) {
        console.log(start, end);
        this.showToast('error', 'Invalid Date', 'Starting date cannot be before the initial starting date');
        return false;
      }
      if (new Date() > end) {
        console.log(start, end);
        this.showToast('error', 'Invalid Date', 'Please choose an end date that is after today');
        return false;
      }
    }
    var span = this.dateDiff(start, end, 'days');
    if (span > 0 && span < 180) return true;
    this.showToast('error', 'Invalid Date', 'Please set a valid range that is less than 6 months');
    return false;
  }

  private showToast(type: string, title: string, body: string) {
    this.config = new ToasterConfig({
      positionClass: 'toast-top-right',
      timeout: 10000,
      newestOnTop: true,
      tapToDismiss: true,
      preventDuplicates: true,
      animation: 'slideDown',
      limit: 5,
    });
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 10000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this.toasterService.popAsync(toast);
  }

  createCORSRequest = function(method, url) {
    var xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {
      xhr.open(method, url, true);
    } /*else if (typeof XDomainRequest != "undefined") {
      // IE8 & IE9
      xhr = new XDomainRequest();
      xhr.open(method, url);
    }*/ else {
      xhr = null;
    }
    return xhr;
  };

  urlSubmission: any[] = [];

  focus(row) {
    console.log('focus');
  }

  blur(row) {
    console.log('blur');
  }

  check(): any[] {
    var list = [];
    for (var i = 0; i < this.urlList.length; i++) {
      if (this.isUrlValid(this.urlList[i])) {
        list.push(this.urlList[i]);
      } /*else {
        this.showToast('error', 'Invalid URL', 'Please make sure URL is correct');
      }*/
    }
    return list.filter(function(item, pos, self) {
      return self.indexOf(item) == pos;
    });
  }

  isUrlValid(userInput) {
    var res = userInput.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
    if(res == null)
        return false;
    else
        return true;
  }

  clean() {
    var list = this.check();
    if (this.urlSubmission.length > 0) {
      for (var i = 0; i < list.length; i++) {
        var found = false;
        for (var j = 0; j < this.urlSubmission.length; j++) {
          if (this.urlSubmission[j].url == list[i]) {
            found = true;
          }
        }
        if (!found) {
          if (!this.isMobile(list[i])) {
            this.urlSubmission.push(list[i]);
            this.urls.add({
              url: list[i]
            });
          } else {
            this.showToast('error', 'Invalid URL', 'Please do not input mobile URL');
          }
        }
      }
    } else {
      for (var i = 0; i < list.length; i++) {
        if (!this.isMobile(list[i])) {
          this.urlSubmission.push(list[i]);
          this.urls.add({
            url: list[i]
          });
        } else {
          this.showToast('error', 'Invalid URL', 'Please do not input mobile URL');
        }
      }
    }
    this.urls.refresh();

    this.urlList = ["","","","","",];
  }

  serialURL() : string {
    this.clean();
    var url = "";
    for (var i = 0; i < this.urlSubmission.length; i++) {
      url = url + this.urlSubmission[i] + ",";
    }
    return url.slice(0, -1);
  }

  addToList() {
    this.urls.getElements().then((data) => {
      var list = this.check();
      if (data.length > 0) {
        for (var i = 0; i < list.length; i++) {
          var found = false;
          for (var j = 0; j < data.length; j++) {
            if (data[j].url == list[i]) {
              found = true;
            }
          }
          if (!found) {
            if (!this.isMobile(list[i])) {
              this.urlSubmission.push(list[i]);
              this.urls.add({
                url: list[i]
              });
            } else {
              this.showToast('error', 'Invalid URL', 'Please do not input mobile URL');
            }
          }
        }
      } else {
        for (var i = 0; i < list.length; i++) {
          if (!this.isMobile(list[i])) {
            this.urlSubmission.push(list[i]);
            this.urls.add({
              url: list[i]
            });
          } else {
            this.showToast('error', 'Invalid URL', 'Please do not input mobile URL');
          }
        }
      }
      this.urls.refresh();

      this.urlList = ["","","","","",];
    });
  }
}
