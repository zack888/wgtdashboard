import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'ngx-chart-edit',
  template: `
    <button *ngIf="visible" class="btn btn-success btn-tn" style="height: 30px;" (click)="onClick()"><i class="ion-edit" ></i></button>
  `,
})

export class ChartEditButtonComponent {
  @Input() value;
  @Output() click: EventEmitter<any> = new EventEmitter();

  visible = true;

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {}

  onClick() {
    setTimeout(() => {
      return this.router.navigate(['../submit' ], {
        relativeTo: this.activatedRoute,
        queryParams: {
          usr: this.value.usr,
          tid: this.value.tid
        }
      });
    }, 0);
  }
}
