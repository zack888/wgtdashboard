import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import * as AWS from 'aws-sdk';
import * as Cognito from "amazon-cognito-identity-js";
import { LoadingBarService } from '@ngx-loading-bar/core';
import { UserService } from '../../../../@core/data/users.service';
import { setTimeout } from 'timers';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToasterService } from 'angular2-toaster';
import { LocalDataSource } from 'ng2-smart-table';
import { VerbamodalComponent } from '../../analysis/verbamodal/verbamodal.component';
import { VisibleButtonComponent } from '../../analysis/mentions/visible-button.component';
import { VerbatimButtonComponent } from '../../analysis/mentions/verbatim-button.component';
import { UndoButtonComponent } from '../../analysis/mentions/undo-button.component';
import { SaverComponent } from '../../analysis/mentions/saver/saver.component';
import { OpenerComponent } from '../../analysis/mentions/saver/opener.component';

@Component({
  selector: 'ngx-mon-analysis',
  styleUrls: ['./monitor-analysis.component.scss'],
  templateUrl: './monitor-analysis.component.html',
})

export class MonitorAnalysisComponent {
  data = [];
  lcs = [];

  tasks: any[] = [
    { id: 0, url: 'Select a completed task to view analysis', date: '' },
  ];
  curTask: any = this.tasks[0];
  empty = false;

  ngOnInit() {
    var hasTask = false;

    var userPool = new Cognito.CognitoUserPool({
      UserPoolId: 'ap-southeast-1_7PvKAeRNz',
      ClientId: 'sa6cluje5ml19da5j49hfr2ms',
    });

    AWS.config.update({
      region: 'ap-southeast-1'
    });

    var docClient = new AWS.DynamoDB.DocumentClient();

    var params = {
      TableName: "WGTMonitorTask",
      KeyConditionExpression: "#id = :id",
      ExpressionAttributeNames: {
          "#id": "username",
      },
      ExpressionAttributeValues: {
           ":id": userPool.getCurrentUser().getUsername(),
      },
      ScanIndexForward: false
    };

    this.loadingBar.start();

    docClient.query(params, (err, data) => {
      if (err) {
        //TODO
      } else {
        for (let item of data.Items) {
          hasTask = true;
          if (item.status != 0) {
            this.tasks.push({
              id: this.tasks.length,
              url: item.title,
              date: this.getDateString(item.timestamp),
              timestamp: item.timestamp
            });
          }
        };

        if (!hasTask) {
          this.empty = true;
        }
      }

      this.loadingBar.complete();
    });
  }

  loadAnalysis(id) {
    var self = this;
    var xhr = this.createCORSRequest('GET', 'https://s3-ap-southeast-1.amazonaws.com/wgtmonitor/' + this.tasks[id].timestamp + '.json');
    xhr.onload = () => {
      var file = JSON.parse(xhr.responseText);
      this.data = file.data;
      this.lcs = file.lcs;
      this.loadVerbfile(id);
    }

    xhr.onerror = () => {
      console.log(xhr.responseText);
      this.loadingBar.complete();
    };

    xhr.send();
  }

  loadVerbfile(id) {
    var self = this;
    var xhr = this.createCORSRequest('GET', 'https://s3-ap-southeast-1.amazonaws.com/wgtmonitor/v' + this.tasks[id].timestamp + '.json');
    xhr.onload = () => {
      self.loadingBar.complete();
      this.verbatim = JSON.parse(xhr.responseText);
      this.start(this.data, this.lcs);
    }

    xhr.onerror = () => {
      this.loadingBar.complete();
    };

    xhr.send();
  }

  setNewUser(id: any): void {
    if (id > 0) {
      this.curr = id;
      this.loadingBar.start();
      this.loadAnalysis(id);
    }
  }

  firstOriginList: any[] = [];

  mainSettings = {
    actions: false,
    selectMode: 'multi',
    hideSubHeader: true,
    columns: {
      name: {
        title: 'Term',
        type: 'string',
        width: '30%',
        filter: {
          type: 'completer',
          config: {
            selectText: 'Search for term',
            completer: {
              data: this.firstOriginList,
              searchFields: 'name',
              titleField: 'name'
            }
          }
        }
      },
      mention: {
        title: '% of Mention',
        type: 'string',
      },
      negative: {
        title: '% of Negative',
        type: 'string',
      },
      visible: {
        title: 'Visible',
        type: 'custom',
        filter: false,
        sort: false,
        renderComponent: VisibleButtonComponent,
        onComponentInitFunction: (instance: any) => {
          instance.click.subscribe(row => {
            this.refresh();
          });
        },
      },
      verbatim: {
        title: 'Verbatim',
        type: 'custom',
        filter: false,
        sort: false,
        renderComponent: VerbatimButtonComponent,
        onComponentInitFunction: (instance: any) => {
          instance.click.subscribe(row => {
            this.showVerbatim(row);
          });
        },
      },
    },
  };

  chartSettings = {
    actions: false,
    hideSubHeader: true,
    columns: {
      name: {
        title: 'Term',
        type: 'string',
        width: '70%',
        filter: false,
        sort: false,
      },
      mention: {
        title: '% Mention',
        type: 'string',
        width: '30%',
        filter: false,
        sort: false,
      },
    },
  };

  lcsSettings = {
    actions: false,
    hideSubHeader: true,
    columns: {
      phrase: {
        title: 'Common Phrases',
        type: 'string',
        width: '70%',
        filter: false,
        sort: false,
      },
      count: {
        title: 'Count',
        type: 'string',
        width: '30%',
        filter: false,
        sort: false,
      },
    },
  };

  actionSettings = {
    actions: false,
    hideSubHeader: true,
    columns: {
      label: {
        title: 'Action',
        type: 'string',
        width: '70%',
        filter: false,
        sort: false,
      },
      undo: {
        title: 'Undo',
        type: 'custom',
        width: '30%',
        filter: false,
        sort: false,
        renderComponent: UndoButtonComponent,
        onComponentInitFunction: (instance: any) => {
          instance.click.subscribe(row => {
            this.undo(row);
          });
        },
      },
    },
  };

  mainSource: LocalDataSource = new LocalDataSource();
  chartSource: LocalDataSource = new LocalDataSource();
  actionSource: LocalDataSource = new LocalDataSource();
  lcsSource: LocalDataSource = new LocalDataSource();

  curr = 0;
  task = 0;
  verbatim: any[] = [];

  originHoldingList: any[] = [];
  originActionList: any[] = [];
  originSort: any[] = [{field: 'mention', direction: 'desc'}];
  originPage = 1;

  holdingList: any[] = [];
  displayList: any[] = [];
  actionList: any[] = [];
  holdingListSelected: any[] = [];

  availableId = 100000;

  maxDisplay = 10;
  maxEllipsize = 8;
  maxActEllipsize = 12;

  lastSort: any[] = [{field: 'mention', direction: 'desc'}];
  lastPage = 1;

  incoming = [];

  public reset() {
    this.displayList = [];
    this.holdingListSelected = [];
  }

  public startData(data) {
    this.incoming = data;

    this.originHoldingList = [...data];
    this.originActionList = [];

    this.originPage = 1;
    this.originSort = [{field: 'mention', direction: 'desc'}];

    for (var i = 0; i < this.originHoldingList.length; i++) {
      this.originHoldingList[i] = {
        name: this.originHoldingList[i].name,
        mention: this.originHoldingList[i].value,
        negative: this.originHoldingList[i].secondary,
        count: this.originHoldingList[i].count,
        visible: true,
        action: false,
        verbatim: true,
        id: i,
        order: i
      };
      this.firstOriginList.push(this.originHoldingList[i]);
    }

    this.mainSource.reset();
    this.actionSource.reset();

    this.restart();

    this.incoming = [...data];
  }

  public start(data, lcs) {
    this.lcs = [];
    this.lcs = lcs;

    this.startData(data);

    this.lcsSource.load(lcs);
  }

  removal(event) {
    this.hide(this.displayList[event.index].id);
  }

  /* Refresh display, use this after all ops */
  refresh() {
    this.displayList = [];

    this.mainSource.getFilteredAndSorted().then(data => {
      var i = 0;

      while (this.displayList.length < this.maxDisplay && i < data.length) {
        if (i < data.length && data[i].visible) {
          this.displayList.push(data[i]);
        }
        i++;
      }

      this.chartSource.load(this.displayList);
    });
  }

  deleteItem(row) {
    if (this.holdingListSelected.indexOf(row) >= 0) {
      this.holdingListSelected.splice(this.holdingListSelected.indexOf(row), 1);
    }
    //Delete item
  }

  delete() {
    if (this.holdingListSelected.length > 0) {
      var name = "";
      var label = "Deleted";
      var array = [];
      var mention = 0;
      var negative = 0;

      for (var i = 0; i < this.holdingListSelected.length; i++) {
        label = label + " " + this.holdingListSelected[i].name;
        name = name + this.holdingListSelected[i].name;

        if (this.holdingListSelected[i].action) {
          for (var j = 0; j < this.holdingListSelected[i].items.length; j++) {
            array.push(this.holdingListSelected[i].items[j]);
          }

          this.actionList.splice(this.actionList.indexOf(this.holdingListSelected[i].actItem), 1);
        } else {
          array.push(this.holdingListSelected[i]);
        }

        if (i != this.holdingListSelected.length-1) {
          label += ", ";
          name += ", ";
        }

        this.holdingList.splice(this.holdingList.indexOf(this.holdingListSelected[i]), 1);
      }

      var actItem = {
        label: label,
        undo: true,
        isDelete: true,
        id: this.availableId,
        items: array
      };

      this.actionList.push(actItem);

      this.availableId++;
      this.holdingListSelected = [];

      this.actionSource.load(this.actionList);
      this.mainSource.load(this.holdingList).then(() => {
        this.refresh();
      });
    }
  }

  /* Combine Terms */
  combine() {
    if (this.holdingListSelected.length > 1) {
      var name = "";
      var label = "Combined";
      var array = [];
      var mention = 0;
      var negative = 0;

      for (var i = 0; i < this.holdingListSelected.length; i++) {
        label = label + " " + this.holdingListSelected[i].name;
        name = name + this.holdingListSelected[i].name;
        mention += this.holdingListSelected[i].mention;
        negative += this.holdingListSelected[i].negative;

        if (this.holdingListSelected[i].action) {
          for (var j = 0; j < this.holdingListSelected[i].items.length; j++) {
            array.push(this.holdingListSelected[i].items[j]);
          }

          this.actionList.splice(this.actionList.indexOf(this.holdingListSelected[i].actItem), 1);
        } else {
          array.push(this.holdingListSelected[i]);
        }

        if (i != this.holdingListSelected.length-1) {
          label += ", ";
          name += ", ";
        }

        this.holdingList.splice(this.holdingList.indexOf(this.holdingListSelected[i]), 1);
      }

      var actItem = {
        label: label,
        undo: true,
        isDelete: false,
        id: this.availableId
      };

      var action = {
        name: name,
        mention: mention,
        negative: negative,
        //count: count,
        visible: true,
        action: true,
        actItem: actItem,
        id: this.availableId,
        order: -1,
        items: array
      };

      this.actionList.push(actItem);

      this.availableId++;
      this.holdingListSelected = [];

      var inserted = false;

      for (var i = 0; i < this.holdingList.length; i++) {
        if (this.lastSort[0].field == 'mention') {
          if (this.lastSort[0].direction == 'desc') {
            if (!inserted && action.mention >= this.holdingList[i].mention) {
              this.holdingList.splice(i, 0, action);
              inserted = true;
            }
          } else {
            if (!inserted && action.mention <= this.holdingList[i].mention) {
              this.holdingList.splice(i, 0, action);
              inserted = true;
            }
          }
        } else {
          if (this.lastSort[0].direction == 'desc') {
            if (!inserted && action.negative >= this.holdingList[i].negative) {
              this.holdingList.splice(i, 0, action);
              inserted = true;
            }
          } else {
            if (!inserted && action.negative <= this.holdingList[i].negative) {
              this.holdingList.splice(i, 0, action);
              inserted = true;
            }
          }
        }
      }
      if (!inserted) {
        this.mainSource.append(action);
      } else {
        this.mainSource.load(this.holdingList);
      }
      this.actionSource.load(this.actionList);
      this.refresh();
    }
  }

  /* Undo action */
  undo(action) {
    this.actionList.splice(this.actionList.indexOf(action), 1);

    var newData = [];
    var items = [];

    if (action.isDelete) {
      newData = this.holdingList;
      items = action.items;
    } else {
      var found = false;
      for (var i = 0; i < this.holdingList.length; i++) {
        if (this.holdingList[i].id == action.id) {
          items = this.holdingList[i].items;
        } else {
          newData.push(this.holdingList[i]);
        }
      }
    }

    items = this.sort(items);
    var lead = 0;
    var count = 0;

    while (count < newData.length) {
      var inserted = false;
      if (this.lastSort[0].field == 'mention') {
        if (this.lastSort[0].direction == 'desc') {
          if (!inserted && items[lead].mention >= newData[count].mention) {
            newData.splice(count, 0, items[lead]);
            inserted = true;
          }
        } else {
          if (!inserted && items[lead].mention <= newData[count].mention) {
            if (items[lead].id > newData[count].id) {
              newData.splice(i, 0, items[lead]);
              inserted = true;
            }
          }
        }
      } else {
        if (this.lastSort[0].direction == 'desc') {
          if (!inserted && items[lead].negative >= newData[count].negative) {
            if (items[lead].id < newData[count].id) {
              newData.splice(i, 0, items[lead]);
              inserted = true;
            }
          }
        } else {
          if (!inserted && items[lead].negative <= newData[count].negative) {
            if (items[lead].id > newData[count].id) {
              newData.splice(i, 0, items[lead]);
              inserted = true;
            }
          }
        }
      }

      if (inserted) {
        lead++;
      } else {
        count++;
      }

      if (lead == items.length) {
        break;
      }
    }

    if (lead < items.length) {
      for (var i = lead; i < items.length; i++) {
        newData.push(items[i]);
      }
    }

    this.holdingList = newData;

    this.actionSource.load(this.actionList);
    this.mainSource.load(newData).then(() => {
      this.refresh();
    });
  }

  sort(items) : any[] {
    var self = this;
    var ret = items.sort( function(a,b) {
      if (self.lastSort[0].field == 'mention') {
        if (self.lastSort[0].direction == 'desc') {
          return a.id - b.id;
        } else {
          return a.mention - b.mention;
        }
      } else {
        if (self.lastSort[0].direction == 'desc') {
          return b.negative - a.negative;
        } else {
          return a.negative - b.negative;
        }
      }
    });
    return ret;
  }

  /* Toggle term visible */
  hide(id) {
    this.mainSource.getFilteredAndSorted().then(data => {
      for (var i = 0; i < data.length; i++) {
        if (data[i].id == id) {
          data[i].visible = false;
          this.unshow(id);
          this.refresh();
          this.mainSource.refresh();
          return;
        }
      }
    });
  }

  /* Remove the term from chart */
  unshow(id) {
    for (var i = 0; i < this.displayList.length; i++) {
      if (this.displayList[i].id == id) {
        this.displayList.splice(i, 1);
        return;
      }
    }
  }

  public loadVerbatim(data) {
    this.verbatim = data;
  }

  showVerbatim(item) {
    const activeModal = this.modalService.open(VerbamodalComponent, { size: 'lg', container: 'nb-layout' });
    activeModal.componentInstance.load(item.name, this.handleVerbatim(item.name));
  }

  handleVerbatim(name) {
    var names = name.split(', ');
    var back = [];
    for (var i = 0; i < names.length; i++) {
      var list = this.searchVerbatim(names[i]);
      for (var j = 0; j < list.length; j++) {
        var found = false;
        for (var k = 0; k < back.length; k++) {
          if (list[j]==back[k].name) {
            found = true;
          }
        }
        if (!found) {
          back.push({
            name: list[j]
          });
        }
      }
    }
    return back;
  }

  searchVerbatim(name): any[] {
    for (var i = 0; i < this.verbatim.length; i++) {
      var element = this.verbatim[i];
      if (element[name]) {
        return element[name];
      }
    }
    return [];
  }

  select(event) {
    if (event.data) {
      if (event.isSelected) {
        if (this.holdingListSelected.indexOf(event.data) < 0) {
          this.holdingListSelected.push(event.data);
        }
      } else {
        this.holdingListSelected.splice(this.holdingListSelected.indexOf(event.data), 1);
      }
    } else {
      if (this.holdingListSelected.length == event.source.data.length) {
        this.holdingListSelected = [];
      } else {
        this.holdingListSelected = event.source.data;
      }
    }
  }

  clear() {
    //this.reset();
    this.startData(this.incoming);
    /*this.originSort = [{field: 'mention', direction: 'desc'}];
    this.originPage = 1;
    this.originActionList = [];
    this.originHoldingList = [...this.firstOriginList];
    this.restart();*/
  }

  restart() {
    this.mainSource.setSort(this.originSort);
    this.mainSource.setPage(this.originPage);
    this.mainSource.load(this.originHoldingList);
    this.actionSource.load(this.originActionList);
    this.holdingList = this.originHoldingList;
    this.actionList = this.originActionList;
    this.holdingListSelected = [];
    this.refresh();
  }

  save() {
    this.actionSource.getAll().then(action => {
      this.mainSource.getFilteredAndSorted().then(data => {
        const activeModal = this.modalService.open(SaverComponent, { size: 'lg', container: 'nb-layout' });
        activeModal.componentInstance.load(data, action, this.lastSort, this.lastPage, this.task);
      });
    });
  }

  open() {
    const activeModal = this.modalService.open(OpenerComponent, { size: 'lg', container: 'nb-layout' });
    activeModal.componentInstance.load(this.task);

    activeModal.componentInstance.update.subscribe(data => {
      this.originHoldingList = data.data;
      this.originActionList = data.action;
      this.originSort = data.sort;
      this.originPage = data.page;
      this.restart();
    });
  }

  constructor(private modalService: NgbModal, private loadingBar: LoadingBarService, public userService: UserService, protected router: Router,
    private activatedRoute: ActivatedRoute, private toasterService: ToasterService) {
      if (this.userService.isLoggedOut()) {
        setTimeout(() => {
          this.activatedRoute.queryParams.subscribe((params: Params) => {
            let taskId = params['task'];
            if (taskId) {
              return this.router.navigateByUrl('/auth/login?page=analysis&task=' + taskId);
            } else {
              return this.router.navigateByUrl('/auth/login');
            }
          });
        }, 0);
      } else {
        this.userService.session();
      }

      this.mainSource.onChanged().subscribe((change) => {
        if (change.action == "sort") {
          this.lastSort = change.sort;
          this.refresh();
        } else {
          this.lastPage = change.paging.page;
        }
      });
  }

  getDateString(timestamp: number): string {
    const date = new Date(timestamp);
    var minutes = "0" + date.getMinutes();
    var seconds = "0" + date.getSeconds();
    var month = (date.getMonth() + 1).toString();

    return '[ ' + date.getDate() + '/' + month + '/' + date.getFullYear() + ' - ' +
      date.getHours() + ':' + minutes.substr(-2) + ':' + seconds.substr(-2) + ' ]';
  }

  createCORSRequest = function(method, url) {
    var xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {
      xhr.open(method, url, true);
    } /*else if (typeof XDomainRequest != "undefined") {
      // IE8 & IE9
      xhr = new XDomainRequest();
      xhr.open(method, url);
    }*/ else {
      xhr = null;
    }
    return xhr;
  };
}
