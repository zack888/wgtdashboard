import { Component, ViewChild, Input, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ChartUrlmodalComponent } from './urlmodal/urlmodal.component';

@Component({
  selector: 'ngx-chart-url',
  template: `
    <button *ngIf="visible" class="btn btn-success btn-tn" style="height: 30px;" (click)="onClick()"><i class="ion-navicon-round" ></i></button>
  `,
})

export class ChartUrlButtonComponent {
  @Input() value;
  @Output() click: EventEmitter<any> = new EventEmitter();

  visible = true;

  constructor(private modalService: NgbModal) {}

  onClick() {
    this.click.emit(this.value);
    var data = [];
    var list = this.value.split(',');
    for (var i = 0; i < list.length; i++) {
      data.push({
        name: list[i]
      });
    }
    const activeModal = this.modalService.open(ChartUrlmodalComponent, { size: 'lg', container: 'nb-layout' });
    activeModal.componentInstance.load(data);
  }
}
