import { Component, ViewChild, Input, SimpleChanges, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ngx-urlstat',
  template: `
    <i *ngIf="rowData.crunched == 'f'" title="Crunching in progress, please wait up to 24 hours for results to be shown" class="ion-load-a"></i>
    <i *ngIf="rowData.crunched == 't'" title="Results for this URL are current shown in this analysis" class="ion-checkmark-round"></i>
  `,
})

export class UrlStatButtonComponent {
  @Input() value;
  @Input() rowData: any;
  @Output() click: EventEmitter<any> = new EventEmitter();

  onClick() {
    this.click.emit(this.rowData);
  }
}
