import { Component, ViewChild, Input, SimpleChanges, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ngx-url',
  styleUrls: ['./fom.component.scss'],
  template: `
    <a href="{{rowData.url}}" target="_blank"><span id="elli" title="{{rowData.url}}">{{rowData.url}}</span></a>
  `,
})

export class UrlLineComponent {
  @Input() value;
  @Input() rowData: any;
}
