import { Component, Output, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import * as AWS from 'aws-sdk';
import * as Cognito from "amazon-cognito-identity-js";
import { CognitoIdentityServiceProvider } from 'amazon-cognito-identity-js';

import { LoadingBarService } from '@ngx-loading-bar/core';

@Component({
  selector: 'ngx-modal',
  template: `
    <div class="modal-header">
      <span>{{ modalHeader }}</span>
      <button class="close" aria-label="Close" (click)="closeModal()">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <div *ngIf="errors && errors.length > 0 && !submitted" class="alert alert-danger" role="alert" style="background-color: white">
        <div><strong>Oh snap! Something went wrong with your submission 😥</strong></div>
        <div *ngFor="let error of errors">{{ error }}</div>
      </div>

      <div *ngIf="messages && messages.length > 0 && !submitted" class="alert alert-success" role="alert" style="background-color: white">
        <div><strong>Your task has been submitted to us! 👍</strong></div>
        <div>It takes one day for the new URL to be crunched. Results will be shown with other URLs in 24 hours!</div>
      </div>

      <small class="form-text error" *ngIf="(url.invalid && url.touched)" style="margin-bottom: 1rem;">
        Valid URL is required!
      </small>

      <form class="form-inline" (ngSubmit)="submit(form)" name="form" #form="ngForm" autocomplete="nope">

        <input name="url" type="url" class="form-control col-md-8" placeholder="Enter a valid product URL"
          [(ngModel)]="submission" #url="ngModel" required
          [class.form-control-danger]="url.invalid && url.touched" autofocus
          [required]="true" [autofocus]="false" style="margin-right: 1rem;">

        <button class="btn btn-md btn-primary col-md-3" [disabled]="submitted || !form.valid" [class.btn-pulse]="submitted">Submit</button>
      </form>
    </div>
    <div class="modal-footer">

    </div>
  `,
})
export class ModalComponent {
  modalHeader: string;
  submission: string;
  tier: any;

  messages: any[];
  errors: any[];

  submitted = false;

  @Output() update = new EventEmitter<any>();

  constructor(private activeModal: NgbActiveModal, private loadingBar: LoadingBarService) { }

  submit(form) {
    this.send(form);
  }

  send(form) {
    this.messages = [];
    this.errors = [];

    this.loadingBar.start();
    this.submitted = true;

    var submitting = this.submission;

    var params = {
      "tier" : this.tier,
      "url": submitting,
      //"monitor": true,
    }

    var url = 'https://jwzu8znvz1.execute-api.ap-southeast-1.amazonaws.com/prod/WGT_NewMonitor';
    var method = 'POST';
    var xhr = this.createCORSRequest(method, url);

    xhr.onload = () => {
      form.reset();
      if (JSON.parse(xhr.responseText).message == "Success") {
        this.submitted = false;
        this.messages.push('Please wait while we analyze the URL. You may check tomorrow for the analysis');
        this.update.emit({
          url: url
        });
      } else {
        this.submitted = false;
        this.errors.push('Unable to process your task. Please try again later');
      }

      this.loadingBar.complete();
    };

    xhr.onerror = () => {
      console.log(xhr.responseText);
      this.submitted = false;
      this.errors.push('Unable to process your task. Please try again later');

      this.loadingBar.complete();
    };

    xhr.send(JSON.stringify(params));
  }

  closeModal() {
    this.activeModal.close();
  }

  createCORSRequest = function(method, url) {
    var xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {
      xhr.open(method, url, true);
    } /*else if (typeof XDomainRequest != "undefined") {
      // IE8 & IE9
      xhr = new XDomainRequest();
      xhr.open(method, url);
    }*/ else {
      xhr = null;
    }
    return xhr;
  };
}
