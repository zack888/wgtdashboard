import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import * as AWS from 'aws-sdk';
import * as Cognito from "amazon-cognito-identity-js";
import { CognitoIdentityServiceProvider } from 'amazon-cognito-identity-js';

import { LoadingBarService } from '@ngx-loading-bar/core';
import { UserService } from '../../../@core/data/users.service';
import { setTimeout } from 'timers';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from './modal.component';

@Component({
  selector: 'ngx-monitor',
  styleUrls: ['./monitor.component.scss'],
  templateUrl: './monitor.component.html',
})

export class MonitorComponent {
}
