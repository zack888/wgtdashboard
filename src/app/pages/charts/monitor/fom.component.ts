import { Component, ViewChild, Input } from '@angular/core';
import { Router } from '@angular/router';

import * as AWS from 'aws-sdk';
import * as Cognito from "amazon-cognito-identity-js";
import { CognitoIdentityServiceProvider } from 'amazon-cognito-identity-js';

import { LoadingBarService } from '@ngx-loading-bar/core';
import { UserService } from '../../../@core/data/users.service';
import { setTimeout } from 'timers';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from './modal.component';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { VerbamodalComponent } from '../analysis/verbamodal/verbamodal.component';

import { LocalDataSource } from 'ng2-smart-table';
import { VisibleButtonComponent } from '../analysis/mentions/visible-button.component';
import { VerbatimButtonComponent } from '../analysis/mentions/verbatim-button.component';
import { UndoButtonComponent } from '../analysis/mentions/undo-button.component';
import { UrlLineComponent } from './url-line.component';
import { RmUrlButtonComponent } from './url-remove.component';
import { UrlStatButtonComponent } from './url-status.component';

@Component({
  selector: 'ngx-fom',
  styleUrls: ['./fom.component.scss'],
  templateUrl: './fom.component.html',
})

export class FomComponent {
  mainSettings = {
    actions: false,
    selectMode: 'multi',
    hideSubHeader: true,
    columns: {
      name: {
        title: 'Term',
        type: 'string',
        width: '30%',
      },
      mention: {
        title: '% of Mention',
        type: 'string',
      },
      negative: {
        title: '% of Negative',
        type: 'string',
      },
      visible: {
        title: 'Visible',
        type: 'custom',
        filter: false,
        sort: false,
        renderComponent: VisibleButtonComponent,
        onComponentInitFunction: (instance: any) => {
          instance.click.subscribe(row => {
            this.refresh();
          });
        },
      },
      verbatim: {
        title: 'Verbatim',
        type: 'custom',
        filter: false,
        sort: false,
        renderComponent: VerbatimButtonComponent,
        onComponentInitFunction: (instance: any) => {
          instance.click.subscribe(row => {
            this.showVerbatim(row);
          });
        },
      },
    },
  };

  chartSettings = {
    actions: false,
    hideSubHeader: true,
    columns: {
      name: {
        title: 'Term',
        type: 'string',
        width: '70%',
        filter: false,
        sort: false,
      },
      mention: {
        title: '% Mention',
        type: 'string',
        width: '30%',
        filter: false,
        sort: false,
      },
    },
  };

  actionSettings = {
    actions: false,
    hideSubHeader: true,
    columns: {
      label: {
        title: 'Action',
        type: 'string',
        width: '70%',
        filter: false,
        sort: false,
      },
      undo: {
        title: 'Undo',
        type: 'custom',
        width: '30%',
        filter: false,
        sort: false,
        renderComponent: UndoButtonComponent,
        onComponentInitFunction: (instance: any) => {
          instance.click.subscribe(row => {
            this.undo(row);
          });
        },
      },
    },
  };

  urlSettings = {
    actions: false,
    hideSubHeader: true,
    columns: {
      remove: {
        title: 'Remove',
        type: 'custom',
        width: '5%',
        filter: false,
        sort: false,
        renderComponent: RmUrlButtonComponent,
        onComponentInitFunction: (instance: any) => {
          instance.click.subscribe(row => {
            this.removeUrl(row);
          });
        },
      },
      status: {
        title: 'Status',
        type: 'custom',
        width: '5%',
        filter: false,
        sort: false,
        renderComponent: UrlStatButtonComponent,
        onComponentInitFunction: (instance: any) => {
          instance.click.subscribe(row => {
            console.log(row);
          });
        },
      },
      url: {
        title: 'URL',
        type: 'custom',
        width: '90%',
        filter: false,
        sort: false,
        renderComponent: UrlLineComponent,
      },
    },
  };

  mainSource: LocalDataSource = new LocalDataSource();
  chartSource: LocalDataSource = new LocalDataSource();
  actionSource: LocalDataSource = new LocalDataSource();
  urlSource: LocalDataSource = new LocalDataSource();

  url: any[] = [];

  lastSort: any[] = [{field: 'mention', direction: 'desc'}];
  lastPage = 1;

  @Input() tier: any;
  @Input() verbatim: any[] = [];
  @ViewChild('datalist') datalist: DatatableComponent;

  tierTitle = "";

  holdingList: any[] = [];
  displayList: any[] = [];
  actionList: any[] = [];
  holdingListSelected: any[] = [];

  availableId = 100000;
  maxDisplay = 10;

  public reset() {
    this.displayList = [];
    this.holdingListSelected = [];
  }

  public start(data) {
    this.holdingList = data;
    this.actionList = [];

    for (var i = 0; i < this.holdingList.length; i++) {
      this.holdingList[i] = {
        name: this.holdingList[i].name,
        mention: this.holdingList[i].value,
        negative: this.holdingList[i].secondary,
        count: this.holdingList[i].count,
        visible: true,
        action: false,
        verbatim: true,
        id: i,
        order: i
      };
    }
    this.mainSource.load(this.holdingList);
    this.actionSource.load(this.actionList);
    this.holdingListSelected = [];
    this.refresh();
  }

  /*public loadVerbatim(data) {
    this.verbatim = data;
  }*/

  removal(event) {
    this.hide(this.displayList[event.index].id);
  }

  /* Refresh display, use this after all ops */
  refresh() {
    this.displayList = [];

    this.mainSource.getFilteredAndSorted().then(data => {
      var i = 0;

      while (this.displayList.length < this.maxDisplay && i < data.length) {
        if (data[i].visible) {
          this.displayList.push(data[i]);
        }
        i++;
      }

      this.chartSource.load(this.displayList);
    });
  }

  /* Toggle term visible */
  hide(id) {
    this.mainSource.getFilteredAndSorted().then(data => {
      for (var i = 0; i < data.length; i++) {
        if (data[i].id == id) {
          data[i].visible = false;
          this.unshow(id);
          this.refresh();
          this.mainSource.refresh();
          return;
        }
      }
    });
  }

  delete() {
    if (this.holdingListSelected.length > 0) {
      var name = "";
      var label = "Deleted";
      var array = [];
      var mention = 0;
      var negative = 0;

      for (var i = 0; i < this.holdingListSelected.length; i++) {
        label = label + " " + this.holdingListSelected[i].name;
        name = name + this.holdingListSelected[i].name;

        if (this.holdingListSelected[i].action) {
          for (var j = 0; j < this.holdingListSelected[i].items.length; j++) {
            array.push(this.holdingListSelected[i].items[j]);
          }

          this.actionList.splice(this.actionList.indexOf(this.holdingListSelected[i].actItem), 1);
        } else {
          array.push(this.holdingListSelected[i]);
        }

        if (i != this.holdingListSelected.length-1) {
          label += ", ";
          name += ", ";
        }

        this.holdingList.splice(this.holdingList.indexOf(this.holdingListSelected[i]), 1);
      }

      var actItem = {
        label: label,
        undo: true,
        isDelete: true,
        id: this.availableId,
        items: array
      };

      this.actionList.push(actItem);

      this.availableId++;
      this.holdingListSelected = [];

      this.actionSource.load(this.actionList);
      this.mainSource.load(this.holdingList).then(() => {
        this.refresh();
      });
    }
  }

  /* Combine Terms */
  combine() {
    if (this.holdingListSelected.length > 1) {
      var name = "";
      var label = "Combined";
      var array = [];
      var mention = 0;
      var negative = 0;

      for (var i = 0; i < this.holdingListSelected.length; i++) {
        label = label + " " + this.holdingListSelected[i].name;
        name = name + this.holdingListSelected[i].name;
        mention += this.holdingListSelected[i].mention;
        negative += this.holdingListSelected[i].negative;

        if (this.holdingListSelected[i].action) {
          for (var j = 0; j < this.holdingListSelected[i].items.length; j++) {
            array.push(this.holdingListSelected[i].items[j]);
          }

          this.actionList.splice(this.actionList.indexOf(this.holdingListSelected[i].actItem), 1);
        } else {
          array.push(this.holdingListSelected[i]);
        }

        if (i != this.holdingListSelected.length-1) {
          label += ", ";
          name += ", ";
        }

        this.holdingList.splice(this.holdingList.indexOf(this.holdingListSelected[i]), 1);
      }

      var actItem = {
        label: label,
        undo: true,
        isDelete: false,
        id: this.availableId
      };

      var action = {
        name: name,
        mention: mention,
        negative: negative,
        //count: count,
        visible: true,
        action: true,
        actItem: actItem,
        id: this.availableId,
        order: -1,
        items: array
      };

      this.actionList.push(actItem);

      this.availableId++;
      this.holdingListSelected = [];

      var inserted = false;

      for (var i = 0; i < this.holdingList.length; i++) {
        if (this.lastSort[0].field == 'mention') {
          if (this.lastSort[0].direction == 'desc') {
            if (!inserted && action.mention >= this.holdingList[i].mention) {
              this.holdingList.splice(i, 0, action);
              inserted = true;
            }
          } else {
            if (!inserted && action.mention <= this.holdingList[i].mention) {
              this.holdingList.splice(i, 0, action);
              inserted = true;
            }
          }
        } else {
          if (this.lastSort[0].direction == 'desc') {
            if (!inserted && action.negative >= this.holdingList[i].negative) {
              this.holdingList.splice(i, 0, action);
              inserted = true;
            }
          } else {
            if (!inserted && action.negative <= this.holdingList[i].negative) {
              this.holdingList.splice(i, 0, action);
              inserted = true;
            }
          }
        }
      }
      if (!inserted) {
        this.mainSource.append(action);
      } else {
        this.mainSource.load(this.holdingList);
      }
      this.actionSource.load(this.actionList);
      this.refresh();
    }
  }

  /* Undo action */
  undo(action) {
    this.actionList.splice(this.actionList.indexOf(action), 1);

    var newData = [];
    var items = [];

    if (action.isDelete) {
      newData = this.holdingList;
      items = action.items;
    } else {
      var found = false;
      for (var i = 0; i < this.holdingList.length; i++) {
        if (this.holdingList[i].id == action.id) {
          items = this.holdingList[i].items;
        } else {
          newData.push(this.holdingList[i]);
        }
      }
    }

    items = this.sort(items);
    var lead = 0;
    var count = 0;

    while (count < newData.length) {
      var inserted = false;
      if (this.lastSort[0].field == 'mention') {
        if (this.lastSort[0].direction == 'desc') {
          if (!inserted && items[lead].mention >= newData[count].mention) {
            newData.splice(count, 0, items[lead]);
            inserted = true;
          }
        } else {
          if (!inserted && items[lead].mention <= newData[count].mention) {
            if (items[lead].id > newData[count].id) {
              newData.splice(i, 0, items[lead]);
              inserted = true;
            }
          }
        }
      } else {
        if (this.lastSort[0].direction == 'desc') {
          if (!inserted && items[lead].negative >= newData[count].negative) {
            if (items[lead].id < newData[count].id) {
              newData.splice(i, 0, items[lead]);
              inserted = true;
            }
          }
        } else {
          if (!inserted && items[lead].negative <= newData[count].negative) {
            if (items[lead].id > newData[count].id) {
              newData.splice(i, 0, items[lead]);
              inserted = true;
            }
          }
        }
      }

      if (inserted) {
        lead++;
      } else {
        count++;
      }

      if (lead == items.length) {
        break;
      }
    }

    if (lead < items.length) {
      for (var i = lead; i < items.length; i++) {
        newData.push(items[i]);
      }
    }

    this.holdingList = newData;

    this.actionSource.load(this.actionList);
    this.mainSource.load(newData).then(() => {
      this.refresh();
    });
  }

  sort(items) : any[] {
    var self = this;
    var ret = items.sort( function(a,b) {
      if (self.lastSort[0].field == 'mention') {
        if (self.lastSort[0].direction == 'desc') {
          return a.id - b.id;
        } else {
          return a.mention - b.mention;
        }
      } else {
        if (self.lastSort[0].direction == 'desc') {
          return b.negative - a.negative;
        } else {
          return a.negative - b.negative;
        }
      }
    });
    return ret;
  }

  /* Remove the term from chart */
  unshow(id) {
    for (var i = 0; i < this.displayList.length; i++) {
      if (this.displayList[i].id == id) {
        this.displayList.splice(i, 1);
        return;
      }
    }
  }

  showVerbatim(item) {
    const activeModal = this.modalService.open(VerbamodalComponent, { size: 'lg', container: 'nb-layout' });
    activeModal.componentInstance.load(item.name, this.handleVerbatim(item.name));
  }

  handleVerbatim(name) {
    var names = name.split(', ');
    var back = [];
    for (var i = 0; i < names.length; i++) {
      var list = this.searchVerbatim(names[i]);
      for (var j = 0; j < list.length; j++) {
        var found = false;
        for (var k = 0; k < back.length; k++) {
          if (list[j]==back[k].name) {
            found = true;
          }
        }
        if (!found) {
          back.push({
            name: list[j]
          });
        }
      }
    }
    return back;
  }

  searchVerbatim(name): any[] {
    for (var i = 0; i < this.verbatim.length; i++) {
      var element = this.verbatim[i];
      if (element[name]) {
        return element[name];
      }
    }
    return [];
  }

  loadVerbatim(id) {
    var self = this;
    var xhr = this.createCORSRequest('GET', 'https://s3-ap-southeast-1.amazonaws.com/wgtimages/v' + id + '.json');
    xhr.onload = () => {
      self.loadingBar.complete();
      this.verbatim = JSON.parse(xhr.responseText);
    }

    xhr.onerror = () => {
      console.log(xhr.responseText);
      this.loadingBar.complete();
    };

    xhr.send();
  }

  setVisible(index, visible) {
    this.holdingList[index].visible = visible;
  }

  select(event) {
    if (event.data) {
      if (event.isSelected) {
        this.holdingListSelected.push(event.data);
      } else {
        this.holdingListSelected.splice(this.holdingListSelected.indexOf(event.data), 1);
      }
    } else {
      if (this.holdingListSelected.length == event.source.data.length) {
        this.holdingListSelected = [];
      } else {
        this.holdingListSelected = event.source.data;
      }
    }
  }

  add(tier) {
    const activeModal = this.modalService.open(ModalComponent, { size: 'lg', container: 'nb-layout' });
    activeModal.componentInstance.modalHeader = 'Add URL to Tier ' + tier.toString();
    activeModal.componentInstance.tier = tier;
    activeModal.componentInstance.update.subscribe((e) => {
      this.url.push(e);
      this.url = [...this.url];
    })
  }

  constructor(private loadingBar: LoadingBarService, public userService: UserService, protected router: Router, private modalService: NgbModal) {
    if (this.userService.isLoggedOut()) {
      setTimeout(() => {
        return this.router.navigateByUrl('/auth/login');
      }, 0);
    } else {
      this.userService.session();
    }

    this.mainSource.onChanged().subscribe((change) => {
      if (change.action == "sort") {
        this.lastSort = change.sort;
        this.refresh();
      } else {
        this.lastPage = change.paging.page;
      }
    });
  }

  ngOnInit() {
    this.displayList = [];
    this.holdingListSelected = [];

    this.loadingBar.start();

    var userPool = new Cognito.CognitoUserPool({
      UserPoolId: 'ap-southeast-1_7PvKAeRNz',
      ClientId: 'sa6cluje5ml19da5j49hfr2ms',
    });

    AWS.config.update({
      region: 'ap-southeast-1'
    });

    var docClient = new AWS.DynamoDB.DocumentClient();

    docClient.query({
      TableName: "WGT_Analysis",
      KeyConditionExpression: "#id = :time",
      ExpressionAttributeNames: {
          "#id": "timestamp",
      },
      ExpressionAttributeValues: {
            ":time": this.tier,
      }
    }, (err, data) => {
      if (err) {
        this.loadingBar.complete();
      } else {
        for (let item of data.Items) {
          if (item.data) {
            this.holdingList = item.data;
          }
        };

        this.tierTitle = this.tier.toString() + ": " + data.Items[0].title;
        this.start(data.Items[0].data);

        docClient.query({
          TableName: "WGT_Monitor",
          KeyConditionExpression: "#id = :time",
          ExpressionAttributeNames: {
              "#id": "tier",
          },
          ExpressionAttributeValues: {
                ":time": this.tier,
          }
        }, (err, data) => {
          if (err) {
            this.loadingBar.complete();
          } else {
            for (let item of data.Items) {
              if (item) {
                this.url.push(item);
              }
            };
            console.log(this.url);
            this.urlSource.load(this.url);

            this.loadVerbatim(this.tier);

            //this.loadingBar.complete();
          }
        });
      }
    });
  }

  removeUrl(item) {
    this.loadingBar.start();

    var url = 'https://jwzu8znvz1.execute-api.ap-southeast-1.amazonaws.com/prod/WGT_DeleteMonitorURL';
    var method = 'POST';
    var xhr = this.createCORSRequest(method, url);

    xhr.onload = () => {
      if (JSON.parse(xhr.responseText).message == "Success") {
        this.urlSource.remove(item);
      }

      this.loadingBar.complete();
    };

    xhr.onerror = () => {
      console.log(xhr.responseText);
      this.loadingBar.complete();
    };

    xhr.send(JSON.stringify(item));
  }

  createCORSRequest = function(method, url) {
    var xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {
      xhr.open(method, url, true);
    } /*else if (typeof XDomainRequest != "undefined") {
      // IE8 & IE9
      xhr = new XDomainRequest();
      xhr.open(method, url);
    }*/ else {
      xhr = null;
    }
    return xhr;
  };
}
