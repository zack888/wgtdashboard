import { Component, ViewChild, Input, SimpleChanges, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ngx-rmurl',
  template: `
    <button class="btn btn-danger btn-tn" style="height: 30px;" (click)="onClick()" [disabled]="!enable"><i class="ion-close-round"></i></button>
  `,
})

export class RmUrlButtonComponent {
  @Input() value;
  @Input() rowData: any;
  @Output() click: EventEmitter<any> = new EventEmitter();
  enable = true;

  onClick() {
    this.enable = false;
    this.click.emit(this.rowData);
  }
}
