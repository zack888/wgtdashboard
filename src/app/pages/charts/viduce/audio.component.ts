import { Component, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import * as AWS from 'aws-sdk';
import * as Cognito from "amazon-cognito-identity-js";
import { CognitoIdentityServiceProvider } from 'amazon-cognito-identity-js';

import { dateFormat } from 'dateformat';

import { LoadingBarService } from '@ngx-loading-bar/core';
import { UserService } from '../../../@core/data/users.service';

import { EmbedVideoService } from 'ngx-embed-video';
import { LocalDataSource } from 'ng2-smart-table';

import { ToasterService, ToasterConfig, Toast, BodyOutputType } from 'angular2-toaster';

@Component({
  selector: 'ngx-audio-viduce',
  styleUrls: ['./audio.component.scss'],
  templateUrl: './audio.component.html',
})

export class AudioViduceComponent {
  tasks: any[] = [
    { id: 0, url: 'Select a completed task to view analysis', date: '' },
  ];
  analysis: any;
  curTask: any = this.tasks[0];
  curr = 0;
  currTaskId = 0;
  a = 0;

  empty = false;

  config: ToasterConfig;

  settings = {
    actions: false,
    hideSubHeader: true,
    columns: {
      transcript: {
        title: 'Transcript',
        type: 'string',
        width: '80%',
        filter: false,
        sort: false,
      },
      confidence: {
        title: 'Confidence (%)',
        type: 'string',
        width: '20%',
        filter: false,
        sort: false,
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private loadingBar: LoadingBarService, public userService: UserService, private toasterService: ToasterService,
    protected router: Router, private activatedRoute: ActivatedRoute, private embedService: EmbedVideoService) {
    if (this.userService.isLoggedOut()) {
      setTimeout(() => {
        this.activatedRoute.queryParams.subscribe((params: Params) => {
          let taskId = params['task'];
          if (taskId) {
            return this.router.navigateByUrl('/auth/login?page=audio&task=' + taskId);
          } else {
            return this.router.navigateByUrl('/auth/login');
          }
        });
      }, 0);
    } else {
      this.userService.session();
    }
  }

  ngOnInit() {
    var hasTask = false;

    this.activatedRoute.queryParams.subscribe((params: Params) => {
      let taskId = params['task'];
      if (taskId) {
        this.view(taskId);
        hasTask = true;
      }
    });

    var userPool = new Cognito.CognitoUserPool({
      UserPoolId: 'ap-southeast-1_7PvKAeRNz',
      ClientId: 'sa6cluje5ml19da5j49hfr2ms',
    });

    AWS.config.update({
      region: 'ap-southeast-1'
    });

    var docClient = new AWS.DynamoDB.DocumentClient();

    var params = {
      TableName: "WGT_Video",
      KeyConditionExpression: "#id = :id",
      ExpressionAttributeNames: {
          "#id": "username",
      },
      ExpressionAttributeValues: {
           ":id": userPool.getCurrentUser().getUsername(),
      },
      ScanIndexForward: false
    };

    this.loadingBar.start();

    docClient.query(params, (err, data) => {
      if (err) {
        //TODO
        console.log(err);
      } else {
        for (let item of data.Items) {
          if (item.stats == 100) {
            hasTask = true;
            this.tasks.push({
              id: this.tasks.length,
              url: item.title,
              date: this.getDateString(item.timestamp),
              timestamp: item.timestamp,
              videoId: item.video,
              vid: this.getParameterByName('v', item.url)
            });
          }
        };

        if (!hasTask) {
          this.empty = true;
        }
      }

      this.loadingBar.complete();
    });
  }

  getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  }

  getDateString(timestamp: number): string {
    const date = new Date(timestamp);
    var minutes = "0" + date.getMinutes();
    var seconds = "0" + date.getSeconds();
    var month = (date.getMonth() + 1).toString();

    return '[ ' + date.getDate() + '/' + month + '/' + date.getFullYear() + ' - ' +
      date.getHours() + ':' + minutes.substr(-2) + ':' + seconds.substr(-2) + ' ]';
  }

  setNewUser(id: any): void {
    this.curr = id;
    this.curTask = this.tasks.filter(value => value.id === parseInt(id));

    if (id > 0) {
      this.loadingBar.start();
      this.currTaskId = this.tasks[id].timestamp;

      var userPool = new Cognito.CognitoUserPool({
        UserPoolId: 'ap-southeast-1_7PvKAeRNz',
        ClientId: 'sa6cluje5ml19da5j49hfr2ms',
      });

      AWS.config.update({
        region: 'ap-southeast-1'
      });

      this.load(this.tasks[id].videoId);
    }
  }

  view(taskId) {
    this.loadingBar.start();

    var userPool = new Cognito.CognitoUserPool({
      UserPoolId: 'ap-southeast-1_7PvKAeRNz',
      ClientId: 'sa6cluje5ml19da5j49hfr2ms',
    });

    AWS.config.update({
      region: 'ap-southeast-1'
    });

    this.load(taskId);
  }

  load(id) {
    var self = this;
    var xhr = this.createCORSRequest('GET', 'https://s3-ap-southeast-1.amazonaws.com/viduce/' + id + '.json');
    xhr.onload = () => {
      console.log(xhr.responseText);
      if (/^[\],:{}\s]*$/.test(xhr.responseText.replace(/\\["\\\/bfnrtu]/g, '@').
      replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
      replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
        this.analysis = JSON.parse(xhr.responseText);
        var url = this.analysis.url;

        this.showToast('default', 'You are viewing', this.analysis.title);

        for (var i = 0; i < this.analysis.audios.length; i++) {
          this.analysis.audios[i].confidence = parseFloat(this.analysis.audios[i].confidence).toFixed(2);
        }

        this.source.load(this.analysis.audios);
      }

      self.loadingBar.complete();
    }

    xhr.onerror = () => {
      this.loadingBar.complete();
    };

    xhr.send();
  }

  private showToast(type: string, title: string, body: string) {
    this.config = new ToasterConfig({
      positionClass: 'toast-top-right',
      timeout: 10000,
      newestOnTop: true,
      tapToDismiss: true,
      preventDuplicates: true,
      animation: 'slideDown',
      limit: 5,
    });
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 10000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this.toasterService.popAsync(toast);
  }

  createCORSRequest = function(method, url) {
    var xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {
      xhr.open(method, url, true);
    } else {
      xhr = null;
    }
    return xhr;
  };
}
