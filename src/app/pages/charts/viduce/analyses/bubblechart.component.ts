import {Component, NgModule, Input, OnChanges} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {NgxChartsModule} from '@swimlane/ngx-charts';

@Component({
  selector: 'ngx-bubble-chart',
  template: `
  <ngx-charts-bubble-chart
    [view]="view"
    class="chart-container"
    [results]="single"
    [showYAxisLabel]="showYAxisLabel"
    [yAxisLabel]="yAxisLabel"
    [xAxisLabel]="xAxisLabel"
    [autoScale]="autoScale"
    [scheme]="colorScheme">

  </ngx-charts-bubble-chart>
  `
})
export class BubbleChartComponent {
  single: any[] = [];
  @Input() analysis: any;

  view: any[] = [];
  autoscale = true;

  showYAxisLabel = true;
  yAxisLabel = 'Appeared at Time (second)';
  xAxisLabel = 'Category';

  colorScheme = {
    domain: ['#4af48b', '#42db7d', '#3bc36f', '#33aa61', '#2c9253', '#257a45', '#1d6137']
  };

  constructor() {
    this.view = [
      window.innerWidth * 0.7,
      window.innerHeight * 0.7
    ];
    /*for (var i =0; i < 10;i++) {
      const bubbleYear = Math.floor((2010 - 1990) * Math.random() + 1990);
      const bubbleEntry = {
        name: 'Frame',
        series: [{
          name: 'T ' + bubbleYear,
          x: 'Demo ' + i,
          y: Math.floor(30 + Math.random() * 70),
          r: Math.floor(30 + Math.random() * 20),
        }]
      };
      this.single = [...this.single, bubbleEntry];
    }*/
  }

  ngOnChanges(...args: any[]) {
    if (this.analysis) {
      this.single.length = 0;
      this.single = [];
      let items = this.analysis.frames;

      for (let item of items) {
        var cat = 'general';
        if (item.categories.length > 0) {
          cat = item.categories[0].category;
        }
        this.single.push({
          name: item.description,
          series: [{
            name: cat,
            x: cat,
            y: item.offset,
            r: item.confidence * 100
          }]
        });
      }

      this.single = [...this.single];
    }
  }
}
