import {Component, NgModule, Input, OnChanges} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {NgxChartsModule} from '@swimlane/ngx-charts';

@Component({
  selector: 'ngx-timeline',
  styleUrls: ['./timeline.component.scss'],
  template: `
  <div class="container">
    <ul>
      <li *ngFor="let item of display"><span></span>
        <div>
          <div class="title">Timeframe: {{item.start}}s to {{item.end}}s</div>
          <div class="info">{{item.entity}}</div>
        </div> <span class="number"><span>{{item.pstart}}</span> <span>{{item.pend}}</span></span>
      </li>
    </ul>
  </div>
  `
})
export class TimelineComponent {
  @Input() analysis: any;
  details: any[] = [];
  display: any[] =[{
    items: [],
    start: 0,
    end: 10,
    pstart: '0:00',
    pend: '0:10'
  }];

  constructor() {}

  ngOnChanges(...args: any[]) {
    if (this.analysis) {
      this.details = [];
      this.display = [];

      let items = this.analysis.shots;

      for (let item of items) {
        var cat = 'general';
        if (item.categories.length > 0) {
          cat = item.categories[0].category;
        }

        for (let segment of item.data) {
          this.details.push({
            description: item.description,
            category: cat,
            start: segment.segment.start,
            end: segment.segment.end,
            confidence: Math.round(segment.confidence*100)
          });
        }
      }

      this.details.sort(this.compare);

      var pointer = -1;
      for (var i = 0; i < this.details.length; i++) {
        if (i != 0) {
          if (this.details[i-1].end >= this.details[i].start) {
            this.display[pointer].items.push(this.details[i]);
            if (this.display[pointer].end < this.details[i].end) {
              this.display[pointer].end = this.details[i].end;
              this.display[pointer].pend = this.convert(this.details[i].end);
            }
            this.display[pointer].entity += ', ' + this.details[i].description + ' (' + this.details[i].confidence + '%)';
          } else {
            this.display.push({
              items: [this.details[i]],
              start: this.details[i].start,
              end: this.details[i].end,
              pstart: this.convert(Math.round(this.details[i].start)),
              pend: this.convert(Math.round(this.details[i].end)),
              entity: this.details[i].description + ' (' + this.details[i].confidence + '%)'
            });
            pointer++;
          }
        } else {
          this.display.push({
            items: [this.details[i]],
            start: this.details[i].start,
            end: this.details[i].end,
            pstart: this.convert(Math.round(this.details[i].start)),
            pend: this.convert(Math.round(this.details[i].end)),
            entity: this.details[i].description + ' (' + this.details[i].confidence + '%)'
          });
          pointer++;
        }
      }

      console.log(this.display);
    }
  }

  convert(value) {
    var val = '00';
    if (value % 60) {
      if (value % 60 < 10) {
        val = '0' + (value % 60);
      } else {
        val = (value % 60).toString();
      }
    }
    return Math.floor(value / 60) + ":" + (value % 60 ? val : '00')
  }

  compare(a, b) {
    let comparison = 0;
    if (a.start > b.start) {
      comparison = 1;
    } else if (a.start < b.start) {
      comparison = -1;
    }
    return comparison;
  }
}
