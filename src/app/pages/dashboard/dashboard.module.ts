import { NgModule } from '@angular/core';
import { AngularEchartsModule } from 'ngx-echarts';
import { ThemeModule } from '../../@theme/theme.module';
import { DashboardComponent } from './dashboard.component';
import { xStatusCardComponent } from './status-card/status-xcard.component';
import { NotificationsComponent } from './notifications/notifications.component';
//import { NgxStripeModule } from 'ngx-stripe';
import { ToasterModule } from 'angular2-toaster';
import { NgxCarouselModule } from 'ngx-carousel';

@NgModule({
  imports: [
    ThemeModule,
    AngularEchartsModule,
    //NgxStripeModule.forRoot('pk_test_oDf2z6Ps7ZiiHwdMBvaHQA4a'),
    ToasterModule,
    NgxCarouselModule
  ],
  declarations: [
    DashboardComponent,
    xStatusCardComponent,
    NotificationsComponent,
  ],
})

export class DashboardModule { }
