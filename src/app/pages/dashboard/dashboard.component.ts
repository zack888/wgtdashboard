import { Component } from '@angular/core';
import { FormBuilder } from "@angular/forms";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User, UserService } from '../../@core/data/users.service';
import { Http } from '@angular/http';
import { ToasterService, ToasterConfig, Toast, BodyOutputType } from 'angular2-toaster';
import { NbThemeService } from '@nebular/theme';
//import { StripeService, Elements, Element as StripeElement, ElementsOptions } from "ngx-stripe";
import { LoadingBarService } from '@ngx-loading-bar/core';
import { NgxCarousel } from 'ngx-carousel';

@Component({
  selector: 'ngx-dashboard',
  styleUrls: ['./dashboard.component.scss'],
  templateUrl: './dashboard.component.html',
})

export class DashboardComponent {
  LOGO = require("../../../assets/images/flowchart.jpg");

  redirectDelay: number = 0;

  userInterface: User;
  user: any;

  item = 'Select Package';
  prices = ['SGD $60', 'SGD $300', 'SGD $600', 'SGD $3,000', 'SGD $6,000'];
  credits = [100, 500, 1000, 5000, 10000];

  curr = 0;
  currpack = 0;

  submission: any = {};
  disable = true;

  error: string;
  message: string;

  /*elements: Elements;
  card: StripeElement;
  @ViewChild('card') cardRef: ElementRef;
  stripeTest: FormGroup;
  elementsOptions: ElementsOptions = {};*/

  config: ToasterConfig;

  tutObjects = [];

  constructor(private userService: UserService, private http: Http, private fb: FormBuilder, /*private stripeService: StripeService,*/
    private loadingBar: LoadingBarService, protected router: Router, private toasterService: ToasterService,
    private activatedRoute: ActivatedRoute, private themeService: NbThemeService) {

    this.userService.session();
    this.userInterface = this.userService.userInterface;
  }

  ngOnInit() {
    if (this.userService.isLoggedOut()) {
      setTimeout(() => {
        return this.router.navigateByUrl('/auth/login');
      }, 0);
    } else {
      this.userService.reload();
      this.userService.getUsers()
      .subscribe((users: any) => {
        this.user =  users.user;
        console.log(this.user);
      });

      this.activatedRoute.queryParams.subscribe((params: Params) => {
        if (params['zone']) {
          if (params['zone'] == 'na') {
            this.themeService.changeTheme('cosmic');
            this.user.icon = require("../../../assets/images/ic_na.png");
          } else {
            this.themeService.changeTheme('default');
            this.user.icon = require("../../../assets/images/ic_sg.png");
          }
        } else {
          this.themeService.changeTheme('default');
          this.user.icon = require("../../../assets/images/ic_sg.png");
        }

        let taskId = params['task'];
        let pageId = params['page'];
        if (taskId) {
          setTimeout(() => {
            this.loadingBar.complete();
            if (taskId && pageId) {
              return this.router.navigateByUrl('/pages/charts/' + pageId + '?task=' + taskId);
            }
          }, this.redirectDelay);
        }
      });

      if (window.sessionStorage.getItem("first_entry")) {
        this.showToast('default', 'Welcome Back!', 'Start a new task and power up your data analytics now 😎')

        if (/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent)
          || /MSIE 10/i.test(navigator.userAgent) || /Edge\/\d./i.test(navigator.userAgent)) {
          this.showToast('default', 'Do you know?', 'Where Got Text is best viewed with Chrome!')
        }
      } else {
        window.sessionStorage.setItem("first_entry", 'false');
        this.showToast('default', 'Welcome!', 'Feeling lost? Take a look at "Getting Started" to find out why we rock 😘')
      }

      this.carouselTile = {
        grid: {xs: 1, sm: 1, md: 1, lg: 1, all: 0},
        speed: 400,
        animation: 'lazy',
        point: {
          visible: false,
        },
        load: 1,
        touch: true,
        easing: 'ease',
        interval: 6000,
      }
    }
  }

  private showToast(type: string, title: string, body: string) {
    this.config = new ToasterConfig({
      positionClass: 'toast-top-right',
      timeout: 10000,
      newestOnTop: true,
      tapToDismiss: true,
      preventDuplicates: true,
      animation: 'slideDown',
      limit: 5,
    });
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 10000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this.toasterService.popAsync(toast);
  }

/*
  ngOnInit() {
    this.stripeTest = this.fb.group({
      name: ['', [Validators.required]]
    });
    this.stripeService.elements(this.elementsOptions)
      .subscribe(elements => {
        this.elements = elements;
        // Only mount the element the first time
        if (!this.card) {
          this.card = this.elements.create('card', {
            hidePostalCode: true,
            style: {
              base: {
                iconColor: '#666EE8',
                color: '#31325F',
                lineHeight: '40px',
                fontWeight: 300,
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSize: '18px',
                '::placeholder': {
                  color: '#CFD7E0'
                }
              }
            }
          });
          this.card.mount(this.cardRef.nativeElement);
        }
      });
  }*/

  /*buy() {
    this.error = '';
    this.message = '';
    this.disable = true;

    this.stripeService
      .createToken(this.card, {  })
      .subscribe(result  => {
        if (result.token) {
          console.log(result.token);

          var userPool = new Cognito.CognitoUserPool({
            UserPoolId: 'ap-southeast-1_7PvKAeRNz',
            ClientId: 'sa6cluje5ml19da5j49hfr2ms',
          });

          var price = this.currpack * 60;

          var params = {
            "id" : userPool.getCurrentUser().getUsername(),
            "credits": this.currpack,
            "currency": 'sgd',
            "token": result.token.id,
            "amount": price
          }

          var url = 'https://jwzu8znvz1.execute-api.ap-southeast-1.amazonaws.com/prod/WGT_Payment';
          var method = 'POST';
          var xhr = this.createCORSRequest(method, url);

          xhr.onload = () => {
            // Success code goes here.
            console.log(xhr.responseText);

            if (JSON.parse(xhr.responseText).message == "Success") {
              this.disable = false;
              this.message = 'Success';
              this.userService.reload();
            } else {
              this.disable = false;
              this.error = JSON.parse(xhr.responseText).message.message;
            }
          };

          xhr.onerror = () => {
            // Error code goes here.
            console.log(xhr.responseText);
            this.disable = false;
            this.error = 'Unable to process your payment. Please try again later';
          };

          xhr.send(JSON.stringify(params));
        } else if (result.error) {
          console.log(result.error.message);
          this.disable = false;
          this.error = result.error.message;
        }
      });
  }*/

  select(id) {
    this.item = this.prices[id];
    this.curr = id;
    this.currpack = this.credits[id];
    this.disable = false;
  }

  createCORSRequest = function(method, url) {
    var xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {
      xhr.open(method, url, true);
    } /*else if (typeof XDomainRequest != "undefined") {
      // IE8 & IE9
      xhr = new XDomainRequest();
      xhr.open(method, url);
    }*/ else {
      xhr = null;
    }
    return xhr;
  };

  public carouselTileLoad(evt: any) {};
  public carouselTileItems: Array<any> = [
    {
      back: 'https://wgtest-ce5df.firebaseapp.com/assets/images/wgtback.jpg',
      img: require('../../../assets/images/dtf_01.png'),
      title: 'Step 1: Submit New Task',
      desc: 'Go to <a href="#/pages/forms/newtask">Submit New Task</a> page<br>Copy and paste the website link (URL) of the product page(s) that you want to analyze',
      action: true,
      act: 'Submit New Task Now',
      link: '#/pages/forms/newtask'
    },
    {
      back: 'https://wgtest-ce5df.firebaseapp.com/assets/images/wgtback.jpg',
      img: require('../../../assets/images/dtf_02.png'),
      title: 'Step 2: My Tasks',
      desc: 'You can see the status of your submission <a href="#/pages/tables/taskstatus">here</a><br>Please wait for it to be <i><u>completed</i></u> to view the analysis',
      action: true,
      act: 'View My Tasks',
      link: '#/pages/tables/taskstatus'
    },
    {
      back: 'https://wgtest-ce5df.firebaseapp.com/assets/images/wgtback.jpg',
      img: require('../../../assets/images/dtf_03.png'),
      title: 'Step 3: Analysis',
      desc: 'Once task status indicates <i><u>Completed</u></i>, you may view the full analyses straight away!',
      action: true,
      act: 'Analysis Example',
      link: '#/pages/charts/analysis?task=1517195444691',
      action2: true,
      act2: 'Word Cloud Example',
      link2: '#/pages/charts/cloud?task=1517195444691'
    }
  ];
  public carouselTile: NgxCarousel;

}
