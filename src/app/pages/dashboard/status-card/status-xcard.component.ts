import { Component, Input } from '@angular/core';

@Component({
  selector: 'ngx-status-xcard',
  styleUrls: ['./status-xcard.component.scss'],
  template: `
    <nb-card>
      <div class="icon-container">
        <div class="icon {{ type }}">
          <ng-content></ng-content>
        </div>
      </div>

      <div class="details">
        <div class="title">{{ title }}</div>
        <div class="status">{{ status }}</div>
      </div>
    </nb-card>
  `,
})
export class xStatusCardComponent {
  @Input() status: string;
  @Input() title: string;
  @Input() type: string;
  @Input() on = true;
}
