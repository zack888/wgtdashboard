import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'nb-home',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'DATA CLOUD',
    group: true,
  },
  {
    title: 'Submit New Task',
    icon: 'nb-compose',
    link: '/pages/submit/task',
    home: false,
  },
  {
    title: 'My Tasks',
    icon: 'nb-tables',
    link: '/pages/status/task',
    home: false,
  },
  {
    title: 'Analysis',
    icon: 'nb-bar-chart',
    link: '/pages/analysis/task',
    home: false,
  },
  {
    title: 'Word Cloud',
    icon: 'nb-cloudy',
    link: '/pages/analysis/cloud',
    home: false,
  },
  {
    title: 'Cluster',
    icon: 'nb-shuffle',
    link: '/pages/analysis/cluster',
    home: false,
  },
  {
    title: 'Text in Images',
    icon: 'nb-title',
    link: '/pages/analysis/text',
    home: false,
  },
  {
    title: 'Monitor',
    icon: 'nb-star',
    link: '/pages/analysis/monitor',
    home: false,
  },
  {
    title: 'VIDUCE',
    group: true,
  },
  {
    title: 'New Viduce Task',
    icon: 'nb-compose',
    link: '/pages/submit/viduce',
    home: false,
  },
  {
    title: 'My Viduce Tasks',
    icon: 'nb-tables',
    link: '/pages/status/viduce',
    home: false,
  },
  {
    title: 'Viduce Analysis',
    icon: 'nb-audio',
    link: '/pages/analysis/viduce',
    home: false,
  },
  {
    title: 'Audio Analysis',
    icon: 'nb-volume-high',
    link: '/pages/analysis/audio',
    home: false,
  },
];
